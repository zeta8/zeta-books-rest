# zeta-books-rest

The test for Nickel

## How to use the app
For the convenience of user three helper end points are exposed which are accesssible on the swagger:

### /persons/status/add
Which adds the status of the persons:
#### WRI: Writer (the writers of the books)
#### CUS: Customer (the customer who commands the books)
##### Of course A person can have both CUS and WRI statuses
The server will feed in the data from a csv file placed in:
resources/static/status.csv


### /Persons/add
Which adds some 22 persons
The server will add these persons from the file placed in:
resources/static/persons.csv


### /books/add
Which adds 14 books along with their writers.
The server will feed in the information on persons from the file placed in:
resources/static/books.csv

## How to run the application

### Precondition:
#### 1- You need to have MySql up and running on your local.
#### 2- In your mysql instance you need to create two new databases
Your database needs to be called nickel_dev: for dev
and another database for test: nickel_test for tests.
Even if the tests do not really use this database for now.


#### 1- clone the code:
git clone git@gitlab.com:zeta6/zeta-books-rest.git

#### 2- Install the dependencies and produce the jar file
mvn clean install

#### 3- Run the application:
java -jar target/books-0.0.1-SNAPSHOT.jar

#### Important note (The application uses java-11)

