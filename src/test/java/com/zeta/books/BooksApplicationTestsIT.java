package com.zeta.books;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;



@SpringBootTest
@TestPropertySource(value = "classpath:database-test.properties")
class BooksApplicationTestsIT {
	@Test
	void contextLoads() {
	}

}
