package com.zeta.books.toolbox;

import java.util.Random;

public class RandomStringGenerator {
    public static String generate(int len) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        sb.append("-");
        sb.append(len);
        return sb.toString();
    }
}

