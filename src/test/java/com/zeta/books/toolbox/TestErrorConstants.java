package com.zeta.books.toolbox;

public class TestErrorConstants {
    public static final String MANDATORY_VALUE_MSG = "is mandatory";
    public static final String ALREADY_FED_MSG = "has been already fed in";
    public static final String DUAL_BUILD_MSG = "is already built, it cannot be rebuilt";
    public static final String FED_AFTER_INSTANTIATION_MSG = "Feeding in the fields is not permitted after the object is built";
    public static final String NOT_FOUND_MSG = "not found";
    public static final String ILLEGAL_COMMAND_MSG = "There are no valid items in the command";
    public static String COUPLED_FIELD_ERR = "should be either both fed, or no of them fed";
    public static final String RESOURCE_CONFLICT_ERR = "exists already";
}
