package com.zeta.books.toolbox;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;


@Component
public class HttpEntityCreator {

    @Value("${authorizationToken}")
    private String authorizationToken;


    // sets the headers f the http request: here an example of Bearer authorization token
    private HttpHeaders createHttpHeaders() {
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.set("Authorization", "Bearer " + authorizationToken);
        return requestHeaders;
    }

    /**
     * Creates an HttpEntity that is enriched with the request headers
     *
     * @param dtoIn: The Dto containing the input pf the http service to be called
     * @param <T>    The generic type
     * @return an HttpEntity
     */
    public <T> HttpEntity<T> createHttpEntity(T dtoIn) {
        final HttpHeaders requestHeaders = createHttpHeaders();
        return new HttpEntity<>(dtoIn, requestHeaders);
    }
}
