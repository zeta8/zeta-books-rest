package com.zeta.books.e2e;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.*;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.toolbox.HttpEntityCreator;
import com.zeta.books.toolbox.RandomStringGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {BooksApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PersonServiceE2ETestsIT {

    @Value("${url.service.person.status.create-from-file}")
    private String URL_SERVICE_STATUS_CREATE_FROM_FILE;

    @Value("${url.service.person.create-from-file}")
    private String URL_SERVICE_PERSONS_CREATE_FROM_FILE;

    @Value("${url.service.person.status.create-one}")
    private String URL_SERVICE_PERSON_STATUS_CREATE_ONE;

    @Value("${url.service.person.create}")
    private String URL_SERVICE_PERSON_CREATE;

    @Value("${url.service.person.find}")
    private String URL_SERVICE_PERSON_FIND;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private HttpEntityCreator httpEntityCreator;

    private static final String PERSON_CODE = RandomStringGenerator.generate(3);
    private static final String FIRST_NAME = "Siavosh";
    private static final String LAST_NAME = "DAKLAN";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1981, 8, 10);
    private static final StatusEnum STATUS = StatusEnum.WRI;

    @Test
    void creates_all_status_from_file() {
        // given
        final HttpEntity<Object> statusCreationHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        StatusListDtoOut statusListDtoOut = restTemplate.postForObject(URL_SERVICE_STATUS_CREATE_FROM_FILE, statusCreationHttpEntity, StatusListDtoOut.class);

        // then
        assertNotNull(statusListDtoOut);
        assertNotNull(statusListDtoOut.getStatusDtoList());
        assertFalse(statusListDtoOut.getStatusDtoList().isEmpty());
    }

    @Test
    void creates_all_persons_from_file() {
        // given
        final HttpEntity<Object> personsHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        PersonListDtoOut personListDtoOut = restTemplate.postForObject(URL_SERVICE_PERSONS_CREATE_FROM_FILE, personsHttpEntity, PersonListDtoOut.class);

        // then
        assertNotNull(personListDtoOut);
        assertNotNull(personListDtoOut.getPersons());
        assertFalse(personListDtoOut.getPersons().isEmpty());
    }

    @Test
    void when_valid_person_status_dto_in_then_should_return_valid_person_status_dto_out() {
        // given
        final PersonStatusDto personStatusDto = PersonStatusDto.getBuilder()
                .addCode("WRI")
                .addLabel("Writer")
                .addDescription("The writer of the book")
                .build();
        final HttpEntity<PersonStatusDto> personStatusDtoHttpEntity = httpEntityCreator.createHttpEntity(personStatusDto);

        // when
        final PersonStatusDto personStatus = restTemplate.postForObject(URL_SERVICE_PERSON_STATUS_CREATE_ONE, personStatusDtoHttpEntity, PersonStatusDto.class);

        // then
        assertNotNull(personStatus);
        assertEquals(personStatusDto.getCode(), personStatus.getCode());
        assertEquals(personStatusDto.getLabel(), personStatus.getLabel());
        assertEquals(personStatusDto.getDescription(), personStatus.getDescription());
    }

    @Test
    void when_valid_dto_in_for_creating_a_person_then_should_create_it() {
        // given
        final PersonDto personIn = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();
        final HttpEntity<PersonDto> personCreationHttpEntity = httpEntityCreator.createHttpEntity(personIn);

        // when
        PersonCreationDtoOut createdPerson = restTemplate.postForObject(URL_SERVICE_PERSON_CREATE, personCreationHttpEntity, PersonCreationDtoOut.class);

        // then
        assertNotNull(createdPerson);
        assertNotNull(createdPerson.getPersonId());
    }

    @Test
    void when_finding_person_with_an_existing_id_then_should_return_it() {
        // given
        final Long personId = 2L;
        final HttpEntity<Object> findPersonHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        final ResponseEntity<PersonDto> exchange = restTemplate.exchange(URL_SERVICE_PERSON_FIND, HttpMethod.GET, findPersonHttpEntity, PersonDto.class, personId);

        // then
        assertNotNull(exchange);
        assertNotNull(exchange.getBody());
        assertNotNull(exchange.getBody().getPersonCode());
        assertNotNull(exchange.getBody().getFirstName());
        assertNotNull(exchange.getBody().getLastName());
        assertNotNull(exchange.getBody().getBirthDate());
        assertNotNull(exchange.getBody().getStatus());
    }
}
