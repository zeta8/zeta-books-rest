package com.zeta.books.e2e;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.*;
import com.zeta.books.toolbox.HttpEntityCreator;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {BooksApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookServiceE2ETestsIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceE2ETestsIT.class);

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final String MAIN_TITLE_2 = "The Moral Animal";
    private static final String SUB_TITLE_2 = "Why We Are the Way We Are";
    private static final String ISBN_2 = "0-19-506023-8";
    private static final BigDecimal PRICE_2 = new BigDecimal("9.00");

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private HttpEntityCreator httpEntityCreator;

    @Value("${url.service.books.create-one}")
    private String URL_SERVICE_BOOKS_CREATE_ONE;

    @Value("${url.service.books.find}")
    private String URL_SERVICE_BOOKS_FIND;

    @Value("${url.service.books.list}")
    private String URL_SERVICE_BOOKS_LIST;

    @Value("${url.service.books.list-add}")
    private String URL_SERVICE_BOOKS_LIST_ADD;


    @Test
    void when_valid_bookCreationDtoIn_then_should_create_bbo_and_return_valid_bookCreationDtoOut() {
        // given
        final BookCreationDtoIn bookIn = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriter(5L)
                .build();
        final HttpEntity<BookCreationDtoIn> bookInHttpEntity = httpEntityCreator.createHttpEntity(bookIn);
        // when
        final BookCreationDtoOut bookCreationDtoOut = restTemplate.postForObject(URL_SERVICE_BOOKS_CREATE_ONE, bookInHttpEntity, BookCreationDtoOut.class);

        // then
        assertNotNull(bookCreationDtoOut);
        assertNotNull(bookCreationDtoOut.getBookId());
    }

    @Test
    void when_valid_book_id_then_should_return_the_book() {
        // given
        final Long bookId = 1L;
        final HttpEntity<Object> bookHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        ResponseEntity<BookDto> exchange = restTemplate.exchange(URL_SERVICE_BOOKS_FIND, HttpMethod.GET, bookHttpEntity, BookDto.class, bookId);

        // then
        assertEquals(HttpStatus.OK.name(), exchange.getStatusCode().name());
        assertNotNull(exchange);
        assertNotNull(exchange.getBody());
        assertNotNull(exchange.getBody().getMainTitle());
        assertNotNull(exchange.getBody().getIsbn());
        assertNotNull(exchange.getBody().getPrice());
        assertNotNull(exchange.getBody().getWriters());
        assertFalse(exchange.getBody().getWriters().isEmpty());
    }

    @Test
    void should_list_all_existing_books() {
        // given
        final HttpEntity<Object> booksHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        final ResponseEntity<BookDtoOut> exchange = restTemplate.exchange(URL_SERVICE_BOOKS_LIST, HttpMethod.GET, booksHttpEntity, BookDtoOut.class);

        // then
        assertNotNull(exchange);
        assertNotNull(exchange.getBody());
        assertNotNull(exchange.getBody().getBooks());
        LOGGER.info("The body is: {}", exchange.getBody().toString());
        assertFalse(exchange.getBody().getBooks().isEmpty());
        exchange.getBody().getBooks().forEach(entry -> {
            LOGGER.info("The entry is {}", entry);
            assertNotNull(entry.getMainTitle());
            assertNotNull(entry.getIsbn());
            assertNotNull(entry.getPrice());
            assertNotNull(entry.getWriters());
            assertFalse(entry.getWriters().isEmpty());
        });
    }

    @Test
    void when_valid_books_list_given_should_add_and_return_back_the_BookListCreationDtoOut() {
        // given
        final BookCreationDtoIn book_1 = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriter(2L)
                .addWriter(4L)
                .addWriter(9L)
                .build();

        final BookCreationDtoIn book_2 = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE_2)
                .addSubTitle(SUB_TITLE_2)
                .addISBN(ISBN_2)
                .addPrice(PRICE_2)
                .addWriter(4L)
                .addWriter(5L)
                .addWriter(10L)
                .build();

        final List<BookCreationDtoIn> bookCreationDtoInList = new ArrayList<>() {
            {
                add(book_1);
                add(book_2);
            }
        };

        final HttpEntity<List<BookCreationDtoIn>> booksListHttpEntity = httpEntityCreator.createHttpEntity(bookCreationDtoInList);

        // when
        final BookListCreationDtoOut bookListCreationDtoOut = restTemplate.postForObject(URL_SERVICE_BOOKS_LIST_ADD, booksListHttpEntity, BookListCreationDtoOut.class);

        // then
        assertNotNull(bookListCreationDtoOut);
        assertNotNull(bookListCreationDtoOut.getCreatedBooks());
        assertFalse(bookListCreationDtoOut.getCreatedBooks().isEmpty());
        assertEquals(2, bookListCreationDtoOut.getCreatedBooks().size());
        bookListCreationDtoOut.getCreatedBooks().forEach(createdBook -> assertNotNull(createdBook.getBookId()));
    }

    @Test
    void when_finding_book_with_nonExisting_id_then_should_send_Http_Not_Found_Status() {
        // given
        final Long nonExistingId = -1L;
        final HttpEntity<Object> booksListHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        final ResponseEntity<BookDto> exchange = restTemplate.exchange(URL_SERVICE_BOOKS_FIND, HttpMethod.GET, booksListHttpEntity, BookDto.class, nonExistingId);

        // then
        assertEquals(HttpStatus.NOT_FOUND.name(), exchange.getStatusCode().name());
    }
}
