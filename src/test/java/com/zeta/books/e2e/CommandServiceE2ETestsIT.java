package com.zeta.books.e2e;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.BookToBeCommanded;
import com.zeta.books.dtos.CommandCreationDto;
import com.zeta.books.dtos.CommandCreationDtoOut;
import com.zeta.books.dtos.CommandDto;
import com.zeta.books.toolbox.HttpEntityCreator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(classes = {BooksApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CommandServiceE2ETestsIT {

    @Value("${url.service.command.create}")
    private String URL_SERVICE_COMMAND_CREATE;

    @Value("${url.service.command.find}")
    private String URL_SERVICE_COMMAND_FIND;


    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private HttpEntityCreator httpEntityCreator;


    @Test
    void when_valid_CommandCreationDto_with_all_eligible_items_then_should_return_CommandCreationDtoOut_with_empty_nonEligibleItems() {
        // given
        final BookToBeCommanded bookToBeCommanded = BookToBeCommanded.getBuilder()
                .addBook(1L)
                .addNumberOfCopies(1)
                .build();

        final List<BookToBeCommanded> booksToBeCommanded = new ArrayList<>();
        booksToBeCommanded.add(bookToBeCommanded);

        final CommandCreationDto commandCreationDto = CommandCreationDto.getBuilder()
                .addClientId(2L)
                .addCommandedBooks(booksToBeCommanded)
                .build();

        final HttpEntity<CommandCreationDto> commandCreationDtoHttpEntity = httpEntityCreator.createHttpEntity(commandCreationDto);

        // when
        final CommandCreationDtoOut commandCreationDtoOut = restTemplate.postForObject(URL_SERVICE_COMMAND_CREATE, commandCreationDtoHttpEntity, CommandCreationDtoOut.class);

        // then
        assertNotNull(commandCreationDtoOut);
        assertNotNull(commandCreationDtoOut.getCommandId());
        assertNotNull(commandCreationDtoOut.getNonEligibleItems());
        assertTrue(commandCreationDtoOut.getNonEligibleItems().isEmpty());
    }

    @Test
    void when_valid_commandId_then_should_return_valid_CommandDto() {
        // given
        final Long commandId = 1L;
        final HttpEntity<Object> commandHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        final ResponseEntity<CommandDto> exchange = restTemplate.exchange(URL_SERVICE_COMMAND_FIND, HttpMethod.GET, commandHttpEntity, CommandDto.class, commandId);

        // then
        assertNotNull(exchange);
        assertEquals(HttpStatus.OK.name(), exchange.getStatusCode().name());
        assertNotNull(exchange.getBody());
        assertNotNull(exchange.getBody().getTotalPrice());
        assertNotNull(exchange.getBody().getClientId());
        assertNotNull(exchange.getBody().getCommandedBooks());
        exchange.getBody().getCommandedBooks().forEach(commanded -> {
            assertNotNull(commanded.getNumberOfCopies());
            assertNotNull(commanded.getBook());
            assertNotNull(commanded.getBook().getMainTitle());
            assertNotNull(commanded.getBook().getIsbn());
            assertNotNull(commanded.getBook().getPrice());
            assertNotNull(commanded.getBook().getWriters());
            assertFalse(commanded.getBook().getWriters().isEmpty());
        });
    }

    @Test
    void when_finding_command_and_commandId_not_valid_the_should_return_NOT_FOUNd_httpStatus() {
        // given
        final Long nonExistingCommandId = -1L;
        final HttpEntity<Object> findCommandHttpEntity = httpEntityCreator.createHttpEntity(null);

        // when
        final ResponseEntity<CommandDto> exchange = restTemplate.exchange(URL_SERVICE_COMMAND_FIND, HttpMethod.GET, findCommandHttpEntity, CommandDto.class, nonExistingCommandId);

        // then
        assertNotNull(exchange);
        assertEquals(HttpStatus.NOT_FOUND.name(), exchange.getStatusCode().name());
    }
}
