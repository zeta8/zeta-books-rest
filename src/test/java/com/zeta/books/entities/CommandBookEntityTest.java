package com.zeta.books.entities;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class CommandBookEntityTest {

    private static final String PERSON_CODE = "CD-1";
    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static BigDecimal PRICE = new BigDecimal("25.49");

    private static final BookEntity book = BookEntity.getBuilder()
            .addMainTitle(MAIN_TITLE)
            .addSubTitle(SUB_TITLE)
            .addISBN(ISBN)
            .addPrice(PRICE)
            .build();

    private static final String FIRST_NAME = "Eric";
    private static final String LAST_NAME = "Von Dike";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);


    private static final PersonEntity customer = PersonEntity.getBuilder()
            .addPersonCode(PERSON_CODE)
            .addFirstName(FIRST_NAME)
            .addLastName(LAST_NAME)
            .addBirthDate(BIRTH_DATE)
            .build();

    private static final BigDecimal TOTAL_PRICE = new BigDecimal("258.76");

    private static final CommandEntity command = CommandEntity.getBuilder()
            .addPersonEntity(customer)
            .addTotalPrice(TOTAL_PRICE)
            .build();

    @Test
    void when_all_fields_fed_in_properly_then_should_be_ok() {
        // given: Input
        final Integer numberOfCopies = 1;

        // when
        final CommandBookEntity commandBookEntity = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies)
                .build();

        //then
        assertEquals(book, commandBookEntity.getBookEntity());
        assertEquals(command, commandBookEntity.getCommandEntity());
        assertEquals(numberOfCopies, commandBookEntity.getNumberOfCopies());
    }

    @ParameterizedTest
    @NullSource
    void when_book_null_then_should_throw_MandatoryValueException(final BookEntity book) {
        // given: Input
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBookEntity(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_command_null_then_should_throw_MandatoryValueException(final CommandEntity command) {
        // given: Input
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCommandEntity(command));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_numberOfCopies_null_then_should_throw_MandatoryValueException(final Integer numberOfCopies) {
        // given: Input
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addNumberOfCopies(numberOfCopies));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_command_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final Integer numberOfCopies = 1;

        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(numberOfCopies);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBookEntity(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_command_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandEntity(command));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_totalPrice_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandEntity(command));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: Input
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBookEntity(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_command_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: Input
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandEntity(command));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_numberOfCopies_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: Input
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(numberOfCopies));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given: Input
        final Integer numberOfCopies = 1;
        final CommandBookEntity.CommandBookEntityBuilder builder = CommandBookEntity.getBuilder()
                .addBookEntity(book)
                .addCommandEntity(command)
                .addNumberOfCopies(numberOfCopies);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));

    }



}
