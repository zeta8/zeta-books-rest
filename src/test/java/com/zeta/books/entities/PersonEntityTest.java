package com.zeta.books.entities;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PersonEntityTest {

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);


    @Test
    void when_all_fields_fed_correctly_then_should_be_ok() {
        // given: The input

        // when
        final PersonEntity person = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        // Then
        assertEquals(PERSON_CODE, person.getPersonCode());
        assertEquals(FIRST_NAME, person.getFirstName());
        assertEquals(MIDDLE_NAME, person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate().toLocalDate());
    }

    @Test
    void when_middle_name_not_fed_then_should_be_ok() {
        // given: The input

        // when
        final PersonEntity person = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        // Then
        assertEquals(FIRST_NAME, person.getFirstName());
        assertNull(person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate().toLocalDate());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_person_code_null_or_empty_then_should_throw_MandatoryValueException(final String personCode) {
        // given
        final PersonEntity.PersonEntityBuilder builder = PersonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPersonCode(personCode));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_first_name_null_or_empty_then_should_throw_MandatoryValueException(final String firstName) {
        // given
        final PersonEntity.PersonEntityBuilder builder = PersonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addFirstName(firstName));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_middle_name_null_or_empty_then_should_be_ok(final String middleName) {
        // given: Input

        // when
        final PersonEntity person = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(middleName)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        // Then
        assertEquals(FIRST_NAME, person.getFirstName());
        assertNull(person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate().toLocalDate());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_last_name_null_or_empty_then_should_throw_MandatoryValueException(final String lastName) {
        // given
        final PersonEntity.PersonEntityBuilder builder = PersonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addLastName(lastName));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_birthDate_null_then_should_throw_MandatoryValueException(final LocalDate birthDate) {
        // given
        final PersonEntity.PersonEntityBuilder builder = PersonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBirthDate(birthDate));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_code_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, personEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_first_name_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, personEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_last_name_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, personEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_birth_date_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, personEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_code_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);
        personEntityBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addPersonCode("ANOTHER CODE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_first_name_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);
        personEntityBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addFirstName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_middle_name_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);
        personEntityBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addMiddleName(MIDDLE_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_last_name_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);
        personEntityBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addLastName(LAST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_birth_date_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);
        personEntityBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addBirthDate(BIRTH_DATE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_person_code_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addPersonCode("CODE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_first_name_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addFirstName(FIRST_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addFirstName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_middle_name_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addMiddleName(MIDDLE_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addMiddleName("FIRST_NAME"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_last_name_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addLastName(LAST_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addLastName("SOME Other last name"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_birth_date_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addBirthDate(BIRTH_DATE);

        // when
        final LocalDate anotherBirthdate = LocalDate.of(2020, 1, 1);
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> personEntityBuilder.addBirthDate(anotherBirthdate));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonEntity.PersonEntityBuilder personEntityBuilder = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);
        personEntityBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, personEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
