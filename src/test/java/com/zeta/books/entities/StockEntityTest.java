package com.zeta.books.entities;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class StockEntityTest {

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final BookEntity book = BookEntity.getBuilder()
            .addMainTitle(MAIN_TITLE)
            .addSubTitle(SUB_TITLE)
            .addISBN(ISBN)
            .addPrice(PRICE)
            .build();

    private static final Integer NUMBER_OF_COPIES = 25;


    @Test
    void when_all_fields_fed_in_correctly_then_should_be_ok() {
        // given: Input

        // when
        final StockEntity stock = StockEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(NUMBER_OF_COPIES)
                .build();

        // then
        assertEquals(book, stock.getBookEntity());
        assertEquals(NUMBER_OF_COPIES, stock.getNumberOfCopies());
    }

    @ParameterizedTest
    @NullSource
    void when_book_fed_null_then_should_throw_MandatoryValueException(final BookEntity book) {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBookEntity(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_numberOfCopies_fed_null_then_should_throw_MandatoryValueException(final Integer numberOfCopies) {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addNumberOfCopies(numberOfCopies));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addBookEntity(book);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBookEntity(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_numberOfCopies_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NUMBER_OF_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBookEntity(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_addNumberOfCopies_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NUMBER_OF_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StockEntity.StockEntityBuilder builder = StockEntity.getBuilder()
                .addBookEntity(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
