package com.zeta.books.entities;

import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.*;

class NonEligibilityReasonEntityTest {

    private static final NonEligibilityReasonEnum reason = NonEligibilityReasonEnum.NOT_FOUND;

    @Test
    void when_all_fed_correctly_then_should_be_ok() {
        // given: The input

        // when
        final NonEligibilityReasonEntity build = NonEligibilityReasonEntity.getBuilder()
                .addCode(reason.name())
                .addLabel(reason.getLabel())
                .addDescription(reason.getDescription())
                .build();

        // then
        assertNotNull(build);
        assertEquals(reason.name(), build.getCode());
        assertEquals(reason.getLabel(), build.getLabel());
        assertEquals(reason.getDescription(), build.getDescription());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_code_fed_null_or_empty_should_throw_MandatoryValueException(final String code) {
        // given
        final NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder = NonEligibilityReasonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCode(code));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_label_fed_null_or_empty_should_throw_MandatoryValueException(final String label) {
        // given
        final NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder = NonEligibilityReasonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addLabel(label));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_description_fel_null_or_empty_should_throw_MandatoryValueException(final String description) {
        // given
        final NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder = NonEligibilityReasonEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addDescription(description));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_code_not_fed_should_throw_MandatoryValueException() {
        // given
        final NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_label_not_fed_should_throw_MandatoryValueException() {
        // given
        final NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addDescription(reason.getDescription());

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_description_not_fed_should_throw_MandatoryValueException() {
        // given
        final NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel());

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_code_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());

        // when
        final String code = reason.name();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCode(code));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_label_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());

        // when
        final String label = reason.getLabel();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLabel(label));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_description_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());

        // when
        final String description = reason.getDescription();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addDescription(description));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_code_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());
        builder.build();

        // when
        final String code = reason.name();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCode(code));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_label_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());
        builder.build();

        // when
        final String label = reason.getLabel();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLabel(label));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_description_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());
        builder.build();

        // when
        final String description = reason.getDescription();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLabel(description));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        NonEligibilityReasonEntity.NonEligibilityReasonEntityBuilder builder =
                NonEligibilityReasonEntity.getBuilder()
                        .addCode(reason.name())
                        .addLabel(reason.getLabel())
                        .addDescription(reason.getDescription());
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class,  builder::build);

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
