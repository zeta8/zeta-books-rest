package com.zeta.books.entities;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class StatusEntityTest {

    private static final StatusEnum status = StatusEnum.CUS;

    @Test
    void when_all_fields_fed_correctly_then_should_be_ok() {
        // given: The input (status)
        // when
        final StatusEntity statusEntity = StatusEntity.getBuilder()
                .addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();

        // then
        assertEquals(status.name(), statusEntity.getCode());
        assertEquals(status.getLabel(), statusEntity.getLabel());
        assertEquals(status.getDescription(), statusEntity.getDescription());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_status_code_fed_null_or_empty_then_should_throw_MandatoryValueException(final String code) {
        // given: the input
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCode(code));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_status_label_fed_null_or_empty_then_should_throw_MandatoryValueException(final String label) {
        // given: the input
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addLabel(label));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_status_description_fed_null_or_empty_then_should_throw_MandatoryValueException(final String description) {
        // given: the input
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addDescription(description));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_code_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusEntity.StatusEntityBuilder statusEntityBuilder = StatusEntity.getBuilder()
                .addLabel(status.getLabel())
                .addDescription(status.getDescription());

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, statusEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_label_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusEntity.StatusEntityBuilder statusEntityBuilder = StatusEntity.getBuilder()
                .addCode(status.name())
                .addDescription(status.getDescription());

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, statusEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_description_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusEntity.StatusEntityBuilder statusEntityBuilder = StatusEntity.getBuilder()
                .addCode(status.name())
                .addLabel(status.getLabel());

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, statusEntityBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_code_fed_after_build_should_throw_ZetaIllegalActionException() {
        // given
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();
        builder.addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();
        // when
        final String code = status.name();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCode(code));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_label_fed_after_build_should_throw_ZetaIllegalActionException() {
        // given
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();
        builder.addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();
        // when
        final String label = status.getLabel();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLabel(label));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_description_fed_after_build_should_throw_ZetaIllegalActionException() {
        // given
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();
        builder.addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();
        // when
        final String description = status.getDescription();
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addDescription(description));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_code_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final String code = status.name();
        final StatusEntity.StatusEntityBuilder statusEntityBuilder = StatusEntity.getBuilder()
                .addCode(code);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> statusEntityBuilder.addCode(code));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_label_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final String label = status.getLabel();
        final StatusEntity.StatusEntityBuilder statusEntityBuilder = StatusEntity.getBuilder()
                .addLabel(label);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> statusEntityBuilder.addLabel(label));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_description_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final String description = status.getDescription();
        final StatusEntity.StatusEntityBuilder statusEntityBuilder = StatusEntity.getBuilder()
                .addDescription(description);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> statusEntityBuilder.addDescription(description));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusEntity.StatusEntityBuilder builder = StatusEntity.getBuilder();
        builder.addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }

}
