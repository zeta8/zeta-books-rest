package com.zeta.books.entities;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

class CommandEntityTest {

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Eric";
    private static final String LAST_NAME = "Von Dike";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);
    private static BigDecimal TOTAL_PRICE = new BigDecimal("258.76");

    private static final PersonEntity customer = PersonEntity.getBuilder()
            .addPersonCode(PERSON_CODE)
            .addFirstName(FIRST_NAME)
            .addLastName(LAST_NAME)
            .addBirthDate(BIRTH_DATE)
            .build();

    @Test
    void when_all_fields_fed_in_properly_then_should_be_ok() {
        // given: Input

        // when
        final CommandEntity build = CommandEntity.getBuilder()
                .addPersonEntity(customer)
                .addTotalPrice(TOTAL_PRICE)
                .build();

        // then
        assertEquals(customer, build.getPersonEntity());
        assertEquals(TOTAL_PRICE, build.getTotalPrice());
    }

    @ParameterizedTest
    @NullSource
    void when_person_fed_null_then_should_throw_MandatoryValueException(final PersonEntity customer) {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPersonEntity(customer));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_total_price_fed_null_then_should_throw_MandatoryValueException(final BigDecimal totalPrice) {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addTotalPrice(totalPrice));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addTotalPrice(TOTAL_PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_total_price_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addPersonEntity(customer);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addPersonEntity(customer)
                .addTotalPrice(TOTAL_PRICE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonEntity(customer));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_total_price_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addPersonEntity(customer)
                .addTotalPrice(TOTAL_PRICE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addTotalPrice(TOTAL_PRICE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_person_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addPersonEntity(customer)
                .addTotalPrice(TOTAL_PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonEntity(customer));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_total_price_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addPersonEntity(customer)
                .addTotalPrice(TOTAL_PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addTotalPrice(TOTAL_PRICE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_should_throw_ZetaIllegalActionException() {
        // given
        final CommandEntity.CommandEntityBuilder builder = CommandEntity.getBuilder()
                .addPersonEntity(customer)
                .addTotalPrice(TOTAL_PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }


}
