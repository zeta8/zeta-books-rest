package com.zeta.books.entities;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class BookEntityTest {
    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");


    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final BookEntity book = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .build();

        // then
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertEquals(SUB_TITLE, book.getSubTitle());
        assertEquals(PRICE, book.getPrice());
        assertEquals(ISBN, book.getIsbn());
    }

    @Test
    void when_sub_title_not_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        BookEntity book = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .build();

        // then
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertNull(book.getSubTitle());
        assertEquals(PRICE, book.getPrice());
        assertEquals(ISBN, book.getIsbn());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_main_title_null_or_empty_then_should_throw_MandatoryValueException(final String mainTitle) {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addMainTitle(mainTitle));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_sub_title_null_or_empty_then_should_be_ok(final String subtitle) {
        // given: Input

        // when
        final BookEntity book = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(subtitle)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .build();

        // then
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertNull(book.getSubTitle());
        assertEquals(PRICE, book.getPrice());
        assertEquals(ISBN, book.getIsbn());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_ISBN_null_or_empty_then_should_throw_MandatoryValueException(final String ISBN) {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addISBN(ISBN));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_price_null_then_should_throw_MandatoryValueException(final BigDecimal price) {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPrice(price));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_main_title_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_ISBN_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addPrice(PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_price_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_main_title_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMainTitle("ANOTHER MAIN TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_sub_title_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addSubTitle("ANOTHER SUB TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_ISBN_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addISBN("ANOTHER ISBN"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_price_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);
        builder.build();

        // when
        final BigDecimal price = new BigDecimal("15.22");
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPrice(price));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_main_title_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMainTitle("ANOTHER MAIN TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_sub_title_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addSubTitle("ANOTHER SUB TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_ISBN_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addISBN("ANOTHER ISBN"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_price_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);

        // when
        final BigDecimal price = new BigDecimal("15.22");
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPrice(price));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookEntity.BookEntityBuilder builder = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
