package com.zeta.books.entities;


import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PersonStatusEntityTest {

    private static final StatusEnum status = StatusEnum.WRI;
    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);

    private static final PersonEntity person = PersonEntity.getBuilder()
            .addPersonCode(PERSON_CODE)
            .addFirstName(FIRST_NAME)
            .addMiddleName(MIDDLE_NAME)
            .addLastName(LAST_NAME)
            .addBirthDate(BIRTH_DATE)
            .build();

    private static final StatusEntity personStatus = StatusEntity.getBuilder()
            .addCode(status.name())
            .addLabel(status.getLabel())
            .addDescription(status.getDescription())
            .build();

    @Test
    void when_all_fields_correctly_fed_then_should_be_ok() {
        // given: The inputs
        // when
        final PersonStatusEntity build = PersonStatusEntity.getBuilder()
                .addPersonEntity(person)
                .addStatusEntity(personStatus)
                .build();

        // then
        assertEquals(person, build.getPersonEntity());
        assertEquals(personStatus, build.getStatusEntity());
    }

    @ParameterizedTest
    @NullSource
    void when_person_entity_fed_null_then_should_throw_MandatoryValueException(final PersonEntity personEntity) {

        // given
        PersonStatusEntity.PersonStatusEntityBuilder builder = PersonStatusEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPersonEntity(personEntity));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @ParameterizedTest
    @NullSource
    void when_status_entity_fed_null_then_should_throw_MandatoryValueException(final StatusEntity statusEntity) {

        // given
        PersonStatusEntity.PersonStatusEntityBuilder builder = PersonStatusEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addStatusEntity(statusEntity));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_person_entity_not_fed_then_should_throw_MandatoryValueException() {

        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                .addStatusEntity(personStatus);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_status_entity_not_fed_then_should_throw_MandatoryValueException() {

        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                        .addPersonEntity(person);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_person_entity_fed_after_build_then_should_throw_ZetaIllegalActionException() {

        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                        .addPersonEntity(person)
                        .addStatusEntity(personStatus);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonEntity(person));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));

    }

    @Test
    void when_status_entity_fed_after_build_then_should_throw_ZetaIllegalActionException() {

        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                        .addPersonEntity(person)
                        .addStatusEntity(personStatus);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addStatusEntity(personStatus));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));

    }

    @Test
    void when_person_entity_fed_twice_then_should_throw_ZetaIllegalActionException() {

        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                        .addPersonEntity(person);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonEntity(person));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));

    }

    @Test
    void when_status_entity_fed_twice_then_should_throw_ZetaIllegalActionException() {

        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                        .addStatusEntity(personStatus);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addStatusEntity(personStatus));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));

    }

    @Test
    void when_build_twice_hen_should_throw_ZetaIllegalActionException() {
        // given
        final PersonStatusEntity.PersonStatusEntityBuilder builder =
                PersonStatusEntity.getBuilder()
                        .addPersonEntity(person)
                        .addStatusEntity(personStatus);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
