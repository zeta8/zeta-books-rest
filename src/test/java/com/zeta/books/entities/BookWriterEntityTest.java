package com.zeta.books.entities;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class BookWriterEntityTest {

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final BookEntity book = BookEntity.getBuilder()
            .addMainTitle(MAIN_TITLE)
            .addSubTitle(SUB_TITLE)
            .addISBN(ISBN)
            .addPrice(PRICE)
            .build();

    private static final String PERSON_CODE_1 = "CD-1";
    private static final String FIRST_NAME_1 = "Jerome";
    private static final String MIDDLE_NAME_1 = "H.";
    private static final String LAST_NAME_1 = "Barkow";
    private static final LocalDate BIRTH_DATE_1 = LocalDate.of(1945, 1, 27);

    private static final PersonEntity writer = PersonEntity.getBuilder()
            .addPersonCode(PERSON_CODE_1)
            .addFirstName(FIRST_NAME_1)
            .addMiddleName(MIDDLE_NAME_1)
            .addLastName(LAST_NAME_1)
            .addBirthDate(BIRTH_DATE_1)
            .build();

    private static final String PERSON_CODE_2 = "CD-2";
    private static final String FIRST_NAME_2 = "Leda";
    private static final String LAST_NAME_2 = "Cosmides";
    private static final LocalDate BIRTH_DATE_2 = LocalDate.of(1957, 5, 9);

    private static final PersonEntity secondWriter = PersonEntity.getBuilder()
            .addPersonCode(PERSON_CODE_2)
            .addFirstName(FIRST_NAME_2)
            .addLastName(LAST_NAME_2)
            .addBirthDate(BIRTH_DATE_2)
            .build();

    private static final String PERSON_CODE_3 = "CD-3";
    private static final String FIRST_NAME_3 = "John";
    private static final String LAST_NAME_3 = "Tooby";
    private static final LocalDate BIRTH_DATE_3 = LocalDate.of(1952, 7, 26);

    private static final PersonEntity thirdWriter = PersonEntity.getBuilder()
            .addPersonCode(PERSON_CODE_3)
            .addFirstName(FIRST_NAME_3)
            .addLastName(LAST_NAME_3)
            .addBirthDate(BIRTH_DATE_3)
            .build();

    @Test
    void when_all_fields_fed_in_properly_then_should_be_ok() {
        // given: Input

        // when
        final BookWriterEntity build = BookWriterEntity.getBuilder()
                .addBook(book)
                .addWriter(writer)
                .build();

        //then
        assertEquals(book, build.getBookEntity());
        assertEquals(writer, build.getPersonEntity());
    }

    @ParameterizedTest
    @NullSource
    void when_book_fed_null_then_should_throw_MandatoryValueException(final BookEntity book) {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBook(book));

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_writer_fed_null_then_should_throw_MandatoryValueException(final PersonEntity writer) {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addWriter(writer));

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_not_fed_null_then_should_throw_MandatoryValueException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addWriter(writer);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_writer_not_fed_null_then_should_throw_MandatoryValueException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addBook(book);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_writer_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addBook(book)
                .addWriter(writer);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addWriter(writer));

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addBook(book)
                .addWriter(writer);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addBook(book)
                .addWriter(writer);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_writer_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addBook(book)
                .addWriter(writer);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addWriter(writer));

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookWriterEntity.BookWriterEntityBuilder builder = BookWriterEntity.getBuilder()
                .addBook(book)
                .addWriter(writer);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        //then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }


}
