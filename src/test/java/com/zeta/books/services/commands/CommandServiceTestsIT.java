package com.zeta.books.services.commands;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.BookToBeCommanded;
import com.zeta.books.dtos.CommandCreationDto;
import com.zeta.books.dtos.CommandCreationDtoOut;
import com.zeta.books.dtos.CommandDto;
import com.zeta.books.entities.*;
import com.zeta.books.entities.embeddables.BookWriterId;
import com.zeta.books.entities.embeddables.CommandBookId;
import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.exceptions.ResourceNotFoundException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.repositories.*;
import com.zeta.books.services.CommandServiceImpl;
import com.zeta.books.services.interfaces.CommandService;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BooksApplication.class, CommandServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CommandServiceTestsIT {

    @Autowired
    private CommandService commandService;

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Jerome";
    private static final String MIDDLE_NAME = "H.";
    private static final String LAST_NAME = "Barkow";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");



    private static CommandCreationDto commandCreationDto;
    private static CommandCreationDto commandCreationDtoWithOneNonExistingBook;


    private static List<BookToBeCommanded> toBeCommandedList;
    private static PersonEntity existingPersonEntity;
    private static BookEntity existingBookEntity;
    private static StockEntity existingStockEntity;
    private static CommandEntity commandEntityBeforeSave;
    private static CommandEntity commandEntityAfterSave;
    private static CommandBookEntity commandBookEntity;
    private static StockEntity stockEntity;
    private static StockEntity stockEntityAfterCommand;
    private static List<CommandBookEntity> foundCommandBookEntityList;
    private static List<BookWriterEntity> bookWriterEntities;

    private static final Long EXISTING_CLIENT_ID = 1L;
    private static final Long EXISTING_BOOK_ID = 1L;
    private static final Integer VALID_NUMBER_OF_COPIES = 1;
    private static final Long COMMAND_ID = 1L;
    private static final Long STOCK_ENTITY_ID = 1L;
    private static final Long EXISTING_COMMAND_ID = 1L;

    private static final NonEligibilityReasonEnum NOT_FOUND_REASON = NonEligibilityReasonEnum.NOT_FOUND;
    private static NonEligibilityReasonEntity nonEligibilityReasonEntity;

    @BeforeAll
    static void init() {
        final BookToBeCommanded bookToBeCommanded;
        bookToBeCommanded = BookToBeCommanded.getBuilder()
                .addBook(EXISTING_BOOK_ID)
                .addNumberOfCopies(VALID_NUMBER_OF_COPIES)
                .build();

        toBeCommandedList = new ArrayList<>();
        toBeCommandedList.add(bookToBeCommanded);

        commandCreationDto = CommandCreationDto.getBuilder()
                .addClientId(EXISTING_CLIENT_ID)
                .addCommandedBooks(toBeCommandedList)
                .build();

        final List<BookToBeCommanded> toBeCommandedListWithOneNonExistingBook;
        toBeCommandedListWithOneNonExistingBook = new ArrayList<>();
        final BookToBeCommanded bookToBeCommandedNonExisting;
        bookToBeCommandedNonExisting = BookToBeCommanded.getBuilder()
                .addBook(-1L)
                .addNumberOfCopies(2)
                .build();

        toBeCommandedListWithOneNonExistingBook.add(bookToBeCommanded);
        toBeCommandedListWithOneNonExistingBook.add(bookToBeCommandedNonExisting);

        commandCreationDtoWithOneNonExistingBook = CommandCreationDto.getBuilder()
                .addClientId(EXISTING_CLIENT_ID)
                .addCommandedBooks(toBeCommandedListWithOneNonExistingBook)
                .build();

        existingPersonEntity = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        existingBookEntity = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .build();

        existingStockEntity = StockEntity.getBuilder()
                .addBookEntity(existingBookEntity)
                .addNumberOfCopies(VALID_NUMBER_OF_COPIES + 1)
                .build();

        commandEntityBeforeSave = CommandEntity.getBuilder()
                .addPersonEntity(existingPersonEntity)
                .addTotalPrice(existingBookEntity.getPrice().multiply(new BigDecimal(VALID_NUMBER_OF_COPIES)))
                .build();

        commandEntityAfterSave = CommandEntity.getBuilder()
                .addPersonEntity(existingPersonEntity)
                .addTotalPrice(existingBookEntity.getPrice().multiply(new BigDecimal(VALID_NUMBER_OF_COPIES)))
                .build();

        commandBookEntity = CommandBookEntity.getBuilder()
                .addBookEntity(existingBookEntity)
                .addCommandEntity(commandEntityAfterSave)
                .addNumberOfCopies(VALID_NUMBER_OF_COPIES)
                .build();

        final CommandBookEntity foundCommandBookEntity;
        foundCommandBookEntity = CommandBookEntity.getBuilder()
                .addBookEntity(existingBookEntity)
                .addCommandEntity(commandEntityAfterSave)
                .addNumberOfCopies(VALID_NUMBER_OF_COPIES)
                .build();

        final CommandBookId commandBookId = CommandBookId.getNewInstance();
        commandBookId.setBookId(EXISTING_BOOK_ID);
        commandBookId.setCommandId(EXISTING_COMMAND_ID);

        foundCommandBookEntityList = new ArrayList<>();
        foundCommandBookEntityList.add(foundCommandBookEntity);

        stockEntity = StockEntity.getBuilder()
                .addBookEntity(existingBookEntity)
                .addNumberOfCopies(VALID_NUMBER_OF_COPIES + 1)
                .build();

        stockEntityAfterCommand = StockEntity.getBuilder()
                .addBookEntity(existingBookEntity)
                .addNumberOfCopies(stockEntity.getNumberOfCopies() - 1)
                .build();

        final BookWriterEntity bookWriterEntity = BookWriterEntity.getBuilder()
                .addWriter(existingPersonEntity)
                .addBook(existingBookEntity)
                .build();

        bookWriterEntities = new ArrayList<>();
        bookWriterEntities.add(bookWriterEntity);

        final BookWriterId bookWriterId = BookWriterId.getNewInstance();
        bookWriterId.setBookId(EXISTING_BOOK_ID);
        bookWriterId.setWriterId(EXISTING_CLIENT_ID);

        nonEligibilityReasonEntity = NonEligibilityReasonEntity.getBuilder()
                .addCode(NOT_FOUND_REASON.name())
                .addLabel(NOT_FOUND_REASON.getLabel())
                .addDescription(NOT_FOUND_REASON.getDescription())
                .build();

        final Class existingPersonEntityClass = existingPersonEntity.getClass();
        try {
            final Field f = existingPersonEntityClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(existingPersonEntity, EXISTING_CLIENT_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class existingBookClass = existingBookEntity.getClass();
        try {
            final Field f = existingBookClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(existingBookEntity, EXISTING_BOOK_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class commandEntityClass = commandEntityAfterSave.getClass();
        try {
            final Field f = commandEntityClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(commandEntityAfterSave, COMMAND_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class stockEntityClass = stockEntity.getClass();
        try {
            final Field f = stockEntityClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(stockEntity, STOCK_ENTITY_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class stockEntityAfterCommandClass = stockEntityAfterCommand.getClass();
        try {
            final Field f = stockEntityAfterCommandClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(stockEntityAfterCommand, stockEntity.getId());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class foundCommandBookEntityClass = foundCommandBookEntity.getClass();
        try {
            final Field f = foundCommandBookEntityClass.getDeclaredField("commandBookId");
            f.setAccessible(true);
            f.set(foundCommandBookEntity, commandBookId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class bookWriterEntityClass = bookWriterEntity.getClass();
        try {
            final Field f = bookWriterEntityClass.getDeclaredField("bookWriterId");
            f.setAccessible(true);
            f.set(bookWriterEntity, bookWriterId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class notFoundReasonClass = nonEligibilityReasonEntity.getClass();
        try {
            final Field f = notFoundReasonClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(nonEligibilityReasonEntity, 1L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void when_creating_command_with_valid_input_then_should_return_valid_output() {
        // given: CommandCreationDtoIn

        // when
        final CommandCreationDtoOut command = commandService.createCommand(commandCreationDto);

        // then
        assertNotNull(command);
        assertEquals(COMMAND_ID, command.getCommandId());
        assertNull(command.getMessage());
        assertTrue(command.getNonEligibleItems().isEmpty());
    }

    @Test
    void when_creating_command_containing_one_non_existing_book_then_should_return_message_and_nonEligibleItemsList() {
        // given: commandCreationDto with a non existing book

        // when
        final CommandCreationDtoOut command = commandService.createCommand(commandCreationDtoWithOneNonExistingBook);

        // then
        assertNotNull(command);
        assertEquals(COMMAND_ID, command.getCommandId());
        assertNotNull(command.getMessage());
        assertFalse(command.getNonEligibleItems().isEmpty());
        assertEquals(1, command.getNonEligibleItems().size());
        assertEquals(NonEligibilityReasonEnum.NOT_FOUND, command.getNonEligibleItems().get(0).getReason());
    }

    @Test
    void when_creating_command_and_client_id_not_found_then_should_throw_ResourceNotFoundException() {
        // given: non-existing client id
        final Long NON_EXISTING_CLIENT_ID = -1L;
        final CommandCreationDto commandCreationDto = CommandCreationDto.getBuilder()
                .addClientId(NON_EXISTING_CLIENT_ID)
                .addCommandedBooks(toBeCommandedList)
                .build();

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> commandService.createCommand(commandCreationDto));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_none_of_commanded_books_exists_then_should_throw_ZetaIllegalActionException() {
        // given: non-existent book id
        final Long NON_EXISTING_BOOK_ID = -1L;
        final List<BookToBeCommanded> toBeCommandeds = new ArrayList<>();
        BookToBeCommanded bookToBeCommanded = BookToBeCommanded.getBuilder()
                .addBook(NON_EXISTING_BOOK_ID)
                .addNumberOfCopies(2)
                .build();
        toBeCommandeds.add(bookToBeCommanded);
        final CommandCreationDto commandCreationDto = CommandCreationDto.getBuilder()
                .addClientId(EXISTING_CLIENT_ID)
                .addCommandedBooks(toBeCommandeds)
                .build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> commandService.createCommand(commandCreationDto));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ILLEGAL_COMMAND_MSG));
    }

    @Test
    void when_finding_command_with_valid_id_then_should_be_ok() {
        // given: valid command id

        // when
        final CommandDto commandById = commandService.findCommandById(EXISTING_COMMAND_ID);

        // then
        assertNotNull(commandById);
        assertNotNull(commandById.getClientId());
        assertNotNull(commandById.getTotalPrice());
        assertNotNull(commandById.getCommandedBooks());
        assertFalse(commandById.getCommandedBooks().isEmpty());
    }

    @Test
    void when_finding_command_with_non_existent_id_then_should_throw_ResourceNotFoundException() {
        // given: a non-existent command id
        final Long NON_EXISTING_COMMAND_ID = -1L;

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> commandService.findCommandById(NON_EXISTING_COMMAND_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        CommandService commandService() {
            final CommandsRepository commandsRepository = mock(CommandsRepository.class);
            final CommandBookRepository commandBookRepository = mock(CommandBookRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);
            final NonEligibleItemRepository nonEligibleItemRepository = mock(NonEligibleItemRepository.class);
            final NonEligibilityReasonRepository nonEligibilityReasonRepository = mock(NonEligibilityReasonRepository.class);

            when(personRepository.findById(EXISTING_CLIENT_ID)).thenReturn(Optional.of(existingPersonEntity));
            when(booksRepository.findById(EXISTING_BOOK_ID)).thenReturn(Optional.of(existingBookEntity));
            when(stockRepository.findByBookEntity(existingBookEntity)).thenReturn(Optional.of(existingStockEntity));
            when(commandsRepository.save(commandEntityBeforeSave)).thenReturn(commandEntityAfterSave);
            when(commandBookRepository.save(commandBookEntity)).thenReturn(commandBookEntity);
            when(stockRepository.findByBookEntity(existingBookEntity)).thenReturn(Optional.of(stockEntity));
            when(stockRepository.save(stockEntity)).thenReturn(stockEntityAfterCommand);
            when(commandsRepository.findById(EXISTING_COMMAND_ID)).thenReturn(Optional.of(commandEntityAfterSave));
            when(commandBookRepository.findByCommandEntity(commandEntityAfterSave)).thenReturn(Optional.of(foundCommandBookEntityList));
            when(bookWriterRepository.findByBookEntity(existingBookEntity)).thenReturn(Optional.of(bookWriterEntities));
            when(nonEligibilityReasonRepository.findByCode(NOT_FOUND_REASON.name())).thenReturn(Optional.of(nonEligibilityReasonEntity));

            return new CommandServiceImpl(commandsRepository, commandBookRepository, personRepository, stockRepository, booksRepository, bookWriterRepository, nonEligibleItemRepository, nonEligibilityReasonRepository);
        }
    }
}
