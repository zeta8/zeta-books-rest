package com.zeta.books.services.persons;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.PersonCreationDtoOut;
import com.zeta.books.dtos.PersonDto;
import com.zeta.books.dtos.PersonStatusDto;
import com.zeta.books.entities.PersonEntity;
import com.zeta.books.entities.PersonStatusEntity;
import com.zeta.books.entities.StatusEntity;
import com.zeta.books.entities.embeddables.PersonStatusId;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.ResourceNotFoundException;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.PersonStatusRepository;
import com.zeta.books.repositories.StatusRepository;
import com.zeta.books.services.PersonServiceImpl;
import com.zeta.books.services.interfaces.PersonService;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest(classes = {BooksApplication.class, PersonServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PersonServiceTestsIT {

    @Autowired
    private PersonService personService;

    private static PersonEntity person;
    private static PersonEntity nonExistentPerson;
    private static StatusEntity statusEntity;
    private static PersonStatusEntity personStatusEntity;
    private static PersonDto personDto;
    private static PersonStatusDto personStatusDto;

    private static final StatusEnum status = StatusEnum.CUS;
    private static final Long PERSON_ID = 1L;
    private static final Long NON_EXISTENT_PERSON_ID = -1L;
    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);

    private static final String PERSON_CODE_TO_BE_CREATED = "CD-1";
    private static final String FIRST_NAME_TO_BE_CREATED = "Issac";
    private static final String LAST_NAME_TO_BE_CREATED = "NEWTON";
    private static final LocalDate BIRTH_DATE_TO_BE_CREATED = LocalDate.of(1643, 1, 4);
    private static final Long PERSON_CREATED_ID = 1L;
    private static final String personTobBeCreatedStatus = "CUS";

    private static PersonEntity personEntityToBeCreated;
    private static PersonEntity personEntityCreated;
    private static PersonStatusEntity  toBeCreatedPersonStatusEntity;
    private static PersonStatusEntity createdPersonStatusEntity;

    private static final StatusEnum nonExistentStatus = StatusEnum.WRI;
    private static StatusEntity writerStatusToBeAdded;
    private static PersonDto personDtoWithWithNonExistentStatus;

    @BeforeAll
    static void init() {

        personStatusDto = PersonStatusDto.getBuilder()
                .addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();


        person = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        nonExistentPerson = PersonEntity.getBuilder()
                .addPersonCode("CD-3")
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        statusEntity = StatusEntity.getBuilder()
                .addCode(status.name())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build();



        personStatusEntity = PersonStatusEntity.getBuilder()
                .addPersonEntity(person)
                .addStatusEntity(statusEntity)
                .build();

        personDto = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(status)
                .build();

        personDtoWithWithNonExistentStatus = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(nonExistentStatus)
                .build();

        personEntityToBeCreated = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_TO_BE_CREATED)
                .addFirstName(FIRST_NAME_TO_BE_CREATED)
                .addLastName(LAST_NAME_TO_BE_CREATED)
                .addBirthDate(BIRTH_DATE_TO_BE_CREATED)
                .build();

        personEntityCreated = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_TO_BE_CREATED)
                .addFirstName(FIRST_NAME_TO_BE_CREATED)
                .addLastName(LAST_NAME_TO_BE_CREATED)
                .addBirthDate(BIRTH_DATE_TO_BE_CREATED)
                .build();

        StatusEnum statusEnum = StatusEnum.valueOf(personTobBeCreatedStatus);
        writerStatusToBeAdded = StatusEntity.getBuilder()
                .addCode(statusEnum.name())
                .addLabel(statusEnum.getLabel())
                .addDescription(statusEnum.getDescription())
                .build();

        toBeCreatedPersonStatusEntity = PersonStatusEntity.getBuilder()
                .addStatusEntity(writerStatusToBeAdded)
                .addPersonEntity(personEntityCreated)
                .build();

        createdPersonStatusEntity = PersonStatusEntity.getBuilder()
                .addStatusEntity(writerStatusToBeAdded)
                .addPersonEntity(personEntityCreated)
                .build();


        // Here reflection is used to set the value of the id for the person entity
        final Class clazz = person.getClass();
        try {
            Field f = clazz.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(person, PERSON_ID);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        final Class personEntityAfterCreation = personEntityCreated.getClass();
        try {
            Field f = personEntityAfterCreation.getDeclaredField("id");
            f.setAccessible(true);
            f.set(personEntityCreated, PERSON_CREATED_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        PersonStatusId personStatusId = PersonStatusId.getNewInstance();
        personStatusId.setPersonId(PERSON_CREATED_ID);
        personStatusId.setStatusId(1L);

        final Class createdPersonStatus = createdPersonStatusEntity.getClass();
        try {
            final Field f = createdPersonStatus.getDeclaredField("personStatusId");
            f.setAccessible(true);
            f.set(createdPersonStatusEntity, personStatusId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Test
    void when_all_fed_in_properly_should_create_person() {
        // given: Input and mocks

        // when
        PersonCreationDtoOut person = personService.createPerson(personDto);

        // then
        assertNotNull(person);
        assertEquals(PERSON_ID, person.getPersonId());
    }

    @Test
    void when_creating_person_and_status_entity_not_found_then_should_throw_ResourceNotFoundException() {
        // given
        final PersonDto personDto = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(StatusEnum.WRI)
                .build();

        //
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.createPerson(personDto));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }


    @Test
    void finding_person_when_person_entity_not_found_then_should_throw_ResourceNotFoundException() {
        // given: Input

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.findPersonById(NON_EXISTENT_PERSON_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void given_id_should_find_person() {
        // given: Input and mocks

        // when
        PersonDto person = personService.findPersonById(PERSON_ID);

        // then
        assertNotNull(person);
        assertEquals(PERSON_CODE, person.getPersonCode());
        assertEquals(FIRST_NAME, person.getFirstName());
        assertEquals(MIDDLE_NAME, person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate());
        assertEquals(status, person.getStatus());
    }

    @Test
    void given_status_code_should_create_a_new_person_status() {
        // given: Input and mocks

        //when
        final PersonStatusDto personStatus = personService.createPersonStatus(personStatusDto);

        // then
        assertNotNull(personStatusDto);
        assertEquals(status.name(), personStatus.getCode());
        assertEquals(status.getLabel(), personStatus.getLabel());
        assertEquals(status.getDescription(), personStatus.getDescription());
    }

    @Test
    void when_creating_person_and_person_status_not_found_then_should_throw_ResourceNotFoundException() {
        // given: the dto for creating a person: personDtoWithWithNonExistentStatus

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.createPerson(personDtoWithWithNonExistentStatus));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_person_by_non_existent_id_then_should_throw_ResourceNotFoundException() {
        // given : nonexistent person id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.findPersonById(NON_EXISTENT_PERSON_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_adding_all_persons_from_file_with_valid_input_then_should_be_ok() {
        // given: the input and mocks
        final Class service = personService.getClass();
        try {
            final Field f = service.getDeclaredField("personFileName");
            f.setAccessible(true);
            f.set(personService, "static/persons.csv");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        // when
        personService.addAllPersonsFormFile();

        // then
        assertTrue(true);
    }

    @Test
    void when_adding_all_persons_from_file_and_person_status_entity_not_found_the_should_throw_IllegalArgumentException() {
        // given: the input and mocks
        final Class service = personService.getClass();
        try {
            final Field f = service.getDeclaredField("personFileName");
            f.setAccessible(true);
            f.set(personService, "static/person-test.csv");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        // when
        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class, () -> personService.addAllPersonsFormFile());

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("No enum constant com.zeta.books.enums.StatusEnum"));
    }

    @Test
    void whe_adding_status_it_should_be_ok() {
        // given: the input file

        // when
        personService.addAllStatus();

        // then
        assertTrue(true);
    }

    @TestConfiguration
    static class Configuration {
        @Bean
        @Primary
        PersonService personService() {
            final PersonRepository personRepository = mock(PersonRepository.class);
            final PersonStatusRepository personStatusRepository = mock(PersonStatusRepository.class);
            final StatusRepository statusRepository = mock(StatusRepository.class);

            when(personRepository.save(Mockito.any())).thenReturn(person);
            when(statusRepository.findByCode(status.name())).thenReturn(Optional.of(statusEntity));
            when(statusRepository.findByCode(personTobBeCreatedStatus)).thenReturn(Optional.of(writerStatusToBeAdded));
            when(personStatusRepository.save(personStatusEntity)).thenReturn(personStatusEntity);
            when(personRepository.findById(PERSON_ID)).thenReturn(Optional.of(person));
            when(personRepository.findById(NON_EXISTENT_PERSON_ID)).thenReturn(Optional.of(nonExistentPerson));
            when(personRepository.save(personEntityToBeCreated)).thenReturn(personEntityCreated);
            when(personStatusRepository.findByPersonEntity(person)).thenReturn(Optional.of(personStatusEntity));
            when(personStatusRepository.save(toBeCreatedPersonStatusEntity)).thenReturn(createdPersonStatusEntity);
            when(statusRepository.save(statusEntity)).thenReturn(statusEntity);

            return new PersonServiceImpl(personRepository, statusRepository, personStatusRepository);
        }
    }
}
