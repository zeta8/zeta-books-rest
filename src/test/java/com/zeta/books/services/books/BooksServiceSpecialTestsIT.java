package com.zeta.books.services.books;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.BookCreationDtoIn;
import com.zeta.books.dtos.BookCreationDtoOut;
import com.zeta.books.entities.BookEntity;
import com.zeta.books.entities.StockEntity;
import com.zeta.books.repositories.BookWriterRepository;
import com.zeta.books.repositories.BooksRepository;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.StockRepository;
import com.zeta.books.services.BookServiceImpl;
import com.zeta.books.services.interfaces.BookService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BooksApplication.class, BooksServiceSpecialTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BooksServiceSpecialTestsIT {

    @Autowired
    private BookService bookService;

    private static final String MAIN_TITLE_TO_BE_CREATED = "The Adapted mind";
    private static final String SUB_TITLE_TO_BE_CREATED = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN_TO_BE_CREATED = "0-19-506023-7";
    private static final BigDecimal PRICE_TO_BE_CREATED = new BigDecimal("25.49");
    private static final Long WRITER_ID_TO_BE_CREATED_1 = 1003L;
    private static final Long WRITER_ID_TO_BE_CREATED_2 = 1004L;


    private static BookCreationDtoIn bookToBeAdded;
    private static StockEntity stockEntity;
    private static BookEntity bookInStockEntity;

    private static List<StockEntity> stockEntities;

    private static final Integer NUMBER_EXISTING_COPIES = 1;
    private static final Long STOCK_ID = 1L;
    private static final Long EXISTING_BOOK_IN_STOCK_ID = 1L;

    @BeforeAll
    static void init() {
        bookToBeAdded = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE_TO_BE_CREATED)
                .addSubTitle(SUB_TITLE_TO_BE_CREATED)
                .addISBN(ISBN_TO_BE_CREATED)
                .addPrice(PRICE_TO_BE_CREATED)
                .addWriter(WRITER_ID_TO_BE_CREATED_1)
                .addWriter(WRITER_ID_TO_BE_CREATED_2)
                .build();

        bookInStockEntity = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_TO_BE_CREATED)
                .addSubTitle(SUB_TITLE_TO_BE_CREATED)
                .addISBN(ISBN_TO_BE_CREATED)
                .addPrice(PRICE_TO_BE_CREATED)
                .build();

        stockEntity = StockEntity.getBuilder()
                .addBookEntity(bookInStockEntity)
                .addNumberOfCopies(NUMBER_EXISTING_COPIES)
                .build();

        stockEntities = new ArrayList<>();
        stockEntities.add(stockEntity);

        final Class bookInStock = bookInStockEntity.getClass();
        try {
            final Field f = bookInStock.getDeclaredField("id");
            f.setAccessible(true);
            f.set(bookInStockEntity, EXISTING_BOOK_IN_STOCK_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class stockClass = stockEntity.getClass();
        try {
            final Field field = stockClass.getDeclaredField("id");
            field.setAccessible(true);
            field.set(stockEntity, STOCK_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void when_adding_an_existing_book_then_increases_the_number_of_copies() {
        // given

        // when
        bookService.addBook(bookToBeAdded);

        // then
        assertEquals(NUMBER_EXISTING_COPIES + 1, stockEntity.getNumberOfCopies());
    }

    @Test
    void when_adding_an_existing_book_then_returns_the_same_existing_book() {
        // given

        // when
        final BookCreationDtoOut bookCreationDtoOut = bookService.addBook(bookToBeAdded);

        // then
        assertNotNull(bookCreationDtoOut);
        assertEquals(bookInStockEntity.getId(), bookCreationDtoOut.getBookId());
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        BookService bookService() {
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);

            when(stockRepository.findAll()).thenReturn(stockEntities);
            when(stockRepository.save(stockEntity)).thenReturn(stockEntity);

            return new BookServiceImpl(booksRepository, personRepository, bookWriterRepository, stockRepository);
        }
    }
}
