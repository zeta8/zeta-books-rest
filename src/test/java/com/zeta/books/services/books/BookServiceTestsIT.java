package com.zeta.books.services.books;

import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.*;
import com.zeta.books.entities.BookEntity;
import com.zeta.books.entities.BookWriterEntity;
import com.zeta.books.entities.PersonEntity;
import com.zeta.books.entities.StockEntity;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.repositories.BookWriterRepository;
import com.zeta.books.repositories.BooksRepository;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.StockRepository;
import com.zeta.books.services.BookServiceImpl;
import com.zeta.books.services.interfaces.BookService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BooksApplication.class, BookServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookServiceTestsIT {

    @Autowired
    private BookService bookService;

    private static BookEntity bookEntity_1;
    private static BookEntity bookEntity_2;
    private static List<BookEntity> allBooks;

    private static PersonEntity person_1;
    private static PersonEntity person_2;
    private static PersonEntity person_5;


    private static List<BookWriterEntity> bookWriterEntities_1;
    private static List<BookWriterEntity> bookWriterEntities_2;

    private static final String MAIN_TITLE_1 = "The Adapted mind";
    private static final String SUB_TITLE_1 = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN_1 = "0-19-506023-7";
    private static final BigDecimal PRICE_1 = new BigDecimal("25.49");

    private static final String MAIN_TITLE_2 = "The Moral Animal";
    private static final String SUB_TITLE_2 = "Why We Are the Way We Are";
    private static final String ISBN_2 = "0-19-506023-7";
    private static final BigDecimal PRICE_2 = new BigDecimal("9.00");

    private static final String MAIN_TITLE_TO_BE_CREATED = "The Adapted mind";
    private static final String SUB_TITLE_TO_BE_CREATED = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN_TO_BE_CREATED = "0-19-506023-7";
    private static final BigDecimal PRICE_TO_BE_CREATED = new BigDecimal("25.49");
    private static final Long WRITER_ID_TO_BE_CREATED_1 = 1003L;
    private static final Long WRITER_ID_TO_BE_CREATED_2 = 1004L;

    private static final String MAIN_TITLE_CREATED = "The Adapted mind";
    private static final String SUB_TITLE_CREATED = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN_CREATED = "0-19-506023-7";
    private static final BigDecimal PRICE_CREATED = new BigDecimal("25.49");
    private static final Long CREATED_BOOK_ID = 1000L;

    private static final String MAIN_TITLE_TO_BE_CREATED_SECOND = "The Moral Animal";
    private static final String SUB_TITLE_TO_BE_CREATED_SECOND = "Why We Are the Way We Are";
    private static final String ISBN_TO_BE_CREATED_SECOND = "0-19-506023-7";
    private static final BigDecimal PRICE_TO_BE_CREATED_SECOND = new BigDecimal("9.00");
    private static final Long WRITER_ID_TO_BE_CREATED_1_SECOND = 1005L;
    private static final Long WRITER_ID_TO_BE_CREATED_2_SECOND = 1006L;

    private static final String MAIN_TITLE_CREATED_SECOND = "The Moral Animald";
    private static final String SUB_TITLE_CREATED_SECOND = "Why We Are the Way We Are";
    private static final String ISBN_CREATED_SECOND = "0-19-506023-7";
    private static final BigDecimal PRICE_CREATED_SECOND = new BigDecimal("9.00");
    private static final Long CREATED_BOOK_ID_SECOND = 1001L;

    private static final StatusEnum status = StatusEnum.WRI;
    private static final Long BOOK_ID_1 = 1001L;
    private static final Long BOOK_ID_2 = 1002L;

    private static final String PERSON_CODE_1 = "CD-1";
    private static final String FIRST_NAME_1 = "Jerome";
    private static final String MIDDLE_NAME_1 = "H.";
    private static final String LAST_NAME_1 = "Barkow";
    private static final LocalDate BIRTH_DATE_1 = LocalDate.of(1945, 1, 27);

    private static final String PERSON_CODE_2 = "CD-2";
    private static final String FIRST_NAME_2 = "Leda";
    private static final String LAST_NAME_2 = "Cosmides";
    private static final LocalDate BIRTH_DATE_2 = LocalDate.of(1957, 5, 9);

    private static final String PERSON_CODE_3 = "CD-3";
    private static final String FIRST_NAME_3 = "Robert";
    private static final String LAST_NAME_3 = "Wright";
    private static final LocalDate BIRTH_DATE_3 = LocalDate.of(1952, 1, 1);

    private static final String PERSON_CODE_4 = "CD-4";
    private static final String FIRST_NAME_4 = "Robert";
    private static final String LAST_NAME_4 = "Wright";
    private static final LocalDate BIRTH_DATE_4 = LocalDate.of(1957, 1, 15);

    private static BookCreationDtoIn bookToBeAdded;
    private static BookEntity createdBookEntity;
    private static BookEntity createdBookEntitySecond;
    private static BookEntity toBeCreatedBookEntity;
    private static BookEntity toBeCreatedBookEntitySecond;
    private static BookWriterEntity createdBookWriter;
    private static StockEntity stockEntity;
    private static StockEntity stockEntitySecond;
    private static final Integer NUMBER_COPIES = 1;
    private static final List<StockEntity> emptyStock = new ArrayList<>();
    private static List<BookCreationDtoIn> toBeCreatedBooks;

    private static PersonEntity WRITER_FOR_ADD_ALL_BOOKS_TEST;
    private static final Long WRITER_FOR_ADD_ALL_BOOKS_TEST_ID = 1L;
    private static final String WRITER_FOR_ADD_ALL_BOOKS_TEST_PERSON_CODE = "CD-1";

    @BeforeAll
    static void init() {
        bookEntity_1 = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_1)
                .addSubTitle(SUB_TITLE_1)
                .addISBN(ISBN_1)
                .addPrice(PRICE_1)
                .build();
        person_1 = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        person_2 = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_2)
                .addFirstName(FIRST_NAME_2)
                .addMiddleName(null)
                .addLastName(LAST_NAME_2)
                .addBirthDate(BIRTH_DATE_2)
                .build();

        final PersonEntity person_3;

        person_3 = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_3)
                .addFirstName(FIRST_NAME_3)
                .addMiddleName(null)
                .addLastName(LAST_NAME_3)
                .addBirthDate(BIRTH_DATE_3)
                .build();

        bookWriterEntities_1 = new ArrayList<>();
        final BookWriterEntity bookWriterEntity_1 = BookWriterEntity.getBuilder()
                .addBook(bookEntity_1)
                .addWriter(person_1)
                .build();

        final BookWriterEntity bookWriterEntity_2 = BookWriterEntity.getBuilder()
                .addBook(bookEntity_1)
                .addWriter(person_2)
                .build();

        final BookWriterEntity bookWriterEntity_3 = BookWriterEntity.getBuilder()
                .addBook(bookEntity_1)
                .addWriter(person_3)
                .build();
        bookWriterEntities_1.add(bookWriterEntity_1);
        bookWriterEntities_1.add(bookWriterEntity_2);
        bookWriterEntities_1.add(bookWriterEntity_3);

        bookEntity_2 = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_2)
                .addSubTitle(SUB_TITLE_2)
                .addISBN(ISBN_2)
                .addPrice(PRICE_2)
                .build();

        final PersonEntity person_4;

        person_4 = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_4)
                .addFirstName(FIRST_NAME_4)
                .addMiddleName(null)
                .addLastName(LAST_NAME_4)
                .addBirthDate(BIRTH_DATE_4)
                .build();

        person_5 = PersonEntity.getBuilder()
                .addPersonCode("CD-WRI")
                .addFirstName(FIRST_NAME_4)
                .addMiddleName(null)
                .addLastName(LAST_NAME_4)
                .addBirthDate(BIRTH_DATE_4)
                .build();

        final BookWriterEntity bookWriterEntity_4 = BookWriterEntity.getBuilder()
                .addBook(bookEntity_2)
                .addWriter(person_4)
                .build();

        bookWriterEntities_2 = new ArrayList<>();
        bookWriterEntities_2.add(bookWriterEntity_4);

        allBooks = new ArrayList<>();
        allBooks.add(bookEntity_1);
        allBooks.add(bookEntity_2);


        bookToBeAdded = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE_TO_BE_CREATED)
                .addSubTitle(SUB_TITLE_TO_BE_CREATED)
                .addISBN(ISBN_TO_BE_CREATED)
                .addPrice(PRICE_TO_BE_CREATED)
                .addWriter(WRITER_ID_TO_BE_CREATED_1)
                .addWriter(WRITER_ID_TO_BE_CREATED_2)
                .build();

        toBeCreatedBookEntity = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_CREATED)
                .addSubTitle(SUB_TITLE_CREATED)
                .addISBN(ISBN_CREATED)
                .addPrice(PRICE_CREATED)
                .build();

        createdBookEntity = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_CREATED)
                .addSubTitle(SUB_TITLE_CREATED)
                .addISBN(ISBN_CREATED)
                .addPrice(PRICE_CREATED)
                .build();

        createdBookWriter = BookWriterEntity.getBuilder()
                .addWriter(person_1)
                .addBook(createdBookEntity)
                .build();

        final BookCreationDtoIn bookToBeAddedSecond;

        bookToBeAddedSecond = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE_TO_BE_CREATED_SECOND)
                .addSubTitle(SUB_TITLE_TO_BE_CREATED_SECOND)
                .addISBN(ISBN_TO_BE_CREATED_SECOND)
                .addPrice(PRICE_TO_BE_CREATED_SECOND)
                .addWriter(WRITER_ID_TO_BE_CREATED_1_SECOND)
                .addWriter(WRITER_ID_TO_BE_CREATED_2_SECOND)
                .build();

        toBeCreatedBookEntitySecond = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_CREATED_SECOND)
                .addSubTitle(SUB_TITLE_CREATED_SECOND)
                .addISBN(ISBN_CREATED_SECOND)
                .addPrice(PRICE_CREATED_SECOND)
                .build();

        createdBookEntitySecond = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_CREATED_SECOND)
                .addSubTitle(SUB_TITLE_CREATED_SECOND)
                .addISBN(ISBN_CREATED_SECOND)
                .addPrice(PRICE_CREATED_SECOND)
                .build();

        stockEntity = StockEntity.getBuilder()
                .addBookEntity(createdBookEntity)
                .addNumberOfCopies(NUMBER_COPIES)
                .build();

        stockEntitySecond = StockEntity.getBuilder()
                .addBookEntity(createdBookEntitySecond)
                .addNumberOfCopies(NUMBER_COPIES)
                .build();

        toBeCreatedBooks = new ArrayList<>();
        toBeCreatedBooks.add(bookToBeAdded);
        toBeCreatedBooks.add(bookToBeAddedSecond);

        WRITER_FOR_ADD_ALL_BOOKS_TEST = PersonEntity.getBuilder()
                .addPersonCode("CD-1")
                .addFirstName("Issac")
                .addLastName("Newton")
                .addBirthDate(LocalDate.of(1643, 1, 4))
                .build();

        final Class clazz_1 = bookEntity_1.getClass();
        try {
            Field f = clazz_1.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(bookEntity_1, BOOK_ID_1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        final Class clazz_2 = bookEntity_2.getClass();
        try {
            Field f = clazz_2.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(bookEntity_2, BOOK_ID_2);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        final Class clazz_3 = createdBookEntity.getClass();
        try {
            Field f = clazz_3.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(createdBookEntity, CREATED_BOOK_ID);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        final Class clazz_4 = createdBookEntitySecond.getClass();
        try {
            Field f = clazz_4.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(createdBookEntitySecond, CREATED_BOOK_ID_SECOND);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        final Class clazz_5 = toBeCreatedBookEntity.getClass();
        try {
            Field f = clazz_5.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(createdBookEntitySecond, CREATED_BOOK_ID);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        final Class boorWriterForAddAllBook = WRITER_FOR_ADD_ALL_BOOKS_TEST.getClass();
        try {
            final Field f = boorWriterForAddAllBook.getDeclaredField("id");
            f.setAccessible(true);
            f.set(WRITER_FOR_ADD_ALL_BOOKS_TEST, WRITER_FOR_ADD_ALL_BOOKS_TEST_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class person_5Class = person_5.getClass();
        try {
            final Field f = person_5Class.getDeclaredField("id");
            f.setAccessible(true);
            f.set(person_5, 1L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Test
    void when_wants_to_list_all_books_the_should_list_them() {
        // given: the mock

        // when
        final BookDtoOut bookDtoOut = bookService.listAllBooks();

        //then
        assertNotNull(bookDtoOut);
        assertNotNull(bookDtoOut.getBooks());
        assertFalse(bookDtoOut.getBooks().isEmpty());
        bookDtoOut.getBooks().forEach(Assertions::assertNotNull);
        bookDtoOut.getBooks().forEach(entry -> {
            assertNotNull(entry.getMainTitle());
            assertNotNull(entry.getSubTitle());
            assertNotNull(entry.getIsbn());
            assertNotNull(entry.getPrice());
            assertNotNull(entry.getWriters());
            entry.getWriters().forEach(writer -> {
                assertNotNull(writer);
                assertNotNull(writer.getStatus());
                assertEquals(status, writer.getStatus());
                assertNotNull(writer.getFirstName());
                assertNotNull(writer.getLastName());
                assertNotNull(writer.getPersonCode());
                assertNotNull(writer.getBirthDate());
            });
        });
    }

    @Test
    void when_finding_by_existing_book_id_then_should_return_valid_book_dto() {
        // given
        // Here reflection is used to set the value of the id for the book entity


        // when
        final BookDto bookDto = bookService.findBookById(BOOK_ID_1);

        assertNotNull(bookDto);
        assertEquals(bookEntity_1.getMainTitle(), bookDto.getMainTitle());
        assertEquals(bookEntity_1.getSubTitle(), bookDto.getSubTitle());
        assertEquals(bookEntity_1.getIsbn(), bookDto.getIsbn());
        assertEquals(bookEntity_1.getPrice(), bookDto.getPrice());
        assertNotNull(bookDto.getWriters());
        assertEquals(3, bookDto.getWriters().size());
    }

    @Test
    void when_adding_a_list_of_books_and_non_exists_then_should_add_all() {
        // given

        // when
        final BookListCreationDtoOut bookCreationDtoOuts = bookService.addBookList(toBeCreatedBooks);

        // then
        assertNotNull(bookCreationDtoOuts);
        assertNotNull(bookCreationDtoOuts.getCreatedBooks());
        assertEquals(toBeCreatedBooks.size(), bookCreationDtoOuts.getCreatedBooks().size());
        bookCreationDtoOuts.getCreatedBooks().forEach(
                Assertions::assertNotNull
        );
    }

    @Test
    void when_adding_book_and_book_does_not_exist_then_should_add_it() {
        // given

        // when
        final BookCreationDtoOut bookCreationDtoOut = bookService.addBook(bookToBeAdded);

        // then
        assertNotNull(bookCreationDtoOut);
        assertNotNull(bookCreationDtoOut.getBookId());
        assertEquals(CREATED_BOOK_ID, bookCreationDtoOut.getBookId());
    }

    @Test
    void when_adding_all_books_from_file_then_the_method_should_be_called_exactly_once() {
        // given: the file

        // when
        final BookListCreationDtoOut booksList = bookService.addAllBooks();

        // then
        assertNotNull(booksList);
        assertNotNull(booksList.getCreatedBooks());
        assertFalse(booksList.getCreatedBooks().isEmpty());
    }

    @Test
    void when_adding_all_books_from_file_then_should_read_the_file_correctly() {
        // given: the file for books
        final List<BookCreationDtoIn> bookCreationDtoInList = new ArrayList<>();
        final BookCreationDtoIn bookCreationDtoIn = BookCreationDtoIn.getBuilder()
                .addMainTitle("Philosophiæ Naturalis Principia Mathematica")
                .addISBN("1603864350")
                .addPrice(new BigDecimal("39.95"))
                .addWriter(WRITER_FOR_ADD_ALL_BOOKS_TEST_ID)
                .build();
        final BookService bookServiceMocked = mock(BookService.class);
        bookCreationDtoInList.add(bookCreationDtoIn);

        final BookListCreationDtoOut bookListCreationDtoOut = BookListCreationDtoOut.getBuilder()
                .addCreatedBook(BookCreationDtoOut.getBuilder()
                        .addBookId(1L)
                        .build())
                .build();

        // when
        when(bookServiceMocked.addBookList(bookCreationDtoInList)).thenReturn(bookListCreationDtoOut);
        bookServiceMocked.addAllBooks();

        // then
        assertTrue(true);
    }

    @TestConfiguration
    static class Configuration {
        @Bean
        @Primary
        BookService bookService() {
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);

            when(booksRepository.findAll()).thenReturn(allBooks);
            when(booksRepository.findById(BOOK_ID_1)).thenReturn(Optional.of(bookEntity_1));
            when(bookWriterRepository.findByBookEntity(bookEntity_1)).thenReturn(Optional.of(bookWriterEntities_1));
            when(bookWriterRepository.findByBookEntity(bookEntity_2)).thenReturn(Optional.of(bookWriterEntities_2));
            when(stockRepository.findAll()).thenReturn(emptyStock);
            when(booksRepository.save(toBeCreatedBookEntity)).thenReturn(createdBookEntity);
            when(personRepository.findById(WRITER_ID_TO_BE_CREATED_1)).thenReturn(Optional.of(person_1));
            when(personRepository.findById(WRITER_ID_TO_BE_CREATED_2)).thenReturn(Optional.of(person_2));
            when(personRepository.findById(WRITER_ID_TO_BE_CREATED_1_SECOND)).thenReturn(Optional.of(person_1));
            when(personRepository.findById(WRITER_ID_TO_BE_CREATED_2_SECOND)).thenReturn(Optional.of(person_2));
            when(bookWriterRepository.save(createdBookWriter)).thenReturn(createdBookWriter);
            when(stockRepository.save(stockEntity)).thenReturn(stockEntity);
            when(stockRepository.save(stockEntitySecond)).thenReturn(stockEntitySecond);
            when(booksRepository.save(toBeCreatedBookEntitySecond)).thenReturn(createdBookEntitySecond);
            when(personRepository.findByPersonCode(WRITER_FOR_ADD_ALL_BOOKS_TEST_PERSON_CODE)).thenReturn(Optional.of(WRITER_FOR_ADD_ALL_BOOKS_TEST));
            when(personRepository.findById(1L)).thenReturn(Optional.of(person_5));

            return new BookServiceImpl(booksRepository, personRepository, bookWriterRepository, stockRepository);
        }
    }

}
