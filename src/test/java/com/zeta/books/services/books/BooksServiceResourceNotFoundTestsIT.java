package com.zeta.books.services.books;


import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.BookCreationDtoIn;
import com.zeta.books.entities.BookEntity;
import com.zeta.books.entities.BookWriterEntity;
import com.zeta.books.entities.PersonEntity;
import com.zeta.books.exceptions.ResourceNotFoundException;
import com.zeta.books.repositories.BookWriterRepository;
import com.zeta.books.repositories.BooksRepository;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.StockRepository;
import com.zeta.books.services.BookServiceImpl;
import com.zeta.books.services.interfaces.BookService;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {BooksApplication.class, BooksServiceResourceNotFoundTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BooksServiceResourceNotFoundTestsIT {

    @Autowired
    private BookService bookService;

    private static final String MAIN_TITLE_1 = "The Adapted mind";
    private static final String SUB_TITLE_1 = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN_1 = "0-19-506023-7";
    private static final BigDecimal PRICE_1 = new BigDecimal("25.49");

    private static final String MAIN_TITLE_2 = "The Moral Animal";
    private static final String SUB_TITLE_2 = "Why We Are the Way We Are";
    private static final String ISBN_2 = "0-19-506023-7";
    private static final BigDecimal PRICE_2 = new BigDecimal("9.00");

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Jerome";
    private static final String MIDDLE_NAME = "H.";
    private static final String LAST_NAME = "Barkow";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);

    private static BookEntity bookEntity_1;


    private static List<BookEntity> bookEntities;

    private static final Long NON_EXISTENT_BOOK_ID = -145789523L;
    private static final Long EXISTING_BOOK_ID = 1L;

    private static final Long EXISTING_PERSON_ID = 1L;
    private static final Long NON_EXISTING_PERSON_ID = 2L;

    private static PersonEntity existingPersonEntity;
    private static BookWriterEntity bookWriterEntity;


    @BeforeAll
    static void init() {
        bookEntity_1 = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_1)
                .addSubTitle(SUB_TITLE_1)
                .addISBN(ISBN_1)
                .addPrice(PRICE_1)
                .build();

        final BookEntity bookEntity_2;
        bookEntity_2 = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_2)
                .addSubTitle(SUB_TITLE_2)
                .addISBN(ISBN_2)
                .addPrice(PRICE_2)
                .build();

        bookEntities = new ArrayList<>();
        bookEntities.add(bookEntity_1);
        bookEntities.add(bookEntity_2);

        existingPersonEntity = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        bookWriterEntity = BookWriterEntity.getBuilder()
                .addBook(bookEntity_1)
                .addWriter(existingPersonEntity)
                .build();

        final Class clazz_1 = existingPersonEntity.getClass();
        try {
            Field f = clazz_1.getDeclaredField("id");
            f.setAccessible(true);
            try {
                f.set(existingPersonEntity, EXISTING_PERSON_ID);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }


    @Test
    void whe_listing_all_books_and_writers_not_found_then_should_throw_ResourceNotFoundException() {
        // given: the bookEntityList

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.listAllBooks());

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_book_by_id_and_id_not_found_then_should_throw_ResourceNotFoundException() {
        // given: a nonexistent id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.findBookById(NON_EXISTENT_BOOK_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_book_by_id_and_id_exists_but_bookWriter_not_found_then_should_throw_ResourceNotFoundException() {
        // given: a nonexistent id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.findBookById(EXISTING_BOOK_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_adding_a_new_book_with_nonExistent_writer_id_then_should_throw_ResourceNotFoundException() {
        // given
        BookCreationDtoIn bookCreationDtoIn = BookCreationDtoIn.getBuilder()
                .addMainTitle(MAIN_TITLE_1)
                .addSubTitle(SUB_TITLE_1)
                .addISBN(ISBN_1)
                .addPrice(PRICE_1)
                .addWriter(EXISTING_PERSON_ID)
                .addWriter(NON_EXISTING_PERSON_ID)
                .build();

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.addBook(bookCreationDtoIn));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_adding_all_books_with_nonExistent_person_code_then_should_throw_ResourceNotFoundException() {
        // given the file for books

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.addAllBooks());

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        BookService bookService() {
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);

            when(booksRepository.findAll()).thenReturn(bookEntities);
            when(booksRepository.findById(EXISTING_BOOK_ID)).thenReturn(Optional.of(bookEntity_1));
            when(booksRepository.save(bookEntity_1)).thenReturn(bookEntity_1);
            when(personRepository.findById(EXISTING_PERSON_ID)).thenReturn(Optional.of(existingPersonEntity));
            when(bookWriterRepository.save(bookWriterEntity)).thenReturn(bookWriterEntity);


            return new BookServiceImpl(booksRepository, personRepository, bookWriterRepository, stockRepository);
        }
    }
}
