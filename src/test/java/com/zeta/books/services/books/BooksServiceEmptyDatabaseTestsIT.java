package com.zeta.books.services.books;


import com.zeta.books.BooksApplication;
import com.zeta.books.dtos.*;
import com.zeta.books.entities.BookEntity;
import com.zeta.books.repositories.BookWriterRepository;
import com.zeta.books.repositories.BooksRepository;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.StockRepository;
import com.zeta.books.services.BookServiceImpl;
import com.zeta.books.services.interfaces.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(classes = {BooksApplication.class, BooksServiceEmptyDatabaseTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BooksServiceEmptyDatabaseTestsIT {

    @Autowired
    private BookService bookService;

    private static final List<BookEntity> emptyBookList = new ArrayList<>();


    @Test
    void when_no_books_are_in_database_then_should_return_an_empty_list() {
        // given: empty database

        // when
        final BookDtoOut bookDtoOut = bookService.listAllBooks();

        assertNotNull(bookDtoOut);
        assertNotNull(bookDtoOut.getBooks());
        assertTrue(bookDtoOut.getBooks().isEmpty());
    }

    @Test
    void when_adding_an_empty_book_list_then_should_return_and_empty_list() {
        // given: empty book list as input
        BookListCreationDtoOut.BookListCreationDtoOutBuilder builder = BookListCreationDtoOut.getBuilder();

        // when
        final BookListCreationDtoOut bookListCreationDtoOut = builder.build();

        // then
        assertNotNull(bookListCreationDtoOut);
        assertTrue(bookListCreationDtoOut.getCreatedBooks().isEmpty());
    }



    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        BookService bookService() {
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);

            when(booksRepository.findAll()).thenReturn(emptyBookList);

            return new BookServiceImpl(booksRepository, personRepository, bookWriterRepository, stockRepository);
        }
    }
}
