package com.zeta.books.dtos;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ResourceConflictException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.RandomStringGenerator;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PersonListDtoOutTest {

    private static final String PERSON_CODE_1 = "CD-1";
    private static final String FIRST_NAME_1 = "Wolfgang";
    private static final String MIDDLE_NAME_1 = "Amadeus";
    private static final String LAST_NAME_1 = "MOZART";
    private static final LocalDate BIRTH_DATE_1 = LocalDate.of(1756, 1, 27);
    private static final StatusEnum STATUS_1 = StatusEnum.CUS;

    private static final String PERSON_CODE_2 = RandomStringGenerator.generate(3);
    private static final String FIRST_NAME_2 = "Siavosh";
    private static final String LAST_NAME_2 = "DAKLAN";
    private static final LocalDate BIRTH_DATE_2 = LocalDate.of(1981, 8, 10);
    private static final StatusEnum STATUS_2 = StatusEnum.WRI;

    @Test
    void when_valid_person_as_input_then_should_return_valid_PersonListDtoOut() {
        // given
        final PersonDto person_1 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addStatus(STATUS_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        final PersonDto person_2 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_2)
                .addFirstName(FIRST_NAME_2)
                .addLastName(LAST_NAME_2)
                .addStatus(STATUS_2)
                .addBirthDate(BIRTH_DATE_2)
                .build();

        // when
        final PersonListDtoOut personListDtoOut = PersonListDtoOut.getBuilder()
                .addPerson(person_1)
                .addPerson(person_2)
                .build();

        // then
        assertNotNull(personListDtoOut);
        assertNotNull(personListDtoOut.getPersons());
        assertFalse(personListDtoOut.getPersons().isEmpty());
        assertEquals(2, personListDtoOut.getPersons().size());
        personListDtoOut.getPersons().forEach(person -> {
            assertNotNull(person.getPersonCode());
            assertNotNull(person.getFirstName());
            assertNotNull(person.getLastName());
            assertNotNull(person.getStatus());
            assertNotNull(person.getBirthDate());
        });
    }

    @Test
    void when_person_is_null_then_should_throw_MandatoryValueException() {
        // given a null person
        final PersonListDtoOut.PersonListDtoOutBuilder builder = PersonListDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPerson(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_no_person_is_fed_in_then_should_throw_MandatoryValueException() {
        // given
        final PersonListDtoOut.PersonListDtoOutBuilder builder = PersonListDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_two_persons_with_same_PersonCode_fed_then_should_throw_ResourceConflictException() {
        // given
        final PersonDto person_1 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addStatus(STATUS_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        final PersonDto person_2 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_2)
                .addLastName(LAST_NAME_2)
                .addStatus(STATUS_2)
                .addBirthDate(BIRTH_DATE_2)
                .build();

        final PersonListDtoOut.PersonListDtoOutBuilder builder = PersonListDtoOut.getBuilder();
        builder.addPerson(person_1);

        // when
        final ResourceConflictException exception = assertThrows(
                ResourceConflictException.class, () -> builder.addPerson(person_2));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.RESOURCE_CONFLICT_ERR));
    }

    @Test
    void when_person_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto person_1 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addStatus(STATUS_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        final PersonDto person_2 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_2)
                .addFirstName(FIRST_NAME_2)
                .addLastName(LAST_NAME_2)
                .addStatus(STATUS_2)
                .addBirthDate(BIRTH_DATE_2)
                .build();

        final PersonListDtoOut.PersonListDtoOutBuilder builder = PersonListDtoOut.getBuilder()
                .addPerson(person_1);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPerson(person_2));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto person_1 = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addStatus(STATUS_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        final PersonListDtoOut.PersonListDtoOutBuilder builder = PersonListDtoOut.getBuilder()
                .addPerson(person_1);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
