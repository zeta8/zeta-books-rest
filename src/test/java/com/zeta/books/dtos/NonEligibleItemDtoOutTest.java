package com.zeta.books.dtos;

import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalStateException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import static org.junit.jupiter.api.Assertions.*;

class NonEligibleItemDtoOutTest {


    private static final NonEligibilityReasonEnum REASON = NonEligibilityReasonEnum.NOT_FOUND;
    private static final Long NON_EXISTING_ITEM_ID = 1L;

    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: the input

        // when
        final NonEligibleItemDtoOut nonEligibleItem = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON)
                .addItem(NON_EXISTING_ITEM_ID)
                .build();

        // then
        assertNotNull(nonEligibleItem);
        assertEquals(REASON, nonEligibleItem.getReason());
        assertEquals(NON_EXISTING_ITEM_ID, nonEligibleItem.getItem());
    }

    @ParameterizedTest
    @NullSource
    void when_reason_fed_null_then_should_throw_MandatoryValueException(final NonEligibilityReasonEnum reason) {
        // given:
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addReason(reason));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_nonExistingItemId_fed_null_then_should_throw_MandatoryValueException(final Long nonExistingItemId) {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addItem(nonExistingItemId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_reason_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addItem(NON_EXISTING_ITEM_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_nonExistingItemId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_reason_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON)
                .addItem(NON_EXISTING_ITEM_ID);
        // when
        final ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, () -> builder.addReason(REASON));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_nonExistingItemId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON)
                .addItem(NON_EXISTING_ITEM_ID);
        // when
        final ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, () -> builder.addItem(NON_EXISTING_ITEM_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_reason_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON)
                .addItem(NON_EXISTING_ITEM_ID);
        builder.build();
        // when
        final ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, () -> builder.addReason(REASON));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_nonExistingItemId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON)
                .addItem(NON_EXISTING_ITEM_ID);
        builder.build();

        // when
        final ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, () -> builder.addItem(NON_EXISTING_ITEM_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final NonEligibleItemDtoOut.NonEligibleItemDtoOutBuilder builder = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON)
                .addItem(NON_EXISTING_ITEM_ID);
        builder.build();

        // when
        final ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class,builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
