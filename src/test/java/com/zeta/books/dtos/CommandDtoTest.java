package com.zeta.books.dtos;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandDtoTest {

    private static final BigDecimal TOTAL_PRICE = new BigDecimal("127.45");
    private static final Long CLIENT_ID = 1L;

    private static final Integer NUMBER_OF_COPIES = 5;

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");


    private static final String PERSON_COE = "CD-1";
    private static final String FIRST_NAME = "Jerome";
    private static final String MIDDLE_NAME = "H.";
    private static final String LAST_NAME = "Barkow";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);
    private static final StatusEnum STATUS = StatusEnum.WRI;

    private static final List<PersonDto> writers = new ArrayList<>();

    private static final List<CommandedBook> commandedBooks = new ArrayList<>();


    @BeforeAll
    static void init() {
        writers.add(PersonDto.getBuilder()
                .addPersonCode(PERSON_COE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build());

        final BookDto book;
        book = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        final CommandedBook commandedBook = CommandedBook.getBuilder()
                .addBook(book)
                .addNumberOfCopies(NUMBER_OF_COPIES)
                .build();

        commandedBooks.add(commandedBook);
    }


    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final CommandDto command = CommandDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice()
                .build();
        // then
        assertEquals(CLIENT_ID, command.getClientId());
        assertEquals(commandedBooks, command.getCommandedBooks());
        assertEquals(TOTAL_PRICE, command.getTotalPrice());
    }

    @ParameterizedTest
    @NullSource
    void when_clientId_fed_null_then_should_throw_MandatoryValueException(final Long clientId) {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addClientId(clientId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_commandedBooks_fed_null_then_should_throw_MandatoryValueException(final List<CommandedBook> commandedBooks) {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCommandedBooks(commandedBooks));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_totalPrice_fed_null_then_should_throw_MandatoryValueException(final BigDecimal totaaPrice) {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addTotalPrice(totaaPrice));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_clientId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_commandedBooks_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addClientId(CLIENT_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::calculateAndAddTotalPrice);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_totalPrice_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addCommandedBooks(commandedBooks)
                .addClientId(CLIENT_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_clientId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addClientId(CLIENT_ID);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addClientId(CLIENT_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_commandedBooks_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addCommandedBooks(commandedBooks);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandedBooks(commandedBooks));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_totalPrice_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::calculateAndAddTotalPrice);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_clientId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice();
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addClientId(CLIENT_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_commandedBooks_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice();
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandedBooks(commandedBooks));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_totalPrice_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice();
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::calculateAndAddTotalPrice);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandDto.CommandDtoBuilder builder = CommandDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(commandedBooks)
                .calculateAndAddTotalPrice();
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }

}
