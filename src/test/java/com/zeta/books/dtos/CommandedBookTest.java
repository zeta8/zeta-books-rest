package com.zeta.books.dtos;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandedBookTest {

    private static final Integer NUMBER_OF_COPIES= 1;

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");


    private static final String PERSON_COE = "CD-1";
    private static final String FIRST_NAME = "Jerome";
    private static final String MIDDLE_NAME = "H.";
    private static final String LAST_NAME = "Barkow";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);
    private static final StatusEnum STATUS = StatusEnum.WRI;

    private static final List<PersonDto> writers = new ArrayList<>();
    private static BookDto book;


    @BeforeAll
    static void init() {
        writers.add(PersonDto.getBuilder()
                .addPersonCode(PERSON_COE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build());

        book = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();
    }

    @Test
    void when_all_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final CommandedBook commandedBook = CommandedBook.getBuilder()
                .addBook(book)
                .addNumberOfCopies(NUMBER_OF_COPIES)
                .build();

        // then
        assertEquals(book, commandedBook.getBook());
        assertEquals(NUMBER_OF_COPIES, commandedBook.getNumberOfCopies());
    }

    @Test
    void when_book_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addBook(book);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_book_fed_null_then_should_throw_MandatoryValueException(final BookDto book) {
        // given;
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBook(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_number_of_copies_fed_null_then_should_throw_MandatoryValueException(final Integer numberOfCopies) {
        // given;
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addBook(book);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addNumberOfCopies(numberOfCopies));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addBook(book);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_numberOfCopies_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NUMBER_OF_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addBook(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_numberOfCopies_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addBook(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NUMBER_OF_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder()
                .addBook(book)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
