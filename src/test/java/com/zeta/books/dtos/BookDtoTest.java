package com.zeta.books.dtos;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookDtoTest {

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final String PERSON_COE = "CD-1";
    private static final String FIRST_NAME = "Jerome";
    private static final String MIDDLE_NAME = "H.";
    private static final String LAST_NAME = "Barkow";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1945, 1, 27);
    private static final StatusEnum STATUS = StatusEnum.WRI;

    private static final List<PersonDto> writers = new ArrayList<>();

    @BeforeAll
    static void init() {
        writers.add(PersonDto.getBuilder()
                .addPersonCode(PERSON_COE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build());
    }


    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final BookDto book = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        // then
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertEquals(SUB_TITLE, book.getSubTitle());
        assertEquals(PRICE, book.getPrice());
        assertEquals(ISBN, book.getIsbn());
        assertEquals(writers, book.getWriters());
    }

    @Test
    void when_sub_title_not_fed_then_should_be_ok() {
        // given: Input

        // when
        BookDto book = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        // then
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertNull(book.getSubTitle());
        assertEquals(PRICE, book.getPrice());
        assertEquals(ISBN, book.getIsbn());
        assertEquals(writers, book.getWriters());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_main_title_null_or_empty_then_should_throw_MandatoryValueException(final String mainTitle) {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addMainTitle(mainTitle));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_sub_title_null_or_empty_then_should_be_ok(final String subtitle) {
        // given: Input

        // when
        final BookDto book = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(subtitle)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        // then
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertNull(book.getSubTitle());
        assertEquals(PRICE, book.getPrice());
        assertEquals(ISBN, book.getIsbn());
        assertEquals(writers, book.getWriters());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_ISBN_null_or_empty_then_should_throw_MandatoryValueException(final String ISBN) {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addISBN(ISBN));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_price_null_then_should_throw_MandatoryValueException(final BigDecimal price) {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPrice(price));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_main_title_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addWriters(writers)
                .addPrice(PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_ISBN_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addWriters(writers)
                .addPrice(PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_price_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addWriters(writers)
                .addISBN(ISBN);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_writers_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_main_title_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addWriters(writers)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMainTitle("ANOTHER MAIN TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_sub_title_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addWriters(writers)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addSubTitle("ANOTHER SUB TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_ISBN_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addWriters(writers)
                .addPrice(PRICE);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addISBN("ANOTHER ISBN"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_price_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addWriters(writers)
                .addPrice(PRICE);
        builder.build();

        // when
        final BigDecimal price = new BigDecimal("15.22");
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPrice(price));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_writers_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addWriters(writers)
                .addPrice(PRICE);
        builder.build();

        // when
        final BigDecimal price = new BigDecimal("15.22");
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addWriters(writers));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_main_title_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMainTitle("ANOTHER MAIN TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_sub_title_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addSubTitle("ANOTHER SUB TITLE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_ISBN_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addISBN("ANOTHER ISBN"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_price_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);

        // when
        final BigDecimal price = new BigDecimal("15.22");
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPrice(price));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookDto.BookDtoBuilder builder = BookDto.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addISBN(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
