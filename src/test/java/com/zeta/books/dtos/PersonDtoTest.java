package com.zeta.books.dtos;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PersonDtoTest {

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);
    private static final StatusEnum STATUS = StatusEnum.CUS;


    @Test
    void when_all_fields_fed_correctly_then_should_be_ok() {
        // given: The input

        // when
        final PersonDto person = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // Then
        assertEquals(PERSON_CODE, person.getPersonCode());
        assertEquals(FIRST_NAME, person.getFirstName());
        assertEquals(MIDDLE_NAME, person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate());
    }

    @Test
    void when_middle_name_not_fed_then_should_be_ok() {
        // given: The input

        // when
        final PersonDto person = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // Then
        assertEquals(FIRST_NAME, person.getFirstName());
        assertNull(person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_person_code_null_or_empty_then_should_throw_MandatoryValueException(final String personCode) {
        // given
        final PersonDto.PersonDtoBuilder builder = PersonDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPersonCode(personCode));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_first_name_null_or_empty_then_should_throw_MandatoryValueException(final String firstName) {
        // given
        final PersonDto.PersonDtoBuilder builder = PersonDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addFirstName(firstName));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_middle_name_null_or_empty_then_should_be_ok(final String middleName) {
        // given: Input

        // when
        final PersonDto person = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(middleName)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // Then
        assertEquals(FIRST_NAME, person.getFirstName());
        assertNull(person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "  "})
    void when_last_name_null_or_empty_then_should_throw_MandatoryValueException(final String lastName) {
        // given
        final PersonDto.PersonDtoBuilder builder = PersonDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addLastName(lastName));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_birthDate_null_then_should_throw_MandatoryValueException(final LocalDate birthDate) {
        // given
        final PersonDto.PersonDtoBuilder builder = PersonDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBirthDate(birthDate));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_status_null_then_should_throw_MandatoryValueException(final StatusEnum statusEnum) {
        // given
        final PersonDto.PersonDtoBuilder builder = PersonDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addStatus(statusEnum));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_code_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, PersonDtoBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_first_name_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, PersonDtoBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_last_name_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, PersonDtoBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_birth_date_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, PersonDtoBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_status_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, PersonDtoBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_person_code_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addPersonCode("ANOTHER CODE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_first_name_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addFirstName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_middle_name_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addMiddleName(MIDDLE_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_last_name_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addLastName(LAST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_birth_date_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addBirthDate(BIRTH_DATE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_status_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addStatus(StatusEnum.WRI));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_person_code_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addPersonCode("CODE"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_first_name_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addFirstName(FIRST_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addFirstName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_middle_name_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addMiddleName(MIDDLE_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addMiddleName("FIRST_NAME"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_last_name_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addLastName(LAST_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addLastName("SOME Other last name"));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_birth_date_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addBirthDate(BIRTH_DATE);

        // when
        final LocalDate anotherBirthdate = LocalDate.of(2020, 1, 1);
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addBirthDate(anotherBirthdate));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_status_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addStatus(StatusEnum.WRI);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> PersonDtoBuilder.addStatus(STATUS));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonDto.PersonDtoBuilder PersonDtoBuilder = PersonDto.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        PersonDtoBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, PersonDtoBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));

    }
}
