package com.zeta.books.dtos;

import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ResourceConflictException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StatusListDtoOutTest {

    private static final StatusEnum STATUS_1 = StatusEnum.CUS;
    private static final StatusEnum STATUS_2 = StatusEnum.WRI;

    @Test
    void when_valid_StatusDtoInList_then_shouldReturn_Valid_StatusListDtoOut() {
        // given
        final PersonStatusDto status_1 = PersonStatusDto.getBuilder()
                .addCode(STATUS_1.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_1.getDescription())
                .build();

        final PersonStatusDto status_2 = PersonStatusDto.getBuilder()
                .addCode(STATUS_2.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_2.getDescription())
                .build();

        // when
        final StatusListDtoOut statusListDtoOut = StatusListDtoOut.getBuilder()
                .addStatus(status_1)
                .addStatus(status_2)
                .build();

        // then
        assertNotNull(statusListDtoOut);
        assertNotNull(statusListDtoOut.getStatusDtoList());
        assertFalse(statusListDtoOut.getStatusDtoList().isEmpty());
        assertEquals(2, statusListDtoOut.getStatusDtoList().size());
    }

    @Test
    void when_no_status_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusListDtoOut.StatusListDtoOutBuilder builder = StatusListDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_status_fed_null_then_should_throw_MandatoryValueException() {
        // given
        final PersonStatusDto status = PersonStatusDto.getBuilder()
                .addCode(STATUS_1.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_1.getDescription())
                .build();
        final StatusListDtoOut.StatusListDtoOutBuilder builder = StatusListDtoOut.getBuilder()
                .addStatus(status);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addStatus(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_two_status_with_same_code_fed_then_should_throw_ResourceConflictException() {
        // given
        final PersonStatusDto status_1 = PersonStatusDto.getBuilder()
                .addCode(STATUS_1.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_1.getDescription())
                .build();

        final PersonStatusDto status_2 = PersonStatusDto.getBuilder()
                .addCode(STATUS_1.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_2.getDescription())
                .build();

        final StatusListDtoOut.StatusListDtoOutBuilder builder = StatusListDtoOut.getBuilder()
                .addStatus(status_1);

        // when
        final ResourceConflictException exception = assertThrows(
                ResourceConflictException.class, () -> builder.addStatus(status_2));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.RESOURCE_CONFLICT_ERR));
    }

    @Test
    void when_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonStatusDto status_1 = PersonStatusDto.getBuilder()
                .addCode(STATUS_1.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_1.getDescription())
                .build();

        final PersonStatusDto status_2 = PersonStatusDto.getBuilder()
                .addCode(STATUS_2.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_2.getDescription())
                .build();

        final StatusListDtoOut.StatusListDtoOutBuilder builder = StatusListDtoOut.getBuilder()
                .addStatus(status_1);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addStatus(status_2));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonStatusDto status = PersonStatusDto.getBuilder()
                .addCode(STATUS_1.name())
                .addLabel(STATUS_1.getLabel())
                .addDescription(STATUS_1.getDescription())
                .build();
        final StatusListDtoOut.StatusListDtoOutBuilder builder = StatusListDtoOut.getBuilder()
                .addStatus(status);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));

    }
}
