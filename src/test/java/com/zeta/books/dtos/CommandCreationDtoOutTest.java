package com.zeta.books.dtos;

import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.exceptions.ZetaIllegalStateException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandCreationDtoOutTest {

    private static final Long COMMAND_ID = 1L;
    private static final String MESSAGE = "Some message";
    private static final NonEligibilityReasonEnum REASON_NOT_FOUND = NonEligibilityReasonEnum.NOT_FOUND;
    private static final Long NOT_FOUND_ITEM = 1L;
    private static final NonEligibilityReasonEnum REASON_NOT_ENOUGH = NonEligibilityReasonEnum.NOT_ENOUGH_COPIES;
    private static final Long NOT_ENOUGH_ITEM = 2L;
    private static List<NonEligibleItemDtoOut> nonEligibleItems;


    @BeforeAll
    static void init() {
        final NonEligibleItemDtoOut nonEligibleItem_1 = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON_NOT_FOUND)
                .addItem(NOT_FOUND_ITEM)
                .build();
        
        final NonEligibleItemDtoOut nonEligibleItem_2 = NonEligibleItemDtoOut.getBuilder()
                .addReason(REASON_NOT_ENOUGH)
                .addItem(NOT_ENOUGH_ITEM)
                .build();

        nonEligibleItems = new ArrayList<>() {
            {
                add(nonEligibleItem_1);
                add(nonEligibleItem_2);
            }
        };
    }

    @Test
    void when_all_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final CommandCreationDtoOut createdCommand = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID)
                .addMessage(MESSAGE)
                .addNonEligibleItems(nonEligibleItems)
                .build();

        // then
        assertEquals(COMMAND_ID, createdCommand.getCommandId());
    }

    @Test
    void when_neither_message_nor_nonEligibleItems_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final CommandCreationDtoOut createdCommand = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID)
                .build();

        // then
        assertEquals(COMMAND_ID, createdCommand.getCommandId());
    }

    @Test
    void when_commandId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_message_and_nonEligibleItems_both_fed_empty_and_null_should_be_ok() {
        // when
        final CommandCreationDtoOut createdCommand = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID)
                .addMessage(null)
                .addNonEligibleItems(null)
                .build();

        // then
        assertEquals(COMMAND_ID, createdCommand.getCommandId());
    }

    @Test
    void when_message_empty_and_nonEligibleItems_null_should_be_ok() {
        // when
        final CommandCreationDtoOut createdCommand = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID)
                .addMessage(" ")
                .addNonEligibleItems(null)
                .build();

        // then
        assertEquals(COMMAND_ID, createdCommand.getCommandId());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_message_null_or_empty_while_nonEligibleItems_not_null_then_should_throw_ZetaIllegalStateException(final String message) {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID)
                .addMessage(message)
                .addNonEligibleItems(nonEligibleItems);

        // when
        ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.COUPLED_FIELD_ERR));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_message_fed_non_empty_while_nonEligibleItems_null_or_empty_then_should_throw_ZetaIllegalStateException(final List<NonEligibleItemDtoOut> nonEligibleItems) {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID)
                .addMessage(MESSAGE)
                .addNonEligibleItems(nonEligibleItems);

        // when
        ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.COUPLED_FIELD_ERR));
    }

    @ParameterizedTest
    @NullSource
    void when_commandId_fed_null_then_should_throw_MandatoryValueException(final Long commandId) {
        // given;
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCommandId(commandId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_commandId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_message_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addMessage(MESSAGE);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMessage(MESSAGE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_nonEligibleItems_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addNonEligibleItems(nonEligibleItems);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNonEligibleItems(nonEligibleItems));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }



    @Test
    void when_commandId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_message_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMessage(MESSAGE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_nonEligibleItems_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNonEligibleItems(nonEligibleItems));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDtoOut.CommandCreationDtoOutBuilder builder = CommandCreationDtoOut.getBuilder()
                .addCommandId(COMMAND_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
