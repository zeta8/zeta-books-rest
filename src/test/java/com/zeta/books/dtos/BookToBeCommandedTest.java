package com.zeta.books.dtos;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import static org.junit.jupiter.api.Assertions.*;

class BookToBeCommandedTest {

    private static final Long BOOK_ID = 1L;
    private static final Integer NUMBER_OF_COPIES = 1;

    @Test
    void when_all_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final BookToBeCommanded toBeCommanded = BookToBeCommanded.getBuilder()
                .addBook(BOOK_ID)
                .addNumberOfCopies(NUMBER_OF_COPIES)
                .build();

        // then
        assertEquals(BOOK_ID, toBeCommanded.getBookId());
        assertEquals(NUMBER_OF_COPIES, toBeCommanded.getNumberOfCopies());
    }

    @Test
    void when_BookId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder =
                BookToBeCommanded.getBuilder()
                        .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder =
                BookToBeCommanded.getBuilder()
                        .addBook(BOOK_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_bookId_fed_null_then_should_throw_MandatoryValueException(final Long bookId) {
        // given;
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addNumberOfCopies(NUMBER_OF_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBook(bookId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_number_of_copies_fed_null_then_should_throw_MandatoryValueException(final Integer numberOfCopies) {
        // given;
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addBook(BOOK_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addNumberOfCopies(numberOfCopies));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_bookId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addBook(BOOK_ID);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_numberOfCopies_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addNumberOfCopies(NUMBER_OF_COPIES);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NUMBER_OF_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_bookId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addBook(BOOK_ID)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_numberOfCopies_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addBook(BOOK_ID)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NUMBER_OF_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookToBeCommanded.BookToBeCommandedBuilder builder = BookToBeCommanded.getBuilder()
                .addBook(BOOK_ID)
                .addNumberOfCopies(NUMBER_OF_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
