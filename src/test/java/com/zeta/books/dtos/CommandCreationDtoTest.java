package com.zeta.books.dtos;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandCreationDtoTest {

    private static final Long CLIENT_ID = 1L;


    private static final List<BookToBeCommanded> TO_BE_COMMANDED_BOOKS = new ArrayList<>();

    @BeforeAll
    static void init() {
        final Integer NUMBER_OF_COPIES = 1;
        final Long BOOK_ID = 1L;
        TO_BE_COMMANDED_BOOKS.add(BookToBeCommanded.getBuilder()
                .addNumberOfCopies(NUMBER_OF_COPIES)
                .addBook(BOOK_ID)
                .build());
    }

    @Test
    void when_all_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final CommandCreationDto commandedBooks = CommandCreationDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(TO_BE_COMMANDED_BOOKS)
                .build();

        // then
        assertEquals(CLIENT_ID, commandedBooks.getClientId());
        assertEquals(TO_BE_COMMANDED_BOOKS, commandedBooks.getToBeCommandedBooks());
    }

    @Test
    void when_clientId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder =
                CommandCreationDto.getBuilder()
                        .addCommandedBooks(TO_BE_COMMANDED_BOOKS);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_toBeCommandedBooks_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder =
                CommandCreationDto.getBuilder()
                        .addClientId(CLIENT_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_clientId_fed_null_then_should_throw_MandatoryValueException(final Long bookId) {
        // given;
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addCommandedBooks(TO_BE_COMMANDED_BOOKS);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addClientId(bookId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_toBeCommanded_fed_null_then_should_throw_MandatoryValueException(final List<BookToBeCommanded> toBeCommandedBooks) {
        // given;
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addClientId(CLIENT_ID);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCommandedBooks(toBeCommandedBooks));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_clientId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addClientId(CLIENT_ID);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addClientId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_toBeCommandedBooks_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addCommandedBooks(TO_BE_COMMANDED_BOOKS);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandedBooks(TO_BE_COMMANDED_BOOKS));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_clientId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(TO_BE_COMMANDED_BOOKS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addClientId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_toBeCommandedBooks_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(TO_BE_COMMANDED_BOOKS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommandedBooks(TO_BE_COMMANDED_BOOKS));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final CommandCreationDto.CommandCreationDtoBuilder builder = CommandCreationDto.getBuilder()
                .addClientId(CLIENT_ID)
                .addCommandedBooks(TO_BE_COMMANDED_BOOKS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
