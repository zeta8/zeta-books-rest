package com.zeta.books.dtos;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import static org.junit.jupiter.api.Assertions.*;

class PersonCreationDtoOutTest {

    private static final Long PERSON_ID = 1L;

    @Test
    void when_all_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final PersonCreationDtoOut createdPerson = PersonCreationDtoOut.getBuilder()
                .addPersonId(PERSON_ID)
                .build();

        // then
        assertEquals(PERSON_ID, createdPerson.getPersonId());
    }

    @Test
    void when_personId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonCreationDtoOut.PersonCreationDtoOutBuilder builder = PersonCreationDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_personId_fed_null_then_should_throw_MandatoryValueException(final Long personId) {
        // given;
        final PersonCreationDtoOut.PersonCreationDtoOutBuilder builder = PersonCreationDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPersonId(personId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_personId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonCreationDtoOut.PersonCreationDtoOutBuilder builder = PersonCreationDtoOut.getBuilder()
                .addPersonId(PERSON_ID);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_personId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonCreationDtoOut.PersonCreationDtoOutBuilder builder = PersonCreationDtoOut.getBuilder()
                .addPersonId(PERSON_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonCreationDtoOut.PersonCreationDtoOutBuilder builder = PersonCreationDtoOut.getBuilder()
                .addPersonId(PERSON_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
