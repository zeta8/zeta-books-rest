package com.zeta.books.dtos;

import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import static org.junit.jupiter.api.Assertions.*;

class BookCreationDtoOutTest {
    private static final Long BOOK_ID = 1L;

    @Test
    void when_all_fed_properly_then_should_be_ok() {
        // given: Input

        // when
        final BookCreationDtoOut createdPerson = BookCreationDtoOut.getBuilder()
                .addBookId(BOOK_ID)
                .build();

        // then
        assertEquals(BOOK_ID, createdPerson.getBookId());
    }

    @Test
    void when_BookId_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final BookCreationDtoOut.BookCreationDtoOutBuilder builder = BookCreationDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_bookId_fed_null_then_should_throw_MandatoryValueException(final Long bookId) {
        // given;
        final BookCreationDtoOut.BookCreationDtoOutBuilder builder = BookCreationDtoOut.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBookId(bookId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));

    }

    @Test
    void when_bookId_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookCreationDtoOut.BookCreationDtoOutBuilder builder = BookCreationDtoOut.getBuilder()
                .addBookId(BOOK_ID);


        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBookId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_bookId_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookCreationDtoOut.BookCreationDtoOutBuilder builder = BookCreationDtoOut.getBuilder()
                .addBookId(BOOK_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBookId(2L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final BookCreationDtoOut.BookCreationDtoOutBuilder builder = BookCreationDtoOut.getBuilder()
                .addBookId(BOOK_ID);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
