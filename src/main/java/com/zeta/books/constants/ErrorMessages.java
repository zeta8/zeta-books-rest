package com.zeta.books.constants;

public final class ErrorMessages {

    public static final String MANDATORY_VALUE_ERROR = "The field (%s) is mandatory";
    public static final String ILLEGAL_FEEDING_ERR = "Feeding in the fields is not permitted after the object is built";
    public static final String DUPLICATED_FILED_FEED_ERROR = "The field (%s) has been already fed in";
    public static final String EMPTY_FILED_FEED_ERROR = "The field (%s) should be either feed in with a non empty value or not feed at all";
    public static final String ILLEGAL_BUILD_ERR = "The object (%s) is already built, it cannot be rebuilt";
    public static final String NOT_FOUND_ERR = "The object with the id or code (%s) not found";
    public static final String ONE_FILED_MANDATORY = "Exactly one of the fields (%s) or (%s) should be fed in";
    public static final String BOOK_NOT_FOUND = "The book with the id (%s) not found";
    public static final String COMMAND_NOT_VALID = "There are no valid items in the command (%s)";
    public static final String COUPLED_FIELD_ERR = "The two fields (%s) and (%s) should be either both fed, or no of them fed";
    public static final String NOT_ENOUGH_COPIES = "You have demanded (%s) copies, while there are just (%s) copies accessible for the moment";
    public static final String NON_ACCUMULATIVE_FIELD_ERR = "Exactly one of the fields (%s) or (%s) should be fed";
    public static final String RESOURCE_CONFLICT_ERR = "The (%s) with the id or code (%s) exists already";
}
