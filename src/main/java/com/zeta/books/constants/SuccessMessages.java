package com.zeta.books.constants;

public final class SuccessMessages {

    public static final String PERSON_DELETE_MSG = "The person with the code (%s) is successfully deleted";
    public static final String NON_ADDED_BOOKS_LIST_MSG = "The following list could not be added to your command";
}
