package com.zeta.books.repositories;

import com.zeta.books.entities.NonEligibilityReasonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NonEligibilityReasonRepository extends JpaRepository<NonEligibilityReasonEntity, Long> {
    Optional<NonEligibilityReasonEntity> findByCode(String code);
}