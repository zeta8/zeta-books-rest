package com.zeta.books.repositories;

import com.zeta.books.entities.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BooksRepository extends JpaRepository<BookEntity, Long> {
}
