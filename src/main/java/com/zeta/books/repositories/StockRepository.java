package com.zeta.books.repositories;

import com.zeta.books.entities.BookEntity;
import com.zeta.books.entities.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StockRepository extends JpaRepository<StockEntity, Long> {
    Optional<StockEntity> findByBookEntity(BookEntity bookEntity);
}
