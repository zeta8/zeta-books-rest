package com.zeta.books.repositories;

import com.zeta.books.entities.CommandEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandsRepository extends JpaRepository<CommandEntity, Long> {
}
