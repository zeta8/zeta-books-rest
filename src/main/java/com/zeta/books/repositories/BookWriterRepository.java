package com.zeta.books.repositories;

import com.zeta.books.entities.BookEntity;
import com.zeta.books.entities.BookWriterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookWriterRepository extends JpaRepository<BookWriterEntity, Long> {
    Optional<List<BookWriterEntity>> findByBookEntity(BookEntity bookEntity);
}
