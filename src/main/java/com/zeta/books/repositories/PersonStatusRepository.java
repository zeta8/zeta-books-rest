package com.zeta.books.repositories;

import com.zeta.books.entities.PersonEntity;
import com.zeta.books.entities.PersonStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonStatusRepository extends JpaRepository<PersonStatusEntity, Long> {

    Optional<PersonStatusEntity> findByPersonEntity(PersonEntity personEntity);
}
