package com.zeta.books.repositories;

import com.zeta.books.entities.NonEligibleItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NonEligibleItemRepository extends JpaRepository<NonEligibleItemEntity, Long> {
}