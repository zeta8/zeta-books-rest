package com.zeta.books.repositories;

import com.zeta.books.entities.CommandBookEntity;
import com.zeta.books.entities.CommandEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommandBookRepository extends JpaRepository<CommandBookEntity, Long> {
    Optional<List<CommandBookEntity>> findByCommandEntity(CommandEntity commandEntity);
}
