package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.entities.embeddables.CommandBookId;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "command_book")
public final class CommandBookEntity implements Serializable {

    private static final long serialVersionUID = -2737733557698976893L;

    @EmbeddedId
    private final CommandBookId commandBookId = CommandBookId.getNewInstance();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "command_id", foreignKey = @ForeignKey(name = "FK_command"))
    @MapsId("commandId")
    private CommandEntity commandEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_book"))
    @MapsId("bookId")
    private BookEntity bookEntity;

    @Column(name = "number_of_copies")
    private Integer numberOfCopies;

    // Used by hibernate
    // Not to be directly instantiated by the client
    CommandBookEntity() {
    }

    // Used by the builder
    private CommandBookEntity(CommandBookEntityBuilder builder) {
        commandEntity = builder.commandEntity;
        bookEntity = builder.bookEntity;
        numberOfCopies = builder.numberOfCopies;
        builder.isInstantiated = true;
    }

    // called by the client to obtain a new instance of teh builder of this class
    public static CommandBookEntityBuilder getBuilder() {
        return CommandBookEntityBuilder.getNewInstance();
    }

    public CommandBookId getCommandBookId() {
        return commandBookId;
    }

    public CommandEntity getCommandEntity() {
        return commandEntity;
    }

    public BookEntity getBookEntity() {
        return bookEntity;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandBookEntity)) return false;

        CommandBookEntity that = (CommandBookEntity) o;

        return new EqualsBuilder()
                .append(getCommandBookId(), that.getCommandBookId())
                .append(getCommandEntity(), that.getCommandEntity())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCommandBookId())
                .append(getCommandEntity())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("commandBookId", commandBookId)
                .append("commandEntity", commandEntity)
                .append("bookEntity", bookEntity)
                .append("numberOfCopies", numberOfCopies)
                .toString();
    }

    /**
     * The builder of the class {@link CommandEntity}
     */
    public static final class CommandBookEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandBookEntityBuilder.class);

        private CommandEntity commandEntity;
        private BookEntity bookEntity;
        private Integer numberOfCopies;
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private CommandBookEntityBuilder() {
        }

        // Called by the man class to obtain a new instance of its won builder
        private static CommandBookEntityBuilder getNewInstance() {
            return new CommandBookEntityBuilder();
        }

        /**
         * @param commandEntity: The command of the client
         * @return {@link CommandBookEntityBuilder}
         */
        public CommandBookEntityBuilder addCommandEntity(final CommandEntity commandEntity) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.commandEntity) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "commandEntity"));
            }

            if (null == commandEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandEntity"));
            }

            LOGGER.info("Adding the command: {}", commandEntity);
            this.commandEntity = commandEntity;
            return this;
        }

        /**
         * @param bookEntity: The book to be added to the command of the client
         * @return {@link CommandBookEntityBuilder}
         */
        public CommandBookEntityBuilder addBookEntity(final BookEntity bookEntity) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.bookEntity) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "bookEntity"));
            }

            if (null == bookEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookEntity"));
            }

            LOGGER.info("Adding the book: {} to the command", bookEntity);
            this.bookEntity = bookEntity;
            return this;
        }

        /**
         * @param numberOfCopies: The number of copies bought by the client
         * @return {@link CommandBookEntityBuilder}
         */
        public CommandBookEntityBuilder addNumberOfCopies(final Integer numberOfCopies) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.numberOfCopies) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "numberOfCopies"));
            }

            if (null == numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }

            LOGGER.info("Adding the number of copies: : {} bought by the client", numberOfCopies);
            this.numberOfCopies = numberOfCopies;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         *
         * @return {@link CommandBookEntity}
         */
        public CommandBookEntity build() {
            validate();
            return new CommandBookEntity(this);
        }

        // Validates the integrity of the object before actually building it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.commandEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandEntity"));
            }

            if (null == this.bookEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookEntity"));
            }

            if (null == this.numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }
        }
    }
}
