package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

@Entity
@Table(name = "stock")
public final class StockEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "number_of_copies")
    private Integer numberOfCopies;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_book_stock"), nullable=false)
    private BookEntity bookEntity;

    // To be used by hibernate
    // Not to be instantiated directly by the client
    StockEntity() {
    }

    // Used by the builder
    private StockEntity(StockEntityBuilder builder) {
        numberOfCopies = builder.numberOfCopies;
        bookEntity = builder.bookEntity;
        builder.isInstantiated = true;
    }

    /**
     * called by the client to obtain a new instance of teh builder of this class
     * @return {@link StockEntityBuilder}
     */
    public static StockEntityBuilder getBuilder() {
        return StockEntityBuilder.getNewInstance();
    }

    /**
     * for ading one to the number of the present books in the stock
     */
    public void addOneToStock() {
        this.numberOfCopies = numberOfCopies + 1;
    }

    /**
     * For adding a given number of copies to the existing one
     * @param noNewCopies: The number of copies to be added
     */
    public void addToStock(final Integer noNewCopies) {
        this.numberOfCopies = this.numberOfCopies + noNewCopies;
    }

    /**
     * For deleting the sold books from the stock
     * @param noOfCommandedCopies The number of the sold copies
     */
    public StockEntity subtractFromStock(Integer noOfCommandedCopies) {
        this.numberOfCopies = this.numberOfCopies - noOfCommandedCopies;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    public BookEntity getBookEntity() {
        return bookEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof StockEntity)) return false;

        StockEntity that = (StockEntity) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("numberOfCopies", numberOfCopies)
                .append("bookEntity", bookEntity)
                .toString();
    }

    /**
     * The builder of {@link StockEntity}
     */
    public static class StockEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(StockEntityBuilder.class);

        private Integer numberOfCopies;
        private BookEntity bookEntity;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private StockEntityBuilder() {
        }

        // Called by the main class to obtain a new instance of its own builder
        private static StockEntityBuilder getNewInstance() {
            return new StockEntityBuilder();
        }

        /**
         * @param numberOfCopies: Number of copies available in the stock
         * @return {@link StockEntityBuilder}
         */
        public StockEntityBuilder addNumberOfCopies(final Integer numberOfCopies) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.numberOfCopies) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "numberOfCopies"));
            }

            if (null == numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }

            LOGGER.info("Adding number of copies: {}", numberOfCopies);
            this.numberOfCopies = numberOfCopies;
            return this;
        }

        /**
         * @param bookEntity: the book in the stock
         * @return {@link StockEntityBuilder}
         */
        public StockEntityBuilder addBookEntity(final BookEntity bookEntity) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.bookEntity) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "bookEntity"));
            }

            if (null == bookEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookEntity"));
            }

            LOGGER.info("Adding the book: {}", bookEntity);
            this.bookEntity = bookEntity;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the cleint
         * @return {@link StockEntity
         * }
         */
        public StockEntity build() {
            validate();
            return new StockEntity(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.bookEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookEntity"));
            }
            if (null == this.numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }
        }
    }
}
