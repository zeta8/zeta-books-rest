package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.entities.embeddables.BookWriterId;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "book_writer")
public final class BookWriterEntity implements Serializable {

    private static final long serialVersionUID = -769614091910850871L;

    @EmbeddedId
    private final BookWriterId bookWriterId = BookWriterId.getNewInstance();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_book_book"))
    @MapsId("bookId")
    private BookEntity bookEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "writer_id", foreignKey = @ForeignKey(name = "FK_writer_book"))
    @MapsId("writerId")
    private PersonEntity writer;

    // used by hibernate
    BookWriterEntity() {
    }

    // used by the builder
    private BookWriterEntity(BookWriterEntityBuilder builder) {
        this.bookEntity = builder.bookEntity;
        this.writer = builder.writer;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link BookWriterEntityBuilder}
     */
    public static BookWriterEntityBuilder getBuilder() {
        return BookWriterEntityBuilder.getNewInstance();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public BookWriterId getBookWriterId() {
        return bookWriterId;
    }

    public BookEntity getBookEntity() {
        return bookEntity;
    }

    public PersonEntity getPersonEntity() {
        return writer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookWriterEntity)) return false;

        BookWriterEntity that = (BookWriterEntity) o;

        return new EqualsBuilder()
                .append(getBookWriterId(), that.getBookWriterId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBookWriterId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bookEntity", bookEntity)
                .append("personEntity", writer)
                .toString();
    }

    /**
     * The builder of {@link BookWriterEntity}
     */
    public static final class BookWriterEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookWriterEntityBuilder.class);

        private BookEntity bookEntity;
        private PersonEntity writer;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private BookWriterEntityBuilder() {
        }

        // called by the main class to obtain a new instance of its own builder
        private static BookWriterEntityBuilder getNewInstance() {
            return new BookWriterEntityBuilder();
        }

        /**
         * @param bookEntity: The book written by the writer
         * @return {@link BookWriterEntity}
         */
        public BookWriterEntityBuilder addBook(final BookEntity bookEntity) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.bookEntity) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "bookEntity"));
            }

            if (null == bookEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookEntity"));
            }

            LOGGER.info("Adding the book: {}", bookEntity);
            this.bookEntity = bookEntity;
            return this;
        }

        /**
         * @param writer: The writer of the book
         * @return {@link BookWriterEntity}
         */
        public BookWriterEntityBuilder addWriter(final PersonEntity writer) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.writer) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "writer"));
            }

            if (null == writer) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writer"));
            }

            LOGGER.info("Adding the writer of the book: {}", writer);
            this.writer = writer;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         *
         * @return {@link BookWriterEntity}
         */
        public BookWriterEntity build() {
            validate();
            return new BookWriterEntity(this);
        }

        // checks the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.bookEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookEntity"));
            }

            if (null == this.writer) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writer"));
            }
        }
    }
}
