package com.zeta.books.entities.embeddables;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The id for the non eligible items: to be related to a specific command
 */
@Embeddable
public final class NonEligibleCommandId implements Serializable {

    private static final long serialVersionUID = 387999117867196398L;

    @Column(name = "command_id")
    private Long commandId;

    public static NonEligibleCommandId getNewInstance() {
        return new NonEligibleCommandId();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCommandId() {
        return commandId;
    }

    public void setCommandId(Long commandId) {
        this.commandId = commandId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof NonEligibleCommandId)) return false;

        NonEligibleCommandId that = (NonEligibleCommandId) o;

        return new EqualsBuilder()
                .append(getCommandId(), that.getCommandId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCommandId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("commandId", commandId)
                .toString();
    }
}
