package com.zeta.books.entities.embeddables;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The embeddable class to create the IDClass for PersonStatusEntity
 */
@Embeddable
public final class PersonStatusId implements Serializable {

    private static final long serialVersionUID = -8808082693353584287L;

    @Column(name = "person_id")
    private Long personId;

    @Column(name = "status_id")
    private Long statusId;

    /**
     * Returns a new instance of this class to the client
     * @return {@link PersonStatusId}
     */
    public static PersonStatusId getNewInstance() {
        return new PersonStatusId();
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonStatusId)) return false;

        PersonStatusId that = (PersonStatusId) o;

        return new EqualsBuilder()
                .append(getPersonId(), that.getPersonId())
                .append(getStatusId(), that.getStatusId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getPersonId())
                .append(getStatusId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("personId", personId)
                .append("statusId", statusId)
                .toString();
    }
}
