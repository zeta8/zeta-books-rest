package com.zeta.books.entities.embeddables;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The embeddable class to create the IDClass for BookWriterEntity
 */
@Embeddable
public final class BookWriterId  implements Serializable {

   private static final long serialVersionUID = -7352690887126614645L;

    @Column(name = "book_id")
    private Long bookId;

    @Column(name = "writer_id")
    private Long writerId;

    /**
     * Instantiates this embeddedId class and returns it to the client
     * @return {@link BookWriterId}
     */
    public static BookWriterId getNewInstance() {
        return new BookWriterId();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getWriterId() {
        return writerId;
    }

    public void setWriterId(Long writerId) {
        this.writerId = writerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookWriterId)) return false;

        BookWriterId that = (BookWriterId) o;

        return new EqualsBuilder()
                .append(getBookId(), that.getBookId())
                .append(getWriterId(), that.getWriterId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBookId())
                .append(getWriterId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bookId", bookId)
                .append("writerId", writerId)
                .toString();
    }
}
