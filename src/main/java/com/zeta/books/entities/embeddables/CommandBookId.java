package com.zeta.books.entities.embeddables;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The embeddable class to create the IDClass for CommandBooksEntity
 */
@Embeddable
public final class CommandBookId implements Serializable {

    private static final long serialVersionUID = -5417562790357720067L;

    @Column(name = "command_id")
    private Long commandId;

    @Column(name = "book_Id")
    private Long bookId;

    /***
     * Returns a new instance of this class to the client
     * @return {@link BookWriterId}
     */
    public static CommandBookId getNewInstance() {
        return new CommandBookId();
    }

    public Long getCommandId() {
        return commandId;
    }

    public void setCommandId(Long commandId) {
        this.commandId = commandId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandBookId)) return false;

        CommandBookId that = (CommandBookId) o;

        return new EqualsBuilder()
                .append(getCommandId(), that.getCommandId())
                .append(getBookId(), that.getBookId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCommandId())
                .append(getBookId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("commandId", commandId)
                .append("bookId", bookId)
                .toString();
    }
}
