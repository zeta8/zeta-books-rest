package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;

@Entity
@Table(name = "person")
public final class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code", unique = true)
    private String personCode;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "birth_date")
    private Date birthDate;


    // To be used by Hibernate
    // Not to be instantiated directly by the client
    PersonEntity() {
    }

    // Used by the builder
    private PersonEntity(PersonEntityBuilder builder) {
        personCode = builder.personCode;
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.lastName = builder.lastName;
        this.birthDate = builder.birthDate;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link PersonEntityBuilder}
     */
    public static PersonEntityBuilder getBuilder() {
        return PersonEntityBuilder.getNewInstance();
    }

    public Long getId() {
        return id;
    }

    public String getPersonCode() {
        return personCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonEntity)) return false;

        PersonEntity that = (PersonEntity) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("personCode", personCode)
                .append("firstName", firstName)
                .append("middleName", middleName)
                .append("lastName", lastName)
                .append("birthDate", birthDate)
                .toString();
    }

    /**
     * The builder of the class {@link PersonEntity}
     */
    public static class PersonEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(PersonEntityBuilder.class);

        private String personCode;
        private String firstName;
        private String middleName;
        private String lastName;
        private Date birthDate;
        // In order to check if the builder is already instantiated
        private boolean isInstantiated;

        // In order not to be instantiated directly by the client
        private PersonEntityBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static PersonEntityBuilder getNewInstance() {
            return new PersonEntityBuilder();
        }

        /**
         * @param personCode: The unique code of the person
         * @return {@link PersonEntityBuilder}
         */
        public PersonEntityBuilder addPersonCode(final String personCode) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.personCode)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "personCode"));
            }

            if (StringUtils.isBlank(personCode)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personCode"));
            }


            LOGGER.info("Adding the code of the person: {}", personCode);
            this.personCode = personCode;
            return this;
        }

        /**
         * @param firstName: The firstname of the person
         * @return {@link PersonEntityBuilder}
         */
        public PersonEntityBuilder addFirstName(final String firstName) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.firstName)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "firstName"));
            }

            if (StringUtils.isBlank(firstName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "firstName"));
            }


            LOGGER.info("Adding the first name: {}", firstName);
            this.firstName = firstName;
            return this;
        }

        /**
         * @param middleName: The middleName of the person
         * @return {@link PersonEntityBuilder}
         */
        public PersonEntityBuilder addMiddleName(final String middleName) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.middleName)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "middleName"));
            }

            if (!StringUtils.isBlank(middleName)) {
                LOGGER.info("Adding the middle name: {}", middleName);
                this.middleName = middleName;
            }

            return this;
        }

        /**
         * @param lastName: The lastName of the person
         * @return {@link PersonEntityBuilder}
         */
        public PersonEntityBuilder addLastName(final String lastName) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.lastName)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "lastName"));
            }

            if (StringUtils.isBlank(lastName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "lastName"));
            }

            LOGGER.info("Adding the last name: {}", lastName);
            this.lastName = lastName;
            return this;
        }

        /**
         * @param birthDate: The birthDate of the person
         * @return {@link PersonEntityBuilder}
         */
        public PersonEntityBuilder addBirthDate(final LocalDate birthDate) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.birthDate) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "birthDate"));
            }

            if (null == birthDate) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "birthDate"));
            }

            LOGGER.info("Adding the birth date: {}", birthDate);
            this.birthDate = Date.valueOf(birthDate);
            return this;
        }

        /**
         * Instantiates the class {@link PersonEntity} and returns this instance to the client
         *
         * @return {@link PersonEntity}
         */
        public PersonEntity build() {
            validate();
            return new PersonEntity(this);
        }


        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.personCode)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personCode"));
            }

            if (StringUtils.isBlank(this.firstName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "firstName"));
            }

            if (null == this.birthDate) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "birthDate"));
            }

            if (StringUtils.isBlank(this.lastName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "lastName"));
            }
        }
    }
}
