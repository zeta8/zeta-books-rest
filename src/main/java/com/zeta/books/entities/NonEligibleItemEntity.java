package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.entities.embeddables.NonEligibleCommandId;
import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.exceptions.IncoherentValueException;
import com.zeta.books.exceptions.InvalidValueException;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Table containing nonEligible items in a command for commercial use
 */
@Entity
@Table(name = "non_eligible_command_elements")
public final class NonEligibleItemEntity implements Serializable {

    private static final long serialVersionUID = -3207168769241308994L;

    @EmbeddedId
    private final NonEligibleCommandId nonEligibleCommandId = NonEligibleCommandId.getNewInstance();

    @OneToOne
    @JoinColumn(name = "non_eligibility_reason", foreignKey = @ForeignKey(name = "FK_non_eligibility_reason"))
    private NonEligibilityReasonEntity reason;

    @Column(name = "non_existing_book")
    private Long nonExistingBook;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_non_eligible_book"))
    private BookEntity book;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "non_eligible_item", foreignKey = @ForeignKey(name = "FK_non_eligible_book_command"))
    @MapsId("commandId")
    private CommandEntity command;


    // used by hibernate
    NonEligibleItemEntity() {
    }

    // used by the builder
    private NonEligibleItemEntity(NonEligibleItemEntityBuilder builder) {
        reason = builder.reason;
        nonExistingBook = builder.nonExistingBook;
        book = builder.book;
        command = builder.command;
        builder.isInstantiated = true;
    }

    /**
     * used by the client to obtain a new instance of the builder fo this class
     * @return {@link NonEligibleItemEntityBuilder}
     */
    public static NonEligibleItemEntityBuilder getBuilder() {
        return NonEligibleItemEntityBuilder.getNewInstance();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public NonEligibleCommandId getNonEligibleCommandId() {
        return nonEligibleCommandId;
    }

    public NonEligibilityReasonEntity getReason() {
        return reason;
    }

    public Long getNonExistingBook() {
        return nonExistingBook;
    }

    public BookEntity getBook() {
        return book;
    }

    public CommandEntity getCommand() {
        return command;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof NonEligibleItemEntity)) return false;

        NonEligibleItemEntity that = (NonEligibleItemEntity) o;

        return new EqualsBuilder()
                .append(getNonEligibleCommandId(), that.getNonEligibleCommandId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getNonEligibleCommandId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("nonEligibleCommandId", nonEligibleCommandId)
                .append("reason", reason)
                .append("nonExistingBook", nonExistingBook)
                .append("bookEntity", book)
                .append("commandEntity", command)
                .toString();
    }

    /**
     * the builer of {@link NonEligibleItemEntity}
     */
    public static final class NonEligibleItemEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibleItemEntityBuilder.class);

        private NonEligibilityReasonEntity reason;
        private Long nonExistingBook;
        private BookEntity book;
        private CommandEntity command;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private NonEligibleItemEntityBuilder() {
        }

        // used by the main class to obtain a new instance of its own builder
        private static NonEligibleItemEntityBuilder getNewInstance() {
            return new NonEligibleItemEntityBuilder();
        }

        /**
         *
         * @param reason: The reason of no eligibility of the item in command
         * @return {@link NonEligibleItemEntityBuilder}
         */
        public NonEligibleItemEntityBuilder addReason(final NonEligibilityReasonEntity reason) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.reason) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "reason"));
            }

            if (null == reason) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "reason"));
            }

            LOGGER.info("Adding the reason of non eligibility of the item: {}", reason);
            this.reason = reason;
            return this;
        }

        /**
         *
         * @param nonExistingBook: The id of the non existing book in the command
         * @return {@link NonEligibleItemEntityBuilder}
         */
        public NonEligibleItemEntityBuilder addNonExistingBook(final Long nonExistingBook) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.nonExistingBook) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "nonExistingBook"));
            }

            if (null != nonExistingBook) {
                LOGGER.info("Adding id of the non existing book in the command: {}", nonExistingBook);
                this.nonExistingBook = nonExistingBook;
            }

            return this;
        }

        /**
         *
         * @param book: The book in the non eligible command: it is added because it is found but
         *            the number of the existing copies in the stock are less than the number of the copies
         *            included in the client's command
         * @return {@link NonEligibleItemEntityBuilder}
         */
        public NonEligibleItemEntityBuilder addBook(final BookEntity book) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.book) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "book"));
            }

            if (null != book) {
                LOGGER.info("Adding the book that could not be included in the command: {}", book);
                this.book = book;
            }

            return this;
        }

        /**
         *
         * @param command: The command to which all the above information belongs
         * @return {@link NonEligibleItemEntityBuilder}
         */
        public NonEligibleItemEntityBuilder addCommand(final CommandEntity command) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.command) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "command"));
            }

            if (null == command) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "command"));
            }

            LOGGER.info("Adding the command including the non eligible elements: {}", command);
            this.command = command;
            return this;
        }

        /**
         * validates and instantiates the main class and returns this instance to the client
         * @return {@link NonEligibleItemEntity}
         */
        public NonEligibleItemEntity build() {
            validate();
            return new NonEligibleItemEntity(this);
        }

        // validate the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "NonEligibleItemEntity"));
            }

            if (null == this.reason) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "reason"));
            }

            if (null == command) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "command"));
            }

            if (null == this.nonExistingBook && null == this.book) {
                throw new InvalidValueException(String.format(ErrorMessages.ONE_FILED_MANDATORY, "nonExistingBook", "book"));
            }

            if (null != this.nonExistingBook && null != this.book) {
                throw new IncoherentValueException(String.format(ErrorMessages.NON_ACCUMULATIVE_FIELD_ERR, "nonExistingBook", "book"));
            }

            if (null != this.nonExistingBook && !this.reason.getCode().equalsIgnoreCase(NonEligibilityReasonEnum.NOT_FOUND.name())) {
                throw new IncoherentValueException(
                        String.format(
                                ErrorMessages.COUPLED_FIELD_ERR, "nonExistingBook", "reason"));
            }

            if (null != this.book && !this.reason.getCode().equalsIgnoreCase(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.name())) {
                throw new IncoherentValueException(
                        String.format(
                                ErrorMessages.COUPLED_FIELD_ERR, "nonExistingBook", "reason"));
            }

        }
    }
}
