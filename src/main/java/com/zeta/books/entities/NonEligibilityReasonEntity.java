package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Table containing the information on the reason of non-eligibility of an item in the command
 */
@Entity
@Table(name = "non_eligibility_reason")
public final class NonEligibilityReasonEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "label")
    private String label;

    @Column(name = "description")
    private String description;

    // used by hibernate
    // not to be instantiated directly by the client
    NonEligibilityReasonEntity() {
    }

    // used by the builder
    private NonEligibilityReasonEntity(NonEligibilityReasonEntityBuilder builder) {
        code = builder.code;
        label = builder.label;
        description = builder.description;
        builder.isInstantiated = true;
    }

    /**
     * Used by the client to obtain an new instance of the builder of this class
     * @return {@link NonEligibilityReasonEntityBuilder}
     */
    public static NonEligibilityReasonEntityBuilder getBuilder() {
        return NonEligibilityReasonEntityBuilder.getNewInstance();
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof NonEligibilityReasonEntity)) return false;

        NonEligibilityReasonEntity that = (NonEligibilityReasonEntity) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("code", code)
                .append("label", label)
                .append("description", description)
                .toString();
    }

    /**
     * The builder of {@link NonEligibilityReasonEntity}
     */
    public static final class NonEligibilityReasonEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibilityReasonEntityBuilder.class);

        private String code;
        private String label;
        private String description;
        // to control if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private NonEligibilityReasonEntityBuilder() {
        }
        
        // used by the main class to obtain a new instance of ts own builder
        private static NonEligibilityReasonEntityBuilder getNewInstance() {
            return new NonEligibilityReasonEntityBuilder();
        }

        /**
         * @param code: the code of the non eligibility reason
         * @return {@link NonEligibilityReasonEntityBuilder}
         */
        public NonEligibilityReasonEntityBuilder addCode(final String code) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.code)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "code"));
            }

            if (StringUtils.isBlank(code)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "code"));
            }


            LOGGER.info("Adding the code: {}", code);
            this.code = code;
            return this;
        }

        /**
         * @param label: a short description of the non eligibility reason
         * @return {@link NonEligibilityReasonEntityBuilder}
         */
        public NonEligibilityReasonEntityBuilder addLabel(final String label) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.label)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "label"));
            }

            if (StringUtils.isBlank(label)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "label"));
            }


            LOGGER.info("Adding the label: {}", label);
            this.label = label;
            return this;
        }

        /**
         * @param description: a long description of the non eligibility reason
         * @return {@link NonEligibilityReasonEntityBuilder}
         */
        public NonEligibilityReasonEntityBuilder addDescription(final String description) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.description)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "description"));
            }

            if (StringUtils.isBlank(description)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "description"));
            }


            LOGGER.info("Adding the description: {}", description);
            this.description = description;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         *
         * @return {@link NonEligibilityReasonEntity}
         */
        public NonEligibilityReasonEntity build() {
            validate();
            return new NonEligibilityReasonEntity(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.code)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "code"));
            }

            if (StringUtils.isBlank(this.label)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "label"));
            }

            if (StringUtils.isBlank(this.description)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "description"));
            }
        }
    }
}
