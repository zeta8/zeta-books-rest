package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "books")
public final class BookEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "main_title")
    private String mainTitle;

    @Column(name = "sub_title")
    private String subTitle;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "price", columnDefinition = "Decimal(6,2)")
    private BigDecimal price;


    // Used by hibernate
    // not to be instantiated directly by the client
    BookEntity() {
    }

    // Used by the builder
    private BookEntity(BookEntityBuilder builder) {
        mainTitle = builder.mainTitle;
        subTitle = builder.subTitle;
        isbn = builder.isbn;
        price = builder.price;
        builder.isInstantiated = true;
    }

    /**
     * To be called by the client to obtain a new instance of the builder of this class
     * @return {@link BookEntityBuilder}
     */
    public static BookEntityBuilder getBuilder() {
        return BookEntityBuilder.getNewInstance();
    }

    public Long getId() {
        return id;
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getIsbn() {
        return isbn;
    }

    public BigDecimal getPrice() {
        return price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookEntity)) return false;

        BookEntity that = (BookEntity) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("mainTitle", mainTitle)
                .append("subTitle", subTitle)
                .append("ISBN", isbn)
                .append("price", price)
                .toString();
    }

    /**
     * The builder for the class {@link BookEntity}
     */
    public static class BookEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookEntityBuilder.class);

        private String mainTitle;
        private String subTitle;
        private String isbn;
        private BigDecimal price;
        // Checks if the builder has been already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private BookEntityBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static BookEntityBuilder getNewInstance() {
            return new BookEntityBuilder();
        }

        /**
         *
         * @param mainTitle: The main title of the book
         * @return {@link BookEntityBuilder}
         */
        public BookEntityBuilder addMainTitle(final String mainTitle) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.mainTitle)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "mainTitle"));
            }

            if (StringUtils.isBlank(mainTitle)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "mainTitle"));
            }


            LOGGER.info("Adding the main title: {}", mainTitle);
            this.mainTitle = mainTitle;
            return this;
        }

        /**
         *
         * @param subTitle: The subtitle of the book
         * @return {@link BookEntityBuilder}
         */
        public BookEntityBuilder addSubTitle(final String subTitle) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.subTitle)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "subTitle"));
            }

            if (!StringUtils.isBlank(subTitle)) {
                LOGGER.info("Adding the subtitle: {}", subTitle);
                this.subTitle = subTitle;
            }

            return this;
        }

        /**
         *
         * @param ISBN: The International Standard Book Number
         * @return {@link BookEntityBuilder}
         */
        public BookEntityBuilder addISBN(final String ISBN) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.isbn)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "ISBN"));
            }

            if (StringUtils.isBlank(ISBN)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "ISBN"));
            }

            LOGGER.info("Adding the ISBN: {}", ISBN);
            this.isbn = ISBN;
            return this;
        }

        /**
         *
         * @param price: The price of the book
         * @return {@link BookEntityBuilder}
         */
        public BookEntityBuilder addPrice(final BigDecimal price) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.price) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "price"));
            }

            if (null == price) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "price"));
            }

            LOGGER.info("Adding the price of the book: {}", price);
            this.price = price;
            return this;
        }


        /**
         * Instantiates the main class and returns it to the client
         * @return {@link BookEntity}
         */
        public BookEntity build() {
            validate();
            return new BookEntity(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.mainTitle)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "mainTitle"));
            }

            if (StringUtils.isBlank(isbn)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "ISBN"));
            }

            if (null == this.price) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "price"));
            }

        }
    }
}
