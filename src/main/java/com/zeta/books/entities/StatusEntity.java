package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

@Entity
@Table(name = "status")
public final class StatusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "label")
    private String label;

    @Column(name = "description")
    private String description;

    // To be used by hibernate
    // Not to be instantiated directly by the client
    StatusEntity() {
    }

    // To be used by the builder
    private StatusEntity(StatusEntityBuilder builder) {
        code = builder.code;
        label = builder.label;
        description = builder.description;
        builder.isInstantiated = true;
    }

    /**
     * To be called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link StatusEntityBuilder}
     */
    public static StatusEntityBuilder getBuilder() {
        return StatusEntityBuilder.getNewInstance();
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof StatusEntity)) return false;

        StatusEntity that = (StatusEntity) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("code", code)
                .append("label", label)
                .append("description", description)
                .toString();
    }

    /**
     * The builder of {@link StatusEntity}
     */
    public static final class StatusEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(StatusEntityBuilder.class);

        private String code;
        private String label;
        private String description;
        // Checks if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private StatusEntityBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static StatusEntityBuilder getNewInstance() {
            return new StatusEntityBuilder();
        }

        /**
         * @param code: the code of the person status
         * @return {@link StatusEntityBuilder}
         */
        public StatusEntityBuilder addCode(final String code) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.code)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "code"));
            }

            if (StringUtils.isBlank(code)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "code"));
            }


            LOGGER.info("Adding the code: {}", code);
            this.code = code;
            return this;
        }

        /**
         * @param label: a short description of the person status
         * @return {@link StatusEntityBuilder}
         */
        public StatusEntityBuilder addLabel(final String label) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.label)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "label"));
            }

            if (StringUtils.isBlank(label)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "label"));
            }


            LOGGER.info("Adding the label: {}", label);
            this.label = label;
            return this;
        }

        /**
         * @param description: a long description of the person status
         * @return {@link StatusEntityBuilder}
         */
        public StatusEntityBuilder addDescription(final String description) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.description)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "description"));
            }

            if (StringUtils.isBlank(description)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "description"));
            }


            LOGGER.info("Adding the description: {}", description);
            this.description = description;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         *
         * @return {@link StatusEntity}
         */
        public StatusEntity build() {
            validate();
            return new StatusEntity(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.code)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "code"));
            }

            if (StringUtils.isBlank(this.label)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "label"));
            }

            if (StringUtils.isBlank(this.description)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "description"));
            }
        }
    }
}
