package com.zeta.books.entities;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.entities.embeddables.PersonStatusId;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "person_status")
public final class PersonStatusEntity implements Serializable {

    @EmbeddedId
    private final PersonStatusId personStatusId = new PersonStatusId();

    @JoinColumn(name = "person_id", foreignKey = @ForeignKey(name = "FK_person"), insertable = false, updatable = false)
    @MapsId("personId")
    @ManyToOne(fetch = FetchType.EAGER)
    private PersonEntity personEntity;

    @JoinColumn(name = "status_id", foreignKey = @ForeignKey(name = "FK_status"), insertable = false, updatable = false)
    @MapsId("statusId")
    @ManyToOne(fetch = FetchType.EAGER)
    private StatusEntity statusEntity;

    // Used by hibernate
    // Not to be instantiated directly by the client
    PersonStatusEntity() {
    }

    // Used by the builder
    private PersonStatusEntity(PersonStatusEntityBuilder builder) {
        personEntity = builder.personEntity;
        statusEntity = builder.statusEntity;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     * @return {@link PersonStatusEntityBuilder}
     */
    public static PersonStatusEntityBuilder getBuilder() {
        return PersonStatusEntityBuilder.getNewInstance();
    }

    public PersonStatusId getPersonStatusId() {
        return personStatusId;
    }

    public PersonEntity getPersonEntity() {
        return personEntity;
    }

    public StatusEntity getStatusEntity() {
        return statusEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonStatusEntity)) return false;

        PersonStatusEntity that = (PersonStatusEntity) o;

        return new EqualsBuilder()
                .append(getPersonStatusId(), that.getPersonStatusId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getPersonStatusId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("personEntity", personEntity)
                .append("statusEntity", statusEntity)
                .toString();
    }

    /**
     * The builder of {@link PersonStatusEntity}
     */
    public static final class PersonStatusEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(PersonStatusEntityBuilder.class);

        private PersonEntity personEntity;
        private StatusEntity statusEntity;
        // Checks if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private PersonStatusEntityBuilder() {
        }

        // Called by the main class to obtain a new instance of its own builder
        private static PersonStatusEntityBuilder getNewInstance() {
            return new PersonStatusEntityBuilder();
        }


        /**
         *
         * @param personEntity: The person
         * @return {@link PersonStatusEntityBuilder}
         */
        public PersonStatusEntityBuilder addPersonEntity(final PersonEntity personEntity) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.personEntity) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "personEntity"));
            }

            if (null == personEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personEntity"));
            }

            LOGGER.info("Adding personEntity: {}", personEntity);
            this.personEntity = personEntity;
            return this;
        }

        /**
         *
         * @param statusEntity: The status of the person
         * @return {@link PersonStatusEntityBuilder}
         */
        public PersonStatusEntityBuilder addStatusEntity(final StatusEntity statusEntity) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.statusEntity) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "statusEntity"));
            }

            if (null == statusEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "statusEntity"));
            }

            LOGGER.info("Adding statusEntity: {}", statusEntity);
            this.statusEntity = statusEntity;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link PersonStatusEntity}
         */
        public PersonStatusEntity build() {
            validate();
            return new PersonStatusEntity(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.personEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personEntity"));
            }

            if (null == this.statusEntity) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "statusEntity"));
            }
        }
    }
}
