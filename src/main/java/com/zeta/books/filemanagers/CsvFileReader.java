package com.zeta.books.filemanagers;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.zeta.books.BooksApplication;
import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * A class to manipulate he data red from a csv file
 */
public class CsvFileReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvFileReader.class);

    private String fileName;

    /**
     * Returns a new Instance of this class to the client
     *
     * @return {@link CsvFileReader}
     */
    public static CsvFileReader getNewInstance() {
        return new CsvFileReader();
    }

    /**
     * Initializes the filename of the csv file
     *
     * @param fileName: The name of the csv file to be read and parsed
     * @return {@link CsvFileReader}
     */
    public CsvFileReader withFileName(final String fileName) {
        if (!StringUtils.isBlank(this.fileName)) {
            throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, fileName));
        }

        if (StringUtils.isBlank(fileName)) {
            throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "fileName"));
        }
        LOGGER.info("Adding the filename: {}", fileName);
        this.fileName = fileName;
        return this;
    }

    /**
     * Returns the result of the parsing the csv file as list of array of strings
     *
     * @return A list of list of strings
     */
    public List<String[]> readCsvFile() {
        List<String[]> parseResult = null;
        try (InputStream targetStream = BooksApplication.class.getClassLoader().getResourceAsStream(fileName)){
            parseResult = getCsvData(targetStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parseResult;
    }

    // To parse the cvs and return the result as a list of arrays of strings
    private List<String[]> getCsvData(final InputStream targetStream) {
        List<String[]> parseResult = null;
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setLineSeparator("\n");
        CsvParser parser = new CsvParser(settings);
        try {
            parseResult = parser.parseAll(new InputStreamReader(targetStream, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return parseResult;
    }
}