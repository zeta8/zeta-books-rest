package com.zeta.books.errors;

public enum ErrorCode {

    RESS_NOT_FOUND("Resource Not Found"),
    ILLEGAL_STATE("Illegal State"),
    RESS_CONFLICT("Resource Conflit"),
    INVALID_VALUE("Invalid Value"),
    INTERN_ERR("Internal Error"),
    REQUIRED_VALUE("Obligatory Parameter"),
    MALFORMED_ERROR("Invalid Argument"),
    INVALID_SIZE("The size of the string invalide"),
    INCOHERENT_VALUE("Incoherent values"),
    ILLEGAL_ACTION("This action is not permitted");

    private final String description;

    ErrorCode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}