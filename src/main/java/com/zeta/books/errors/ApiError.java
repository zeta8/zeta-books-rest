package com.zeta.books.errors;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public final class ApiError {

    private final String id;
    private final String code;
    private final String message;

    /**
     * <b>Creates an object containing a unique error.</b>
     *
     * @param code  Code {@link HttpStatus} of teh error
     * @param message Describing the error..
     */
    public ApiError(String code, String message) {
        super();
        this.code = code;
        this.message = message;
        this.id = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) + "-" + code;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


    public String getId() {
        return id;
    }
}
