package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The DTO for creating new Status for Person Object
 */
public final class PersonStatusDto {

    @ApiModelProperty(value = "The code for the status", position = 0, example = "CUS")
    private String code;

    @ApiModelProperty(value = "The short description for the code status", position = 1, example = "Customer")
    private String label;

    @ApiModelProperty(value = "The detailed description for the code status", position = 2, example = "A customer of zeta")
    private String description;

    // Used By jackson
    PersonStatusDto() {
    }

    public PersonStatusDto(PersonStatusDtoBuilder builder) {
        code = builder.code;
        label = builder.label;
        description = builder.description;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     * @return {@link PersonStatusDtoBuilder}
     */
    public static PersonStatusDtoBuilder getBuilder() {
        return PersonStatusDtoBuilder.getNewInstance();
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonStatusDto)) return false;

        PersonStatusDto that = (PersonStatusDto) o;

        return new EqualsBuilder()
                .append(getCode(), that.getCode())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCode())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("code", code)
                .append("label", label)
                .append("description", description)
                .toString();
    }

    /**
     * The builder of {@link PersonStatusDto}
     */
    public static final class PersonStatusDtoBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(PersonStatusDtoBuilder.class);

        private String code;
        private String label;
        private String description;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private PersonStatusDtoBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static PersonStatusDtoBuilder getNewInstance() {
            return new PersonStatusDtoBuilder();
        }

        /**
         * @param code: the code of the person status
         * @return {@link PersonStatusDtoBuilder}
         */
        public PersonStatusDtoBuilder addCode(final String code) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.code)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "code"));
            }

            if (StringUtils.isBlank(code)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "code"));
            }


            LOGGER.info("Adding the code: {}", code);
            this.code = code;
            return this;
        }

        /**
         * @param label: a short description of the person status
         * @return {@link PersonStatusDtoBuilder}
         */
        public PersonStatusDtoBuilder addLabel(final String label) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.label)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "label"));
            }

            if (StringUtils.isBlank(label)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "label"));
            }


            LOGGER.info("Adding the label: {}", label);
            this.label = label;
            return this;
        }

        /**
         * @param description: a long description of the person status
         * @return {@link PersonStatusDtoBuilder}
         */
        public PersonStatusDtoBuilder addDescription(final String description) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.description)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "description"));
            }

            if (StringUtils.isBlank(description)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "description"));
            }


            LOGGER.info("Adding the description: {}", description);
            this.description = description;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         *
         * @return {@link PersonStatusDto}
         */
        public PersonStatusDto build() {
            validate();
            return new PersonStatusDto(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.code)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "code"));
            }

            if (StringUtils.isBlank(this.label)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "label"));
            }

            if (StringUtils.isBlank(this.description)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "description"));
            }
        }
    }
}
