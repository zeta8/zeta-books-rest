package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains a list of {@link BookCreationDtoOut}
 */
public final class BookListCreationDtoOut {

    @ApiModelProperty(value = "The list of ids of the created books", example = "[1, 2, 3]")
    private List<BookCreationDtoOut> createdBooks;

    // used by jackson
    // not to be instantiated directly by the client
    BookListCreationDtoOut() {
    }

    // used by the builder
    private BookListCreationDtoOut(BookListCreationDtoOutBuilder builder) {
        createdBooks = builder.createdBooks;
        builder.isInstantiated = true;
    }

    /**
     * Used by the client to obtain a new instance of the builder of this class
     * @return {@link BookListCreationDtoOutBuilder}
     */
    public static BookListCreationDtoOutBuilder getBuilder() {
        return BookListCreationDtoOutBuilder.getNewInstance();
    }

    public List<BookCreationDtoOut> getCreatedBooks() {
        return createdBooks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookListCreationDtoOut)) return false;

        BookListCreationDtoOut that = (BookListCreationDtoOut) o;

        return new EqualsBuilder()
                .append(getCreatedBooks(), that.getCreatedBooks())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCreatedBooks())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("createdBooks", createdBooks)
                .toString();
    }

    /**
     * The builder of {@link BookListCreationDtoOut}
     */
    public static final class BookListCreationDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookListCreationDtoOutBuilder.class);

        private List<BookCreationDtoOut> createdBooks;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private BookListCreationDtoOutBuilder() {
        }

        // used by the main class to obtain a new instance of its won builder
        private static BookListCreationDtoOutBuilder getNewInstance() {
            return new BookListCreationDtoOutBuilder();
        }

        /**
         *
         * @param bookCreationDtoOut: the class containing the created book id
         * @return {@link BookListCreationDtoOutBuilder}
         */
        public BookListCreationDtoOutBuilder addCreatedBook(final BookCreationDtoOut bookCreationDtoOut) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == bookCreationDtoOut) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookCreationDtoOut"));
            }

            if (null == this.createdBooks) {
                this.createdBooks = new ArrayList<>();
            }

            LOGGER.info("Adding the created book id {} to the list of books", bookCreationDtoOut.getBookId());
            this.createdBooks.add(bookCreationDtoOut);
            return this;
        }

        /**
         * Validates and instantiates the main class and returns this instance to the client
         * @return {@link BookListCreationDtoOut}
         */
        public BookListCreationDtoOut build() {
            validate();
            return new BookListCreationDtoOut(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "BookListCreationDtoOut"));
            }

            // instantiates the class with an empty list in the case no book could be created
            if (null == this.createdBooks) {
                this.createdBooks = new ArrayList<>();
            }
        }
    }
}
