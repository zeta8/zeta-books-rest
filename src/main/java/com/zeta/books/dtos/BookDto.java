package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The class for encoding all information on the books in zeta:
 * The object will be transferred on web via the webservices.
 */
public final class BookDto {
    @ApiModelProperty(value = "The main title of the book", example = "The Adapted Mind")
    private String mainTitle;

    @ApiModelProperty(value = "The sub title of the book", position = 1, example = "Evolutionary Psychology and the Generation of Culture")
    private String subTitle;

    @ApiModelProperty(value = "The International Standard Book Number", position = 2, example = "0-19-506023-7")
    private String isbn;

    @ApiModelProperty(value = "The prices of the book", position = 3, example = "25.49")
    private BigDecimal price;

    @ApiModelProperty(value = "The list of teh writers of the book", position = 4)
    private List<PersonDto> writers;


    /**
     * To be used by jackson in the process of serialization and deserialization.
     * in order not to be instantiated directly by the client
     */
    BookDto() {
    }

    /**
     * used by the builder of this class to instantiate BookDto
     *
     * @param builder: {@link BookDtoBuilder}
     */
    private BookDto(BookDtoBuilder builder) {
        mainTitle = builder.mainTitle;
        subTitle = builder.subTitle;
        writers = builder.writers;
        isbn = builder.isbn;
        price = builder.price;
        builder.isInstantiated = true;
    }

    /**
     * To be called by the client in order to obtain a new instance of the builder o the main class
     *
     * @return {@link BookDtoBuilder}
     */
    public static BookDtoBuilder getBuilder() {
        return BookDtoBuilder.getNewInstance();
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public List<PersonDto> getWriters() {
        return writers;
    }

    public String getIsbn() {
        return isbn;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookDto)) return false;

        BookDto bookDto = (BookDto) o;

        return new EqualsBuilder()
                .append(getMainTitle(), bookDto.getMainTitle())
                .append(getSubTitle(), bookDto.getSubTitle())
                .append(getWriters(), bookDto.getWriters())
                .append(getIsbn(), bookDto.getIsbn())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getMainTitle())
                .append(getSubTitle())
                .append(getWriters())
                .append(getIsbn())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("mainTitle", mainTitle)
                .append("subTitle", subTitle)
                .append("writer", writers)
                .append("ISBN", isbn)
                .append("price", price)
                .toString();
    }

    /**
     * The builder of the class {@link BookDto}
     */
    public static final class BookDtoBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookDtoBuilder.class);

        private String mainTitle;
        private String subTitle;
        private List<PersonDto> writers;
        private String isbn;
        private BigDecimal price;
        // In order to check if the builder is already instantiated or not
        private boolean isInstantiated;

        // In order not to be instantiated directly by the client
        private BookDtoBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static BookDtoBuilder getNewInstance() {
            return new BookDtoBuilder();
        }

        /**
         * @param mainTitle The main title of the book
         * @return {@link BookDtoBuilder}
         */
        public BookDtoBuilder addMainTitle(final String mainTitle) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.mainTitle)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "mainTitle"));
            }

            if (StringUtils.isBlank(mainTitle)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "mainTitle"));
            }

            LOGGER.info("Adding the main title: {}", mainTitle);
            this.mainTitle = mainTitle;
            return this;
        }

        /**
         * @param subTitle The sub title of the book
         * @return {@link BookDtoBuilder}
         */
        public BookDtoBuilder addSubTitle(final String subTitle) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.subTitle)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "subTitle"));
            }

            if (!StringUtils.isBlank(subTitle)) {
                LOGGER.info("Adding the subtitle: {}", subTitle);
                this.subTitle = subTitle;
            }

            return this;
        }

        /**
         * @param writer The writer of the book
         * @return {@link BookDtoBuilder}
         */
        public BookDtoBuilder addWriter(final PersonDto writer) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == writer) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writers"));
            }

            if (null == this.writers) {
                this.writers = new ArrayList<>();
            }


            LOGGER.info("Adding the writers: {}", writers);
            this.writers.add(writer);
            return this;
        }

        /**
         * @param writers The writers of the book
         * @return {@link BookDtoBuilder}
         */
        public BookDtoBuilder addWriters(final List<PersonDto> writers) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == writers || writers.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writers"));
            }

            if (null == this.writers) {
                this.writers = new ArrayList<>();
            }


            LOGGER.info("Adding the writers: {}", writers);
            this.writers = writers;
            return this;
        }


        /**
         * @param ISBN The International Standard Book Number
         * @return {@link BookDtoBuilder}
         */
        public BookDtoBuilder addISBN(final String ISBN) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.isbn)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "ISBN"));
            }

            if (StringUtils.isBlank(ISBN)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "ISBN"));
            }

            LOGGER.info("Adding the ISBN: {}", ISBN);
            this.isbn = ISBN;
            return this;
        }

        /**
         * @param price The price of the book
         * @return {@link BookDtoBuilder}
         */
        public BookDtoBuilder addPrice(final BigDecimal price) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.price) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "price"));
            }

            if (null == price) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "price"));
            }

            LOGGER.info("Adding the price of the book: {}", price);
            this.price = price;
            return this;
        }

        /**
         * Instantiates the class BookDto and returns this instance to the client
         *
         * @return {@link BookDto}
         */
        public BookDto build() {
            validate();
            return new BookDto(this);
        }

        // Validates the integrity of the object before instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "bookDto"));
            }

            if (StringUtils.isBlank(this.mainTitle)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "mainTitle"));
            }

            if (null == this.writers || this.writers.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writers"));
            }

            if (null == price) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "price"));
            }

            if (StringUtils.isBlank(this.isbn)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "ISBN"));
            }
        }

    }
}
