package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The DTO for creating a new command for the client
 */
public final class CommandCreationDto {

    @ApiModelProperty(value = "The id of the client", example = "21")
    private Long clientId;

    @ApiModelProperty(value = "The list of the books to be commanded", position = 1)
    private List<BookToBeCommanded> toBeCommandedBooks;


    // to be used by Jackson
    CommandCreationDto() {
    }

    // to be used by builder
    private CommandCreationDto(CommandCreationDtoBuilder builder) {
        this.clientId = builder.clientId;
        this.toBeCommandedBooks = builder.toBeCommandedBooks;
        builder.isInstantiated = true;
    }

    /**
     * called by the client to obtain a new instance of the builder of this class
     * @return {@link CommandCreationDtoBuilder}
     */
    public static CommandCreationDtoBuilder getBuilder() {
        return CommandCreationDtoBuilder.getNewInstance();
    }

    public Long getClientId() {
        return clientId;
    }

    public List<BookToBeCommanded> getToBeCommandedBooks() {
        return toBeCommandedBooks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandCreationDto)) return false;

        CommandCreationDto that = (CommandCreationDto) o;

        return new EqualsBuilder()
                .append(getClientId(), that.getClientId())
                .append(getToBeCommandedBooks(), that.getToBeCommandedBooks())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getClientId())
                .append(getToBeCommandedBooks())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("clientId", clientId)
                .append("toBeCommandedBooks", toBeCommandedBooks)
                .toString();
    }

    /**
     * The builder of {@link CommandCreationDto}
     */
    public static final class CommandCreationDtoBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandCreationDtoBuilder.class);

        private Long clientId;
        private List<BookToBeCommanded> toBeCommandedBooks;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private CommandCreationDtoBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static CommandCreationDtoBuilder getNewInstance() {
            return new CommandCreationDtoBuilder();
        }

        /**
         *
         * @param clientId: The id of teh client
         * @return {@link CommandCreationDtoBuilder}
         */
        public CommandCreationDtoBuilder addClientId(final Long clientId) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.clientId) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "clientId"));
            }

            if (null == clientId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "clientId"));
            }

            LOGGER.info("Adding the client id: {}", clientId);
            this.clientId = clientId;
            return this;
        }

        /**
         *
         * @param toBeCommandedBooks: The list of all books (their ID) to be commanded
         * @return {@link CommandCreationDtoBuilder}
         */
        public CommandCreationDtoBuilder addCommandedBooks(final List<BookToBeCommanded> toBeCommandedBooks) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.toBeCommandedBooks) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "toBeCommandedBooks"));
            }

            if (null == toBeCommandedBooks || toBeCommandedBooks.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "toBeCommandedBooks"));
            }

            LOGGER.info("Adding the list of the books to be commanded: {}", toBeCommandedBooks);
            this.toBeCommandedBooks = toBeCommandedBooks;
            return this;
        }


        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link CommandCreationDto}
         */
        public CommandCreationDto build() {
            validate();
            return new CommandCreationDto(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.clientId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "clientId"));
            }

            if (null == this.toBeCommandedBooks || this.toBeCommandedBooks.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandedBooks"));
            }

        }
    }
}
