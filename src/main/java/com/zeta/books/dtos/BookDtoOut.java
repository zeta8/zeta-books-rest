package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * The class containing the list of all books
 */
public final class BookDtoOut {

    private List<BookDto> books;

    // used by Jackson
    // not to be instantiated directly by the client
    BookDtoOut() {
    }

    // used by the builder
    private BookDtoOut(BookDtoOutBuilder builder) {
        this.books = builder.books;
        builder.isInstantiated = true;
    }

    /**
     * Used by the client to obtain a new instance of the builder of this class
     * @return {@link BookDtoOutBuilder}
     */
    public static BookDtoOutBuilder getBuilder() {
        return BookDtoOutBuilder.getNewInstance();
    }

    public List<BookDto> getBooks() {
        return books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookDtoOut)) return false;

        BookDtoOut that = (BookDtoOut) o;

        return new EqualsBuilder()
                .append(getBooks(), that.getBooks())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBooks())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("books", books)
                .toString();
    }

    /**
     * The builder of {@link BookDtoOut}
     */
    public static final class BookDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookDtoOutBuilder.class);

        private List<BookDto> books;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private BookDtoOutBuilder() {
        }

        // used by the min class to obtain a new instance of the builder of this class
        private static BookDtoOutBuilder getNewInstance() {
            return new BookDtoOutBuilder();
        }

        /**
         * Adds a book to the list of books
         * @param book: the book to be added to the list of books
         * @return {@link BookDtoOutBuilder}
         */
        public BookDtoOutBuilder addBook(final BookDto book) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            if (null == this.books) {
                this.books = new ArrayList<>();
            }

            LOGGER.info("Adding the book {} to the list of the books", book);
            this.books.add(book);
            return this;
        }

        /**
         *
         * @param books: the list of all books
         * @return {@link BookDtoOutBuilder}
         */
        public BookDtoOutBuilder addBooks(final List<BookDto> books) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.books) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "books"));
            }

            if (null == books) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "books"));
            }

            LOGGER.info("Adding the list of books {}", books);
            this.books = books;
            return this;
        }

        /**
         * validates and instantiates a new instance fo the main class and returns this instance to the client
         * @return {@link BookDtoOut}
         */
        public BookDtoOut build() {
            validate();
            return new BookDtoOut(this);
        }

        // validates the integrity of the objet before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "BookDto"));
            }
            // an empty list can be returned to the client, in the case no books found
            if (null == this.books) {
                this.books = new ArrayList<>();
            }
        }
    }
}
