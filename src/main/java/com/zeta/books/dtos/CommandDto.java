package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * The DTO for the command of the client
 */
public final class CommandDto {

    @ApiModelProperty(value = "The id of the client", example = "21")
    private Long clientId;

    @ApiModelProperty(value = "The list of the books to be commanded", position = 1)
    private List<CommandedBook> commandedBooks;

    @ApiModelProperty(value = "The total price of the command", position = 2, example = "254.98")
    private BigDecimal totalPrice;

    // to be used by Jackson
    CommandDto() {
    }

    // to be used by builder
    private CommandDto(CommandDtoBuilder builder) {
        this.clientId = builder.clientId;
        this.commandedBooks = builder.commandedBooks;
        this.totalPrice = builder.totalPrice;
        builder.isInstantiated = true;
    }

    /**
     * called by the client to obtain a new instance of the builder of this class
     * @return {@link CommandDtoBuilder}
     */
    public static CommandDtoBuilder getBuilder() {
        return CommandDtoBuilder.getNewInstance();
    }

    public Long getClientId() {
        return clientId;
    }

    public List<CommandedBook> getCommandedBooks() {
        return commandedBooks;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandDto)) return false;

        CommandDto that = (CommandDto) o;

        return new EqualsBuilder()
                .append(getClientId(), that.getClientId())
                .append(getCommandedBooks(), that.getCommandedBooks())
                .append(getTotalPrice(), that.getTotalPrice())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getClientId())
                .append(getCommandedBooks())
                .append(getTotalPrice())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("clientId", clientId)
                .append("commandedBooks", commandedBooks)
                .append("totalPrice", totalPrice)
                .toString();
    }

    /**
     * The builder of {@link CommandDto}
     */
    public static final class CommandDtoBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandDtoBuilder.class);

        private Long clientId;
        private List<CommandedBook> commandedBooks;
        private BigDecimal totalPrice;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private CommandDtoBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static CommandDtoBuilder getNewInstance() {
            return new CommandDtoBuilder();
        }

        /**
         *
         * @param clientId: The id of teh client
         * @return {@link CommandDtoBuilder}
         */
        public CommandDtoBuilder addClientId(final Long clientId) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.clientId) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "clientId"));
            }

            if (null == clientId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "clientId"));
            }

            LOGGER.info("Adding the client id: {}", clientId);
            this.clientId = clientId;
            return this;
        }

        /**
         *
         * @param commandedBooks: The list of all commanded books
         * @return {@link CommandDtoBuilder}
         */
        public CommandDtoBuilder addCommandedBooks(final List<CommandedBook> commandedBooks) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.commandedBooks) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "commandedBooks"));
            }

            if (null == commandedBooks || commandedBooks.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandedBooks"));
            }

            LOGGER.info("Adding the list of the commanded books: {}", commandedBooks);
            this.commandedBooks = commandedBooks;
            return this;
        }


        /**
         *
         * @param totalPrice: The total price of the command
         * @return {@link CommandDtoBuilder}
         */
        public CommandDtoBuilder addTotalPrice(final BigDecimal totalPrice) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.totalPrice) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "totalPrice"));
            }

            if (null == totalPrice) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "totalPrice"));
            }

            LOGGER.info("Adding the total price of the command: {}", totalPrice);
            this.totalPrice = totalPrice;
            return this;
        }

        /**
         * calculates the total price and adds it to the builder
         * @return {@link CommandDtoBuilder}
         */
        public CommandDtoBuilder calculateAndAddTotalPrice() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == this.commandedBooks || this.commandedBooks.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandedBooks"));
            }

            if (null != this.totalPrice) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "totalPrice"));
            }

            BigDecimal localTotalPrice = new BigDecimal("0.00");
            for (CommandedBook book : commandedBooks) {
                localTotalPrice = localTotalPrice.add(book.getBook().getPrice().multiply(new BigDecimal(book.getNumberOfCopies())));
            }

            LOGGER.info("Adding the total price of the command: {}", localTotalPrice);
            this.totalPrice = localTotalPrice;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link CommandDto}
         */
        public CommandDto build() {
            validate();
            return new CommandDto(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.clientId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "clientId"));
            }

            if (null == this.commandedBooks || this.commandedBooks.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandedBooks"));
            }

            if (null == this.totalPrice) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "totalPrice"));
            }

        }
    }
}
