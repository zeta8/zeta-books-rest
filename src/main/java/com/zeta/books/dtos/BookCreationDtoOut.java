package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The DTO for the output of the creation of a new Book
 */
public final class BookCreationDtoOut {

    @ApiModelProperty(value = "The id of the created book", example = "1452")
    private Long bookId;

    // used by jackson
    BookCreationDtoOut() {
    }

    // used by the builder
    private BookCreationDtoOut(BookCreationDtoOutBuilder builder) {
        bookId = builder.bookId;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     * @return {@link BookCreationDtoOutBuilder}
     */
    public static BookCreationDtoOutBuilder getBuilder() {
        return BookCreationDtoOutBuilder.getNewInstance();
    }

    public Long getBookId() {
        return bookId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonCreationDtoOut)) return false;

        PersonCreationDtoOut that = (PersonCreationDtoOut) o;

        return new EqualsBuilder()
                .append(getBookId(), that.getPersonId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBookId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bookId", bookId)
                .toString();
    }

    /**
     * The builder of {@link BookCreationDtoOut}
     */
    public static final class BookCreationDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookCreationDtoOutBuilder.class);

        private Long bookId;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private BookCreationDtoOutBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static BookCreationDtoOutBuilder getNewInstance() {
            return new BookCreationDtoOutBuilder();
        }

        /**
         *
         * @param bookId: The id of newly created book
         * @return {@link BookCreationDtoOutBuilder}
         */
        public BookCreationDtoOutBuilder addBookId(final Long bookId) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.bookId) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "bookId"));
            }

            if (null == bookId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookId"));
            }

            LOGGER.info("Adding the book id: {}", bookId);
            this.bookId = bookId;
            return this;
        }

        /**
         *  Instantiates the class {@link BookCreationDtoOut} and returns this instance to the client
         * @return {@link BookCreationDtoOut}
         */
        public BookCreationDtoOut build() {
            validate();
            return new BookCreationDtoOut(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.bookId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookId"));
            }
        }

    }
}
