package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The DTO for the books inside a command
 */
public final class CommandedBook {
    @ApiModelProperty(value = "The commanded book")
    private BookDto book;

    @ApiModelProperty(value = "The number of copies of the commanded book", position = 1, example = "21")
    private Integer numberOfCopies;

    // To be used by Jackson
    CommandedBook() {
    }

    // used by the builder
    private CommandedBook(CommandedBookBuilder builder) {
        book = builder.book;
        numberOfCopies = builder.numberOfCopies;
        builder.isInstantiated = true;
    }

    /**
     * called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link CommandedBookBuilder}
     */
    public static CommandedBookBuilder getBuilder() {
        return CommandedBookBuilder.getNewInstance();
    }

    public BookDto getBook() {
        return book;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandedBook)) return false;

        CommandedBook that = (CommandedBook) o;

        return new EqualsBuilder()
                .append(getBook(), that.getBook())
                .append(getNumberOfCopies(), that.getNumberOfCopies())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBook())
                .append(getNumberOfCopies())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("book", book)
                .append("numberOfCopies", numberOfCopies)
                .toString();
    }

    /**
     * The builder of {@link CommandedBook}
     */
    public static final class CommandedBookBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandedBookBuilder.class);

        private BookDto book;
        private Integer numberOfCopies;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private CommandedBookBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static CommandedBookBuilder getNewInstance() {
            return new CommandedBookBuilder();
        }

        /**
         * @param book: The book to be added to the command
         * @return {@link CommandedBookBuilder}
         */
        public CommandedBookBuilder addBook(final BookDto book) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.book) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "book"));
            }

            if (null == book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            LOGGER.info("Adding the book to the command: {}", book);
            this.book = book;
            return this;
        }

        /**
         * @param numberOfCopies: The number of copies of the book to be added to the command
         * @return {@link CommandedBookBuilder}
         */
        public CommandedBookBuilder addNumberOfCopies(final Integer numberOfCopies) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.numberOfCopies) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "numberOfCopies"));
            }

            if (null == numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }

            LOGGER.info("Adding the numberOfCopies of the book to the command: {}", numberOfCopies);
            this.numberOfCopies = numberOfCopies;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link CommandedBook}
         */
        public CommandedBook build() {
            validate();
            return new CommandedBook(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            if (null == this.numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }
        }
    }
}
