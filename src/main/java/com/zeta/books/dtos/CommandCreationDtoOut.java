package com.zeta.books.dtos;


import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.exceptions.ZetaIllegalStateException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The DTO for the output of the creation of a command
 */
public final class CommandCreationDtoOut {

    @ApiModelProperty(value = "The id of the created command", example = "21")
    private Long commandId;

    @ApiModelProperty(value = "The message to the client", position = 1, example = "The following books could not be added to your command")
    private String message;

    @ApiModelProperty(value = "The list of nonEligible items", position = 2, example = "[{reason: \"NOT_FOUND\", item: 1}]")
    private List<NonEligibleItemDtoOut> nonEligibleItems;

    // to be used by Jackson
    CommandCreationDtoOut() {
    }

    public CommandCreationDtoOut(CommandCreationDtoOutBuilder builder) {
        commandId = builder.commandId;
        message = builder.message;
        nonEligibleItems = builder.nonEligibleItems;
        builder.isInstantiated = true;
    }

    /**
     * called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link CommandCreationDtoOutBuilder}
     */
    public static CommandCreationDtoOutBuilder getBuilder() {
        return CommandCreationDtoOutBuilder.getNewInstance();
    }

    public Long getCommandId() {
        return commandId;
    }

    public String getMessage() {
        return message;
    }

    public List<NonEligibleItemDtoOut> getNonEligibleItems() {
        return nonEligibleItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandCreationDtoOut)) return false;

        CommandCreationDtoOut that = (CommandCreationDtoOut) o;

        return new EqualsBuilder()
                .append(getCommandId(), that.getCommandId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCommandId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("commandId", commandId)
                .append("message", message)
                .append("nonEligibleItems", nonEligibleItems)
                .toString();
    }

    /**
     * The builder of {@link CommandCreationDtoOut}
     */
    public static final class CommandCreationDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandCreationDtoOutBuilder.class);

        private Long commandId;
        private String message;
        private List<NonEligibleItemDtoOut> nonEligibleItems;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private CommandCreationDtoOutBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static CommandCreationDtoOutBuilder getNewInstance() {
            return new CommandCreationDtoOutBuilder();
        }

        /**
         * @param commandId: The id of newly created command
         * @return {@link CommandCreationDtoOutBuilder}
         */
        public CommandCreationDtoOutBuilder addCommandId(final Long commandId) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.commandId) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "commandId"));
            }

            if (null == commandId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandId"));
            }

            LOGGER.info("Adding the command id: {}", commandId);
            this.commandId = commandId;
            return this;
        }

        /**
         * @param message: the message to be added in the case all of the books in the command could not be sent to the client
         * @return {@link CommandCreationDtoOutBuilder}
         */
        public CommandCreationDtoOutBuilder addMessage(final String message) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.message)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "message"));
            }

            if (!StringUtils.isBlank(message)) {
                LOGGER.info("Adding the message: {}", message);
                this.message = message;
            }

            return this;
        }

        /**
         * @param nonEligibleItems: the list of the books in the command that could not be sent to the client
         * @return {@link CommandCreationDtoOutBuilder}
         */
        public CommandCreationDtoOutBuilder addNonEligibleItems(final List<NonEligibleItemDtoOut> nonEligibleItems) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.nonEligibleItems) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "nonEligibleItems"));
            }

            if (null != nonEligibleItems && !nonEligibleItems.isEmpty()) {
                LOGGER.info("Adding the nonAddedBook: {}", nonEligibleItems);
                this.nonEligibleItems = nonEligibleItems;
            }

            LOGGER.info("Adding the nonAddedBook: {}", nonEligibleItems);
            this.nonEligibleItems = nonEligibleItems;

            return this;
        }


        /**
         * Instantiates the main class and returns this instance to the client
         *
         * @return {@link CommandCreationDtoOut}
         */
        public CommandCreationDtoOut build() {
            validate();
            return new CommandCreationDtoOut(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.commandId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "commandId"));
            }

            if (!messageFieldIsValid()) {
                throw new ZetaIllegalStateException(String.format(ErrorMessages.COUPLED_FIELD_ERR, "message", "nonEligibleItems"));
            }
        }

        // Validates if the message field is valid
        // the message field should be set if and only if the list of non added books is non-empty
        private boolean messageFieldIsValid() {
            return (StringUtils.isBlank(this.message) && (null == nonEligibleItems || nonEligibleItems.isEmpty())) ||
                    (!StringUtils.isBlank(this.message) && null != nonEligibleItems && !nonEligibleItems.isEmpty());
        }
    }
}
