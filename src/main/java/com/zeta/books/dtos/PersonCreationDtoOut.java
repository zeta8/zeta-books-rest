package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class PersonCreationDtoOut {
    @ApiModelProperty(value = "The id of the created person", position = 0, example = "1452")
    private Long personId;

    // used by jackson
    PersonCreationDtoOut() {
    }

    // used by the builder
    private PersonCreationDtoOut(PersonCreationDtoOutBuilder builder) {
        personId = builder.personId;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     * @return {@link PersonCreationDtoOutBuilder}
     */
    public static PersonCreationDtoOutBuilder getBuilder() {
        return PersonCreationDtoOutBuilder.getNewInstance();
    }

    public Long getPersonId() {
        return personId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonCreationDtoOut)) return false;

        PersonCreationDtoOut that = (PersonCreationDtoOut) o;

        return new EqualsBuilder()
                .append(getPersonId(), that.getPersonId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getPersonId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("personId", personId)
                .toString();
    }

    /**
     * The builder of {@link PersonCreationDtoOut}
     */
    public static final class PersonCreationDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(PersonCreationDtoOutBuilder.class);

        private Long personId;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private PersonCreationDtoOutBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static PersonCreationDtoOutBuilder getNewInstance() {
            return new PersonCreationDtoOutBuilder();
        }

        /**
         *
         * @param personId: The id of newly created person
         * @return {@link PersonCreationDtoOutBuilder}
         */
        public PersonCreationDtoOutBuilder addPersonId(final Long personId) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.personId) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "personId"));
            }

            if (null == personId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personId"));
            }

            LOGGER.info("Adding the person id: {}", personId);
            this.personId = personId;
            return this;
        }

        /**
         *  Instantiates the class {@link PersonDto} and returns this instance to the client
         * @return {@link PersonCreationDtoOut}
         */
        public PersonCreationDtoOut build() {
            validate();
            return new PersonCreationDtoOut(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.personId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personId"));
            }
        }

    }
}
