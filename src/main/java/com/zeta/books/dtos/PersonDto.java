package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public final class PersonDto {

    @ApiModelProperty(value = "The unique code of the person", example = "CD-1")
    private String personCode;

    @ApiModelProperty(value = "The first name of the person", position = 1, example = "Eric")
    private String firstName;

    @ApiModelProperty(value = "The middle name of the person", position = 2, example = "Walter")
    private String middleName;

    @ApiModelProperty(value = "The last name of the person", position = 3, example = "Von Dike")
    private String lastName;

    @ApiModelProperty(value = "The birthdate name of the person", position = 4, example = "1980-01-25")
    private LocalDate birthDate;

    @ApiModelProperty(value = "The status of the person", position = 5, example = "CUS")
    private StatusEnum status;

    // ued By Jackson
    // not to be instantiated directly by the client
    PersonDto() {
    }

    // used by the builder
    private PersonDto(PersonDtoBuilder builder) {
        personCode = builder.personCode;
        firstName = builder.firstName;
        middleName = builder.middleName;
        lastName = builder.lastName;
        birthDate = builder.birthDate;
        status = builder.status;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link PersonDtoBuilder}
     */
    public static PersonDtoBuilder getBuilder() {
        return PersonDtoBuilder.getNewInstance();
    }

    public String getPersonCode() {
        return personCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonDto)) return false;

        PersonDto personDto = (PersonDto) o;

        return new EqualsBuilder()
                .append(getPersonCode(), personDto.getPersonCode())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getPersonCode())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("personCode", personCode)
                .append("firstName", firstName)
                .append("middleName", middleName)
                .append("lastName", lastName)
                .append("birthDate", birthDate)
                .append("status", status)
                .toString();
    }

    /**
     * The builder of hte class {@link PersonDto}
     */
    public static final class PersonDtoBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(PersonDtoBuilder.class);

        private String personCode;
        private String firstName;
        private String middleName;
        private String lastName;
        private LocalDate birthDate;
        private StatusEnum status;
        // checks if the builder if already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private PersonDtoBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static PersonDtoBuilder getNewInstance() {
            return new PersonDtoBuilder();
        }

        /**
         * @param personCode: The unique code of the person
         * @return {@link PersonDtoBuilder}
         */
        public PersonDtoBuilder addPersonCode(final String personCode) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.personCode)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "personCode"));
            }

            if (StringUtils.isBlank(personCode)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personCode"));
            }


            LOGGER.info("Adding the person code: {}", personCode);
            this.personCode = personCode;
            return this;
        }

        /**
         * @param firstName: The firstname of the person
         * @return {@link PersonDtoBuilder}
         */
        public PersonDtoBuilder addFirstName(final String firstName) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.firstName)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "firstName"));
            }

            if (StringUtils.isBlank(firstName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "firstName"));
            }


            LOGGER.info("Adding the first name: {}", firstName);
            this.firstName = firstName;
            return this;
        }

        /**
         * @param middleName: The middleName of the person
         * @return {@link PersonDtoBuilder}
         */
        public PersonDtoBuilder addMiddleName(final String middleName) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.middleName)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "middleName"));
            }

            if (!StringUtils.isBlank(middleName)) {
                LOGGER.info("Adding the middle name: {}", middleName);
                this.middleName = middleName;
            }

            return this;
        }

        /**
         * @param lastName: The lastName of the person
         * @return {@link PersonDtoBuilder}
         */
        public PersonDtoBuilder addLastName(final String lastName) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.lastName)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "lastName"));
            }

            if (StringUtils.isBlank(lastName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "lastName"));
            }

            LOGGER.info("Adding the last name: {}", lastName);
            this.lastName = lastName;
            return this;
        }

        /**
         * @param birthDate: The birthDate of the person
         * @return {@link PersonDtoBuilder}
         */
        public PersonDtoBuilder addBirthDate(final LocalDate birthDate) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.birthDate) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "birthDate"));
            }

            if (null == birthDate) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "birthDate"));
            }

            LOGGER.info("Adding the birth date: {}", birthDate);
            this.birthDate = birthDate;
            return this;
        }

        /**
         * @param status: The status of the person
         * @return {@link PersonDtoBuilder}
         */
        public PersonDtoBuilder addStatus(final StatusEnum status) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.status) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "status"));
            }

            if (null == status) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "status"));
            }

            LOGGER.info("Adding the status of the person: {}", status);
            this.status = status;
            return this;
        }

        /**
         * Instantiates the class {@link PersonDto} and returns this instance to the client
         *
         * @return {@link PersonDto}
         */
        public PersonDto build() {
            validate();
            return new PersonDto(this);
        }


        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.personCode)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personCode"));
            }

            if (StringUtils.isBlank(this.firstName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "firstName"));
            }

            if (null == this.birthDate) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "birthDate"));
            }

            if (StringUtils.isBlank(this.lastName)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "lastName"));
            }

            if (null == this.status) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "status"));
            }
        }
    }
}
