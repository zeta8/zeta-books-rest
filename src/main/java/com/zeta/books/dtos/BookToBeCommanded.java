package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The DTO for the books inside a command
 */
public final class BookToBeCommanded {
    @ApiModelProperty(value = "The id of the book to be commanded", example = "1")
    private Long bookId;

    @ApiModelProperty(value = "The number of copies of the commanded book", position = 1, example = "21")
    private Integer numberOfCopies;

    // To be used by Jackson
    BookToBeCommanded() {
    }

    // used by the builder
    private BookToBeCommanded(BookToBeCommandedBuilder builder) {
        bookId = builder.bookId;
        numberOfCopies = builder.numberOfCopies;
        builder.isInstantiated = true;
    }

    /**
     * called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link BookToBeCommandedBuilder}
     */
    public static BookToBeCommandedBuilder getBuilder() {
        return BookToBeCommandedBuilder.getNewInstance();
    }

    public Long getBookId() {
        return bookId;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookToBeCommanded)) return false;

        BookToBeCommanded that = (BookToBeCommanded) o;

        return new EqualsBuilder()
                .append(getBookId(), that.getBookId())
                .append(getNumberOfCopies(), that.getNumberOfCopies())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBookId())
                .append(getNumberOfCopies())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bookId", bookId)
                .append("numberOfCopies", numberOfCopies)
                .toString();
    }

    /**
     * The builder of {@link BookToBeCommanded}
     */
    public static final class BookToBeCommandedBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookToBeCommandedBuilder.class);

        private Long bookId;
        private Integer numberOfCopies;
        // checks if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private BookToBeCommandedBuilder() {
        }

        // called by the amin class to obtain a new instance of its own builder
        private static BookToBeCommandedBuilder getNewInstance() {
            return new BookToBeCommandedBuilder();
        }

        /**
         * @param bookId: The ID of the book to be added to the command
         * @return {@link BookToBeCommandedBuilder}
         */
        public BookToBeCommandedBuilder addBook(final Long bookId) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.bookId) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "bookId"));
            }

            if (null == bookId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "bookId"));
            }

            LOGGER.info("Adding the book to the command: {}", bookId);
            this.bookId = bookId;
            return this;
        }

        /**
         * @param numberOfCopies: The number of copies of the book to be added to the command
         * @return {@link BookToBeCommandedBuilder}
         */
        public BookToBeCommandedBuilder addNumberOfCopies(final Integer numberOfCopies) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.numberOfCopies) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "numberOfCopies"));
            }

            if (null == numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }

            LOGGER.info("Adding the numberOfCopies of teh book to the command: {}", numberOfCopies);
            this.numberOfCopies = numberOfCopies;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link BookToBeCommanded}
         */
        public BookToBeCommanded build() {
            validate();
            return new BookToBeCommanded(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.bookId) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            if (null == this.numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }
        }
    }
}
