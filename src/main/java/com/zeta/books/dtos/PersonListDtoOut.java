package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ResourceConflictException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Conatins a list of {@link PersonDto}
 */
public final class PersonListDtoOut {

    private List<PersonDto> persons;

    // used by Jackson
    // not to be instantiated directly by the client
    PersonListDtoOut() {
    }

    // used by the builder
    private PersonListDtoOut(PersonListDtoOutBuilder builder) {
        persons = builder.persons;
        builder.isInstantiated = true;
    }


    /**
     * Used by the client to obtain a new instance of the builder of this class
     * @return {@link PersonListDtoOutBuilder}
     */
    public static PersonListDtoOutBuilder getBuilder() {
        return PersonListDtoOutBuilder.getNewInstance();
    }

    public List<PersonDto> getPersons() {
        return persons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersonListDtoOut)) return false;

        PersonListDtoOut that = (PersonListDtoOut) o;

        return new EqualsBuilder()
                .append(getPersons(), that.getPersons())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getPersons())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("persons", persons)
                .toString();
    }

    /**
     * the builder of {@link PersonListDtoOut}
     */
    public static final class PersonListDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(PersonListDtoOutBuilder.class);

        private List<PersonDto> persons;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private PersonListDtoOutBuilder() {
        }

        // used by the main class to obtain a new instance of its own builder
        private static PersonListDtoOutBuilder getNewInstance() {
            return new PersonListDtoOutBuilder();
        }

        /**
         *
         * @param person: The person to be added to the list
         * @return {@link PersonListDtoOutBuilder}
         */
        public PersonListDtoOutBuilder addPerson(final PersonDto person) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == person) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "person"));
            }

            // Initialize the list for the first time
            if (null == this.persons) {
                this.persons = new ArrayList<>();
            }

            if (this.persons.contains(person)) {
                throw new ResourceConflictException(String.format(ErrorMessages.RESOURCE_CONFLICT_ERR, "person", person.getPersonCode()));
            }

            LOGGER.info("Adding the person {}", person);
            this.persons.add(person);
            return this;
        }

        /**
         * validates and instantiates the main class and returns this instance to the client
         * @return {@link PersonListDtoOut}
         */
        public PersonListDtoOut build() {
            validate();
            return new PersonListDtoOut(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "PersonListDtoOutBuilder"));
            }

            if (null == this.persons) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "persons"));
            }
        }
    }
}
