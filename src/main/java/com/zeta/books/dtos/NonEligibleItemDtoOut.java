package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ZetaIllegalStateException;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * For creating a new NoEligible Item
 */
public final class NonEligibleItemDtoOut {

    @ApiModelProperty(value = "The reason of non-eligibility of the item", example = "NOT_FOUND")
    private NonEligibilityReasonEnum reason;

    @ApiModelProperty(value = "The id of eno-eligible book", position = 1, example = "1")
    private Long item;

    // used by jackson
    NonEligibleItemDtoOut() {
    }

    // used by the builder
    private NonEligibleItemDtoOut(NonEligibleItemDtoOutBuilder builder) {
        reason = builder.reason;
        item = builder.item;
        builder.isInstantiated = true;
    }

    /**
     * used by the client to obtain a new instance of the builder of this class
     * @return {@link NonEligibleItemDtoOutBuilder}
     */
    public static NonEligibleItemDtoOutBuilder getBuilder() {
        return NonEligibleItemDtoOutBuilder.getNewInstance();
    }

    public NonEligibilityReasonEnum getReason() {
        return reason;
    }

    public Long getItem() {
        return item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof NonEligibleItemDtoOut)) return false;

        NonEligibleItemDtoOut that = (NonEligibleItemDtoOut) o;

        return new EqualsBuilder()
                .append(reason, that.reason)
                .append(item, that.item)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(reason)
                .append(item)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("reason", reason)
                .append("item", item)
                .toString();
    }

    /**
     * The builder of {@link NonEligibleItemDtoOut}
     */
    public static final class NonEligibleItemDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibleItemDtoOutBuilder.class);

        private NonEligibilityReasonEnum reason;
        private Long item;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // not to be instantiated directly by the client
        private NonEligibleItemDtoOutBuilder() {
        }

        // called by the main class to obtain a new instance of its own builder
        private static NonEligibleItemDtoOutBuilder getNewInstance() {
            return new NonEligibleItemDtoOutBuilder();
        }

        /**
         *
         * @param reason: the reason of non-Eligibility of the book in the command
         * @return {@link NonEligibleItemDtoOutBuilder}
         */
        public NonEligibleItemDtoOutBuilder addReason(final NonEligibilityReasonEnum reason) {
            if (this.isInstantiated) {
                throw new ZetaIllegalStateException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.reason) {
                throw new ZetaIllegalStateException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "reason"));
            }

            if (null == reason) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "reason"));
            }
            LOGGER.info("Adding the reason of non-Eligibility : {}", reason);
            this.reason = reason;

            return this;
        }

        /**
         *
         * @param item: the id of the non-eligible item
         * @return {@link NonEligibleItemDtoOutBuilder}
         */
        public NonEligibleItemDtoOutBuilder addItem(final Long item) {
            if (this.isInstantiated) {
                throw new ZetaIllegalStateException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.item) {
                throw new ZetaIllegalStateException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "item"));
            }

            if (null == item) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "item"));
            }

            LOGGER.info("Adding the of of non-eligible item: {}", item);
            this.item = item;
            return this;
        }

        /**
         * validates and instantiates the main class and returns this instance to the client
         * @return {@link NonEligibleItemDtoOut}
         */
        public NonEligibleItemDtoOut build() {
            validate();
            return new NonEligibleItemDtoOut(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalStateException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "NonEligibleItemDtoOut"));
            }

            if (null == this.reason) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "reason"));
            }

            if (null == this.item) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "item"));
            }
        }
    }
}
