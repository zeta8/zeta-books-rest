package com.zeta.books.dtos;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.exceptions.MandatoryValueException;
import com.zeta.books.exceptions.ResourceConflictException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains a list of {@link PersonStatusDto}
 */
public final class StatusListDtoOut {

    private List<PersonStatusDto> statusDtoList;

    // used by Jackson
    // not to be instantiated directly by the client
    StatusListDtoOut() {
    }

    // used by the builder
    private StatusListDtoOut(StatusListDtoOutBuilder builder) {
        statusDtoList = builder.statusDtoList;
        builder.isInstantiated = true;
    }

    // used by the client to obtain a new instance of teh builder of this class
    public static StatusListDtoOutBuilder getBuilder() {
        return StatusListDtoOutBuilder.getNewInstance();
    }

    public List<PersonStatusDto> getStatusDtoList() {
        return statusDtoList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof StatusListDtoOut)) return false;

        StatusListDtoOut that = (StatusListDtoOut) o;

        return new EqualsBuilder()
                .append(getStatusDtoList(), that.getStatusDtoList())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getStatusDtoList())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("statusDtoList", statusDtoList)
                .toString();
    }

    /**
     * The builder of {@link StatusListDtoOut}
     */
    public static final class StatusListDtoOutBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(StatusListDtoOutBuilder.class);

        private List<PersonStatusDto> statusDtoList;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private StatusListDtoOutBuilder() {
        }

        // used by the main class to obtain a anew instance of its own builder
        private static StatusListDtoOutBuilder getNewInstance() {
            return new StatusListDtoOutBuilder();
        }

        /**
         *
         * @param status: The status to be added to the list
         * @return {@link StatusListDtoOutBuilder}
         */
        public StatusListDtoOutBuilder addStatus(final PersonStatusDto status) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null == status) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "status"));
            }

            // instantiates the list for the first time
            if (null == this.statusDtoList) {
                this.statusDtoList = new ArrayList<>();
            }

            if (this.statusDtoList.contains(status)) {
                throw new ResourceConflictException(String.format(ErrorMessages.RESOURCE_CONFLICT_ERR, "status", status.getCode()));
            }

            LOGGER.info("Adding the status {}", status);
            this.statusDtoList.add(status);
            return this;
        }

        /**
         * Validates and instantiates the main class and returns this instance to the client
         * @return {@link StatusListDtoOut}
         */
        public StatusListDtoOut build() {
            validate();
            return new StatusListDtoOut(this);
        }

        // validates the integrity of the object before actually building it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "StatusListDtoOutBuilder"));
            }

            if (null == this.statusDtoList) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "statusDtoList"));
            }
        }
    }
}
