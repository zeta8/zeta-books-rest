package com.zeta.books.controllers;


import com.zeta.books.dtos.CommandCreationDto;
import com.zeta.books.dtos.CommandCreationDtoOut;
import com.zeta.books.dtos.CommandDto;
import com.zeta.books.dtos.PersonStatusDto;
import com.zeta.books.services.interfaces.CommandService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(tags = {"Commands"})
@SwaggerDefinition(tags = @Tag(name = "Commands", description = "APIs for managing the commands of the person"))
@RestController
@RequestMapping(value = "commands")
public class CommandController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandController.class);

    private final CommandService commandService;

    public CommandController(final CommandService commandService) {
        this.commandService = commandService;
    }

    /**
     * <b>Operation for creating a new command for the person</b>
     *
     * @return {@link PersonStatusDto}
     */
    @PostMapping( consumes =  {MediaType.APPLICATION_JSON_VALUE} ,produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a new Command", notes = "Service for creating new command for the person")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The new command created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public CommandCreationDtoOut createPersonStatus(@RequestBody final CommandCreationDto command) {
        LOGGER.info("Calling the API Post zeta/commands");

        return commandService.createCommand(command);
    }

    /**
     * <b>Operation for finding a command by the given ID</b>
     *
     * @return {@link PersonStatusDto}
     */
    @GetMapping(value = "/{commandId}",produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Returns a Command", notes = "Service for Retrieving a command by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The command found"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.OK)
    public CommandDto findCommandById(@PathVariable final Long commandId) {
        LOGGER.info("Calling the API Post zeta/commands/id with the parameter {}", commandId);

        return commandService.findCommandById(commandId);
    }
}
