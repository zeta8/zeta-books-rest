package com.zeta.books.controllers;

import com.zeta.books.dtos.*;
import com.zeta.books.services.interfaces.PersonService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(tags = {"Persons"})
@SwaggerDefinition(tags = @Tag(name = "Persons", description = "APIs for managing the persons related to company zeta"))
@RestController
@RequestMapping(value = "persons", produces = {MediaType.APPLICATION_JSON_VALUE})
public class PersonController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

    private final PersonService personService;

    public PersonController(final PersonService personService) {
        this.personService = personService;
    }

    /**
     * <b>Operation for creating a new status for the person</b>
     *
     * @return {@link PersonStatusDto}
     */
    @PostMapping(value = "/status", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a new Status", notes = "Service for creating new status for the person")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The new status created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public PersonStatusDto createPersonStatus(@RequestBody final PersonStatusDto personStatusDto) {
        LOGGER.info("Calling the API Post zeta/persons/status");

        return personService.createPersonStatus(personStatusDto);
    }


    /**
     * <b>Operation for creating a new person</b>
     *
     * @return {@link PersonStatusDto}
     */
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a new Person", notes = "Service for creating new person")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The new person created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public PersonCreationDtoOut createPerson(@RequestBody final PersonDto personDto) {
        LOGGER.info("Calling the API Post zeta/persons");

        return personService.createPerson(personDto);
    }

    /**
     * <b>Operation for finding a person by ID</b>
     *
     * @return {@link PersonStatusDto}
     */
    @GetMapping(value = "{id}")
    @ApiOperation(value = "Finds a new Person", notes = "Service for finding the person for the given ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The person found"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.OK)
    public PersonDto findPersonById(@PathVariable final Long id) {
        LOGGER.info("Calling the API Post zeta/persons/{id}, with the parameter: {}", id);

        return personService.findPersonById(id);
    }

    /**
     * <b>Operation for initializing the database with some persons.</b>
     */
    @PostMapping(value = "/add", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Initializes some persons in the data base", notes = "Service for initializing the data base with some persons")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The persons created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public PersonListDtoOut initializePersons() {
        LOGGER.info("Calling the API Post zeta/persons/add");

        return personService.addAllPersonsFormFile();
    }

    /**
     * <b>Operation for initializing the database with some persons.</b>
     */
    @PostMapping(value = "status/add")
    @ApiOperation(value = "Initializes some status for persons in the database", notes = "Service for initializing the data base with some status")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The statuses created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public StatusListDtoOut initializePersonStatus() {
        LOGGER.info("Calling the API Post zeta/persons/status/add");

        return personService.addAllStatus();
    }
}
