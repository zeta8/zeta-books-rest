package com.zeta.books.controllers;


import com.zeta.books.dtos.*;
import com.zeta.books.services.interfaces.BookService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(tags = {"Books"})
@SwaggerDefinition(tags = @Tag(name = "Books", description = "APIs for managing the books of the company zeta"))
@RestController
@RequestMapping(value = "books")
public class BooksController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BooksController.class);

    private final BookService bookService;

    public BooksController(final BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * <b>Operation for listing all the existing books</b>
     *
     * @return The list of {@link BookDto}
     */
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Returns the list of all books", notes = "Service for restitution the list of books")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The books found"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.OK)
    public BookDtoOut listBooks() {
        LOGGER.info("Calling the API GET zeta/books");

        return bookService.listAllBooks();

    }


    /**
     * <b>Operation for finding the book with the given ID</b>
     *
     * @param id : The id of the book.
     * @return {@link BookDto}
     */
    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Returns the book for the given ID", notes = "Service for restitution of one book with the given ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The book found"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.OK)
    public BookDto findBookById(@PathVariable("id") final Long id) {
        LOGGER.info("Calling the API GET zeta/books/{bookId} with the parameter {}", id);


        return bookService.findBookById(id);
    }

    /**
     * <b>Operation for creating a new book</b>
     *
     * @return {@link PersonStatusDto}
     */
    @PostMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE,} ,produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a new Book", notes = "Service for creating new book")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The new book created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public BookCreationDtoOut createBook(@RequestBody final BookCreationDtoIn book) {
        LOGGER.info("Calling the API Post zeta/persons, withe in input {}", book);

        return bookService.addBook(book);
    }

    /**
     * <b>Operation for creating a new book</b>
     *
     * @return {@link PersonStatusDto}
     */
    @PostMapping(value = "/books", consumes =  {MediaType.APPLICATION_JSON_VALUE} ,produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a list of new books", notes = "Service for creating a list of new books")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The new books created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public BookListCreationDtoOut createBookList(@RequestBody final List<BookCreationDtoIn> books) {
        LOGGER.info("Calling the API Post zeta/persons");

        return bookService.addBookList(books);
    }

    /**
     * <b>Operation for creating a new book</b>
     *
     * @return {@link PersonStatusDto}
     */
    @PostMapping(value = "/add", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a list of new books for testing the API", notes = "Service for creating a list of new books for the test of the API")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The new books created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @ResponseStatus(value = HttpStatus.CREATED)
    public BookListCreationDtoOut initializeBooks() {
        LOGGER.info("Calling the API Post zeta/persons");

        return bookService.addAllBooks();
    }
}
