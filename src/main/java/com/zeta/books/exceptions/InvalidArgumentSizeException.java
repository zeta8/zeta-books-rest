package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

/**
 * <b>The exception thrown in the case of invalid size.</b>
 */
public class InvalidArgumentSizeException extends ZetaException {
    public InvalidArgumentSizeException(String message) {
        super(ErrorCode.INVALID_SIZE, message);
    }
}
