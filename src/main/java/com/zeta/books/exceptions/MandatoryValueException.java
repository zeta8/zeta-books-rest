package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

public class MandatoryValueException extends ZetaException {
    public MandatoryValueException(String message) {
        super(ErrorCode.REQUIRED_VALUE, message);
    }
}
