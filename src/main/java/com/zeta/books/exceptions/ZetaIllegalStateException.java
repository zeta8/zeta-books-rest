package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

public class ZetaIllegalStateException extends ZetaException {

    public ZetaIllegalStateException(String message) {
        super(ErrorCode.ILLEGAL_STATE, message);
    }
}
