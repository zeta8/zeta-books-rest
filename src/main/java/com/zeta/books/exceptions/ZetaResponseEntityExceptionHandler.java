package com.zeta.books.exceptions;

import com.zeta.books.errors.ApiError;
import com.zeta.books.errors.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The callas for handling exceptions thrown by the Application.
 */
@ControllerAdvice
public class ZetaResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger SUBLOGGER = LoggerFactory.getLogger(ZetaResponseEntityExceptionHandler.class);
    private static final String INTERNAL_ERROR = "Service unavailable";
    static final Map<String, String> annotationCodeMap;

    static {
        annotationCodeMap = new HashMap<>();
        annotationCodeMap.put("NotBlank", ErrorCode.REQUIRED_VALUE.name());
        annotationCodeMap.put("NotNull", ErrorCode.REQUIRED_VALUE.name());
        annotationCodeMap.put("Size", ErrorCode.INVALID_SIZE.name());
    }

    @ExceptionHandler(ZetaMethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleSigmaMethodArgumentNotValidException(final ZetaMethodArgumentNotValidException ex) {
        final List<ApiError> apiErrors = ex.getBindingResult().getAllErrors().stream()
                .map(e -> {
                    String field = ((FieldError) e).getField();
                    if (field.contains(".")) {
                        field = field.substring(field.lastIndexOf(".") + 1);
                    }
                    ApiError apiError = new ApiError(annotationCodeMap.get(e.getCode()), field + " " + e.getDefaultMessage());
                    SUBLOGGER.warn("Validation Error {} {} - {}", apiError.getId(), ex.getCode(), ((FieldError) e).getField() + " " + e.getDefaultMessage());
                    return apiError;
                })
                .collect(Collectors.toList());
        return ResponseEntity.badRequest().body(apiErrors);
    }
    /**
     * Handles the exceptions {@link ResourceConflictException} thrown by the API.
     *
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(ResourceConflictException.class)
    public ResponseEntity<ApiError> handleResourceConflictException(final ResourceConflictException ex) {
        return generateResponse(ex, HttpStatus.CONFLICT);
    }

    /**
     * Handles the exceptions {@link ResourceNotFoundException} thrown by the API.
     *
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ApiError> handleResourceNotFoundException(final ResourceNotFoundException ex) {
        return generateResponse(ex, HttpStatus.NOT_FOUND);
    }

    /**
     * Handles the exceptions {@link InvalidValueException} thrown by the API.
     *
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(InvalidValueException.class)
    public ResponseEntity<ApiError> handleInvalidValueException(final InvalidValueException ex) {
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exceptions {@link MandatoryValueException} thrown by the API.
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(MandatoryValueException.class)
    public ResponseEntity<ApiError> handleObligatoryValueException(final MandatoryValueException ex) {
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exceptions {@link ZetaIllegalStateException} thrown by the API.
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(ZetaIllegalStateException.class)
    public ResponseEntity<ApiError> handleSigmaIllegalStateException(final ZetaIllegalStateException ex) {
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exceptions {@link IncoherentValueException} thrown by the API.
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(IncoherentValueException.class)
    public ResponseEntity<ApiError> handleIncoherentValueException(final IncoherentValueException ex) {
        return generateResponse(ex, HttpStatus.CONFLICT);
    }

    /**
     * Handles the exceptions {@link MalFormedException} thrown by the API.
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(MalFormedException.class)
    public ResponseEntity<ApiError> handleMalFormedException(final MalFormedException ex) {
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exceptions {@link InvalidArgumentSizeException} thrown by the API.
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(InvalidArgumentSizeException.class)
    public ResponseEntity<ApiError> handleInvalidArgumentSizeException(final InvalidArgumentSizeException ex) {
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exceptions {@link Exception} thrown by the API.
     *
     * @param ex The thrown exception.
     * @return The structured response.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(final Exception ex) {
        final ApiError apiError = new ApiError(ErrorCode.INTERN_ERR.name(), INTERNAL_ERROR);
        SUBLOGGER.warn("{} {} - {}", apiError.getId(), apiError.getCode(), ex.getMessage());
        SUBLOGGER.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(apiError);
    }

    @ExceptionHandler(ZetaIllegalActionException.class)
    public ResponseEntity<ApiError> handleSigmaIllegalActionException(final ZetaIllegalActionException ex) {
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ApiError> generateResponse(final ZetaException ex, HttpStatus status) {
        ApiError apiError = new ApiError(ex.getCode(), ex.getMessage());
        butFirstLog(apiError.getId(), ex);
        return ResponseEntity.status(status).body(apiError);
    }

    private void butFirstLog(final String exId, final ZetaException ex) {
        SUBLOGGER.warn("{} {} - {}", exId, ex.getCode(), ex.getMessage());
        SUBLOGGER.error(ex.getMessage(), ex);
    }
}
