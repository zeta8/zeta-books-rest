package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

/**
 * <b>The exception thrown when the action is not permitted.</b>
 */
public class ZetaIllegalActionException extends ZetaException {
    public ZetaIllegalActionException(String message) {
        super(ErrorCode.ILLEGAL_ACTION, message);
    }
}
