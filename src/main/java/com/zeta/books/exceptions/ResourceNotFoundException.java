package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

public class ResourceNotFoundException extends ZetaException {

    public ResourceNotFoundException(String message) {
        super(ErrorCode.RESS_NOT_FOUND, message);
    }
}
