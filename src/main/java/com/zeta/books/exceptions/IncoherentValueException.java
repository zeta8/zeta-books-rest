package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

/**
 * <b>The exception thrown when the different input values are not coherent.</b>
 */
public class IncoherentValueException extends ZetaException {

    public IncoherentValueException(String message) {
        super(ErrorCode.INCOHERENT_VALUE, message);
    }
}
