package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

/**
 * <b>The exception thrown when an argument is malformed.</b>
 */
public class MalFormedException extends ZetaException {
    public MalFormedException(String message) {
        super(ErrorCode.MALFORMED_ERROR, message);
    }
}
