package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

/**
 * The main exception of the application.
 */
public class ZetaException extends RuntimeException {

    /**
     * The exception code
     */
    private final ErrorCode code;
    private static final ErrorCode DEFAULT_CODE = ErrorCode.INTERN_ERR;

    /**
     * Constructor without any parameters
     */
    public ZetaException() {
        super();
        this.code = DEFAULT_CODE;
    }

    /**
     * Constructor with a message
     *
     * @param message The explicit message of teh exception
     */
    public ZetaException(String message) {
        super(message);
        this.code = DEFAULT_CODE;
    }

    /**
     * The constructor with an error code and an explicit message
     *
     * @param code    The code of teh exception
     * @param message The detailed content of the exception
     */
    public ZetaException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * @return The code of teh exception
     */
    public String getCode() {
        return code.name();
    }

    /**
     *
     * @return The label of the exception
     */
    public String getLabel() {
        return code.getDescription();
    }
}
