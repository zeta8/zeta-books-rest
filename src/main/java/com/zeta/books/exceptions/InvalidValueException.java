package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

public class InvalidValueException extends ZetaException {

    public InvalidValueException(String message) {
        super(ErrorCode.INVALID_VALUE, message);
    }
}
