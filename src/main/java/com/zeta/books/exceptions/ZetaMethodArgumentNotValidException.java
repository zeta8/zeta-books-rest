package com.zeta.books.exceptions;

import org.springframework.validation.BindingResult;

/**
 * The exception thrown by the process of teh validating the input of the service.
 */
public class ZetaMethodArgumentNotValidException extends ZetaException {

    private final transient BindingResult bindingResult;

    public ZetaMethodArgumentNotValidException(final BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

    public BindingResult getBindingResult() {
        return bindingResult;
    }
}
