package com.zeta.books.exceptions;


import com.zeta.books.errors.ErrorCode;

public class ResourceConflictException extends ZetaException {

    public ResourceConflictException(String message) {
        super(ErrorCode.RESS_CONFLICT, message);
    }
}
