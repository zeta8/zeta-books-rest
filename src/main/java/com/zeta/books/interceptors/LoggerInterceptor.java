package com.zeta.books.interceptors;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class LoggerInterceptor extends HandlerInterceptorAdapter {

    private static final String REQUEST_ID = "request.id";
    @Value("${spring.application.name}")
    private String APP_NAME;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) {
        MDC.put(REQUEST_ID, String.format("%s/%s", APP_NAME, UUID.randomUUID()));
        return true;
    }

    @Override
    public void afterCompletion(final HttpServletRequest request, HttpServletResponse response, final Object handler, final Exception ex) {
        MDC.clear();
    }
}
