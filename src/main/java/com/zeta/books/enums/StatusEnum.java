package com.zeta.books.enums;

public enum StatusEnum {
    CUS("Customer", "A customer of zeta"),
    WRI("Writer", "Writer of the book");

    private final String label;
    private final String description;

    StatusEnum(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }
}
