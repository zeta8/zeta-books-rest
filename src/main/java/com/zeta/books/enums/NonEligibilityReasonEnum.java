package com.zeta.books.enums;

public enum NonEligibilityReasonEnum {

    NOT_FOUND("Book not found", "The commanded book not found in our database"),
    NOT_ENOUGH_COPIES("Not enough number of copies", "There are not enough number of copies for this item");

    private final String label;
    private final String description;

    NonEligibilityReasonEnum(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }
}
