package com.zeta.books.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Bean de configuration de {@link io.swagger.models.Swagger}
 */
@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
    @Value("${project.version}")
    private String version;

    @Bean
    public Docket conf() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zeta.books"))
                .paths(PathSelectors.any())
                .build();
        //.pathMapping(URL_CONTEXT);
    }


    @Bean
    public ApiInfo apiInfo() {
        final ApiInfoBuilder builder = new ApiInfoBuilder();
        builder
                .title("APIs for com-zeta-books")
                .description("List of the APIs for the project com-zeta-books")
                .version(version);

        return builder.build();
    }
}
