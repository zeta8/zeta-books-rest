package com.zeta.books.services;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.dtos.*;
import com.zeta.books.entities.PersonEntity;
import com.zeta.books.entities.PersonStatusEntity;
import com.zeta.books.entities.StatusEntity;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.ResourceNotFoundException;
import com.zeta.books.filemanagers.CsvFileReader;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.PersonStatusRepository;
import com.zeta.books.repositories.StatusRepository;
import com.zeta.books.services.interfaces.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * @see PersonService
 */
@Service
public class PersonServiceImpl implements PersonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    @Value("${persons.file.name}")
    private String personFileName;

    @Value("${status.file.name}")
    private String statusFileName;

    private final PersonRepository personRepository;
    private final StatusRepository statusRepository;
    private final PersonStatusRepository personStatusRepository;

    public PersonServiceImpl(final PersonRepository personRepository,
                             final StatusRepository statusRepository,
                             final PersonStatusRepository personStatusRepository) {
        this.personRepository = personRepository;
        this.statusRepository = statusRepository;
        this.personStatusRepository = personStatusRepository;
    }

    /**
     * @see PersonService#createPerson(PersonDto)
     */
    @Override
    public PersonCreationDtoOut createPerson(final PersonDto personDto) {
        final PersonEntity personEntity = personRepository.save(PersonEntity.getBuilder()
                .addPersonCode(personDto.getPersonCode())
                .addFirstName(personDto.getFirstName())
                .addMiddleName(personDto.getMiddleName())
                .addLastName(personDto.getLastName())
                .addBirthDate(personDto.getBirthDate())
                .build());

        StatusEntity statusEntity;
        try {
            statusEntity = statusRepository.findByCode(personDto.getStatus().name())
                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personDto.getStatus().name())));
        } catch (ResourceNotFoundException ex) {
            personRepository.delete(personEntity);
            LOGGER.error("Rollback: The created person entity {} deleted", personEntity);
            throw ex;
        }

        personStatusRepository.save(PersonStatusEntity.getBuilder()
                .addPersonEntity(personEntity)
                .addStatusEntity(statusEntity)
                .build());

        return PersonCreationDtoOut.getBuilder()
                .addPersonId(personEntity.getId())
                .build();
    }

    /**
     * @see PersonService#findPersonById(Long)
     */
    @Override
    public PersonDto findPersonById(final Long id) {

        final PersonEntity personEntity = personRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, id)));

        final String statusCode = personStatusRepository.findByPersonEntity(personEntity)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, id)))
                .getStatusEntity().getCode();


        return PersonDto.getBuilder()
                .addPersonCode(personEntity.getPersonCode())
                .addFirstName(personEntity.getFirstName())
                .addMiddleName(personEntity.getMiddleName())
                .addLastName(personEntity.getLastName())
                .addBirthDate(personEntity.getBirthDate().toLocalDate())
                .addStatus(StatusEnum.valueOf(statusCode))
                .build();
    }

    @Override
    public PersonStatusDto createPersonStatus(final PersonStatusDto personStatus) {
        statusRepository.findByCode(personStatus.getCode())
                .ifPresentOrElse(statusEntity -> LOGGER.info("The status with teh code {} exists already; passing", personStatus.getCode()),
                        () -> statusRepository.save(StatusEntity.getBuilder()
                                .addCode(personStatus.getCode())
                                .addLabel(personStatus.getLabel())
                                .addDescription(personStatus.getDescription())
                                .build()));

        return PersonStatusDto.getBuilder()
                .addCode(personStatus.getCode())
                .addLabel(personStatus.getLabel())
                .addDescription(personStatus.getDescription())
                .build();
    }

    @Override
    public PersonListDtoOut addAllPersonsFormFile() {
        final PersonListDtoOut.PersonListDtoOutBuilder builder = PersonListDtoOut.getBuilder();
        CsvFileReader.getNewInstance()
                .withFileName(personFileName)
                .readCsvFile()
                .forEach(person -> {
                    personRepository.findByPersonCode(person[5])
                            .ifPresentOrElse(personEntity -> LOGGER.info("The person with the code {} exists already: passing", person[5]),
                                    () -> {
                                        final PersonEntity personEntity = personRepository.save(PersonEntity.getBuilder()
                                                .addPersonCode(person[5])
                                                .addFirstName(person[0])
                                                .addMiddleName(person[1])
                                                .addLastName(person[2])
                                                .addBirthDate(LocalDate.parse(person[3]))
                                                .build());

                                        try {
                                            final StatusEntity statusEntity = statusRepository.findByCode(person[4])
                                                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, person[4])));

                                            personStatusRepository.save(PersonStatusEntity.getBuilder()
                                                    .addPersonEntity(personEntity)
                                                    .addStatusEntity(statusEntity)
                                                    .build());

                                        } catch (ResourceNotFoundException ex) {
                                            LOGGER.warn("Rollback: deleting the recently created personEntity: {}", personEntity);
                                            personRepository.delete(personEntity);
                                        }
                                    });
                    builder.addPerson(PersonDto.getBuilder()
                            .addPersonCode(person[5])
                            .addFirstName(person[0])
                            .addMiddleName(person[1])
                            .addLastName(person[2])
                            .addBirthDate(LocalDate.parse(person[3]))
                            .addStatus(StatusEnum.valueOf(person[4]))
                            .build());
                });
        return builder.build();
    }

    /**
     * The helper operation for adding the status to the database.
     * If they exist already, then they will be not added again.
     */
    public StatusListDtoOut addAllStatus() {
        final StatusListDtoOut.StatusListDtoOutBuilder builder = StatusListDtoOut.getBuilder();
        CsvFileReader.getNewInstance()
                .withFileName(statusFileName)
                .readCsvFile()
                .forEach(status ->
                {
                    builder.addStatus(
                            PersonStatusDto.getBuilder()
                                    .addCode(status[0])
                                    .addLabel(status[1])
                                    .addDescription(status[2])
                                    .build());
                    statusRepository.findByCode(status[0])
                            .ifPresentOrElse(statusEntity -> LOGGER.info("The status with the code {} is already present. Passing.", status[0]),
                                    () ->
                                            statusRepository.save(StatusEntity.getBuilder()
                                                    .addCode(status[0])
                                                    .addLabel(status[1])
                                                    .addDescription(status[2])
                                                    .build()));
                });

        return builder.build();
    }
}
