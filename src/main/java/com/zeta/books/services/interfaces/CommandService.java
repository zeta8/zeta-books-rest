package com.zeta.books.services.interfaces;

import com.zeta.books.dtos.CommandCreationDto;
import com.zeta.books.dtos.CommandDto;
import com.zeta.books.dtos.CommandCreationDtoOut;

/**
 * The service to manage the commands ofe clients
 */
public interface CommandService {

    /**
     *
     * @param command: The command to be created
     * @return {@link CommandCreationDtoOut}
     */
    CommandCreationDtoOut createCommand(CommandCreationDto command);

    /**
     * Retrieves a command by its ID
     * @param id: The id of th command to retrieved
     * @return The command {@link CommandDto}
     */
    CommandDto findCommandById(Long id);
}
