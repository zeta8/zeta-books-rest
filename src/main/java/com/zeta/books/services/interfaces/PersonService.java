package com.zeta.books.services.interfaces;

import com.zeta.books.dtos.*;

/**
 * The service for crud operation on person
 */
public interface PersonService {

    /**
     * Adds the person into the DB
     * @param personDto: The person
     */
    PersonCreationDtoOut createPerson(PersonDto personDto);

    /**
     * Finds a person by its Id
     * @param id: The id of the person
     * @return {@link PersonDto}
     */
    PersonDto findPersonById(Long id);


    /**
     * Crates a nes case of status for person
     * @param personStatus: The status of the person
     * @return {@link PersonStatusDto}
     */
    PersonStatusDto createPersonStatus(PersonStatusDto personStatus);

    /**
     * A helper operation for filling the database with some persons
     * @return a list of {@link PersonDto}
     */
    PersonListDtoOut addAllPersonsFormFile();

    /**
     * A helper operation for filling the database with some status
     * @return a list of {@link PersonStatusDto}
     */
    StatusListDtoOut addAllStatus();

}
