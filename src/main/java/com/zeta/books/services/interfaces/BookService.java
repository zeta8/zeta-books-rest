package com.zeta.books.services.interfaces;


import com.zeta.books.dtos.*;

import java.util.List;

/**
 * The service for crud Operation on Books
 */
public interface BookService {

    /**
     * Lists all books sold by Zeta
     * @return {@link BookDtoOut} which contains a list of  {@link BookDto}
     */
    BookDtoOut listAllBooks();


    /**
     * Finds a book by given ID
     * @param id: The id of the book to be searched for
     * @return {@link BookDto}
     */
    BookDto findBookById(Long id);

    /**
     * Adds a book to the base of all codes for zeta
     * @param bookDto: {@link BookDto}
     * @return {@link BookDto} the created book
     */
    BookCreationDtoOut addBook(BookCreationDtoIn bookDto);

    /**
     * adds a list of books to teh database of Zeta
     * @param books a list of {@link BookDto}
     * @return a list of {@link BookCreationDtoOut} which contains the id of all recently created books
     */
    BookListCreationDtoOut addBookList(List<BookCreationDtoIn> books);

    /**
     * A helper service to add some books into the database
     * @return {@link BookListCreationDtoOut}
     */
    BookListCreationDtoOut addAllBooks();
}
