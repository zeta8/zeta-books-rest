package com.zeta.books.services;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.dtos.*;
import com.zeta.books.entities.BookEntity;
import com.zeta.books.entities.BookWriterEntity;
import com.zeta.books.entities.PersonEntity;
import com.zeta.books.entities.StockEntity;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.ResourceNotFoundException;
import com.zeta.books.filemanagers.CsvFileReader;
import com.zeta.books.repositories.BookWriterRepository;
import com.zeta.books.repositories.BooksRepository;
import com.zeta.books.repositories.PersonRepository;
import com.zeta.books.repositories.StockRepository;
import com.zeta.books.services.interfaces.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @see BookService
 */
@Service
public class BookServiceImpl implements BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);

    @Value("${books.file.name}")
    private String bookFileName;

    private final BooksRepository booksRepository;
    private final PersonRepository personRepository;
    private final BookWriterRepository bookWriterRepository;
    private final StockRepository stockRepository;


    public BookServiceImpl(final BooksRepository booksRepository,
                           final PersonRepository personRepository,
                           final BookWriterRepository bookWriterRepository,
                           final StockRepository stockRepository) {
        this.booksRepository = booksRepository;
        this.personRepository = personRepository;
        this.bookWriterRepository = bookWriterRepository;
        this.stockRepository = stockRepository;
    }

    /**
     * @see BookService#listAllBooks()
     */
    @Override
    public BookDtoOut listAllBooks() {
        final BookDtoOut.BookDtoOutBuilder builder = BookDtoOut.getBuilder();
        booksRepository.findAll()
                .forEach(bookEntity -> {
                    List<PersonDto> personDtos = new ArrayList<>();
                    bookWriterRepository.findByBookEntity(bookEntity)
                            .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, bookEntity.getId())))
                            .forEach(entry ->
                                personDtos.add(PersonDto.getBuilder()
                                        .addPersonCode(entry.getPersonEntity().getPersonCode())
                                        .addStatus(StatusEnum.WRI)
                                        .addFirstName(entry.getPersonEntity().getFirstName())
                                        .addMiddleName(entry.getPersonEntity().getMiddleName())
                                        .addLastName(entry.getPersonEntity().getLastName())
                                        .addBirthDate(entry.getPersonEntity().getBirthDate().toLocalDate())
                                        .build()));
                    builder.addBook(
                            BookDto.getBuilder()
                                    .addMainTitle(bookEntity.getMainTitle())
                                    .addSubTitle(bookEntity.getSubTitle())
                                    .addISBN(bookEntity.getIsbn())
                                    .addPrice(bookEntity.getPrice())
                                    .addWriters(personDtos)
                                    .build());
                });
        return builder.build();
    }

    /**
     * @see BookService#findBookById(Long)
     */
    @Override
    public BookDto findBookById(Long id) {
        final BookEntity bookEntity = booksRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, id)));

        final List<PersonDto> writers = new ArrayList<>();
        bookWriterRepository.findByBookEntity(bookEntity)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, id)))
                .forEach(entry ->
                        writers.add(PersonDto.getBuilder()
                                .addPersonCode(entry.getPersonEntity().getPersonCode())
                                .addFirstName(entry.getPersonEntity().getFirstName())
                                .addMiddleName(entry.getPersonEntity().getMiddleName())
                                .addLastName(entry.getPersonEntity().getLastName())
                                .addBirthDate(entry.getPersonEntity().getBirthDate().toLocalDate())
                                .addStatus(StatusEnum.WRI)
                                .build())
                );

        return BookDto.getBuilder()
                .addMainTitle(bookEntity.getMainTitle())
                .addSubTitle(bookEntity.getSubTitle())
                .addISBN(bookEntity.getIsbn())
                .addPrice(bookEntity.getPrice())
                .addWriters(writers)
                .build();
    }

    /**
     * @see BookService#addBook(BookCreationDtoIn)
     */
    @Override
    public BookCreationDtoOut addBook(final BookCreationDtoIn bookDto) {
        final Optional<StockEntity> existingStock = stockRepository.findAll()
                .stream().filter(stock -> stock.getBookEntity().getIsbn().equalsIgnoreCase(bookDto.getIsbn()))
                .findFirst();

        if (existingStock.isPresent()) {
            existingStock.get().addOneToStock();
            stockRepository.save(existingStock.get());
            return BookCreationDtoOut.getBuilder()
                    .addBookId(existingStock.get().getBookEntity().getId())
                    .build();
        } else {
            final BookEntity bookEntity = booksRepository.save(BookEntity.getBuilder()
                    .addMainTitle(bookDto.getMainTitle())
                    .addSubTitle(bookDto.getSubTitle())
                    .addPrice(bookDto.getPrice())
                    .addISBN(bookDto.getIsbn())
                    .build());

            try {
                bookDto.getWriters().forEach(writer -> {
                    final PersonEntity personEntity =
                            personRepository.findById(writer)
                                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, writer)));
                    bookWriterRepository.save(BookWriterEntity.getBuilder()
                            .addWriter(personEntity)
                            .addBook(bookEntity)
                            .build());
                });
            } catch (ResourceNotFoundException ex) {
                booksRepository.delete(bookEntity);
                LOGGER.error("Rollback: The book {} deleted", bookEntity);
                throw ex;
            }

            // adds the book to the stock
            stockRepository.save(
                    StockEntity.getBuilder()
                            .addBookEntity(bookEntity)
                            .addNumberOfCopies(1)
                            .build());

            return BookCreationDtoOut.getBuilder()
                    .addBookId(bookEntity.getId())
                    .build();
        }
    }

    /**
     * @see BookService#addBookList(List)
     */
    @Override
    public BookListCreationDtoOut addBookList(final List<BookCreationDtoIn> books) {
        final BookListCreationDtoOut.BookListCreationDtoOutBuilder builder = BookListCreationDtoOut.getBuilder();
        books.forEach(book ->
                builder.addCreatedBook(addBook(book)));
        return builder.build();
    }

    @Override
    public BookListCreationDtoOut addAllBooks() {
        final List<BookCreationDtoIn> bookCreationDtoInList = new ArrayList<>();
        CsvFileReader.getNewInstance()
                .withFileName(bookFileName)
                .readCsvFile()
                .forEach(line -> {
                    final ArrayList<Long> writers = new ArrayList<>();
                    Arrays.stream(Arrays.stream(line[4].split(" "))
                            .toArray()).collect(Collectors.toList())
                            .forEach(element ->
                                    writers.add(
                                            personRepository.findByPersonCode(element.toString())
                                                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, element.toString())))
                                                    .getId())
                            );

                    bookCreationDtoInList.add(
                            BookCreationDtoIn.getBuilder()
                            .addMainTitle(line[0])
                            .addSubTitle(line[1])
                            .addISBN(line[2])
                            .addPrice(new BigDecimal(line[3]))
                            .addWriters(writers)
                            .build());
                });
        LOGGER.info("The final result: {}", bookCreationDtoInList);
        return addBookList(bookCreationDtoInList);
    }
}
