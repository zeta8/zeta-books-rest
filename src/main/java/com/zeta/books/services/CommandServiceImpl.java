package com.zeta.books.services;

import com.zeta.books.constants.ErrorMessages;
import com.zeta.books.constants.SuccessMessages;
import com.zeta.books.dtos.*;
import com.zeta.books.entities.*;
import com.zeta.books.enums.NonEligibilityReasonEnum;
import com.zeta.books.enums.StatusEnum;
import com.zeta.books.exceptions.ResourceNotFoundException;
import com.zeta.books.exceptions.ZetaIllegalActionException;
import com.zeta.books.repositories.*;
import com.zeta.books.services.interfaces.CommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @see CommandService
 */
@Service
public class CommandServiceImpl implements CommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandServiceImpl.class);

    private final CommandsRepository commandsRepository;
    private final CommandBookRepository commandBookRepository;
    private final PersonRepository personRepository;
    private final StockRepository stockRepository;
    private final BooksRepository booksRepository;
    private final BookWriterRepository bookWriterRepository;
    private final NonEligibleItemRepository nonEligibleItemRepository;
    private final NonEligibilityReasonRepository nonEligibilityReasonRepository;

    public CommandServiceImpl(final CommandsRepository commandsRepository,
                              final CommandBookRepository commandBookRepository,
                              final PersonRepository personRepository,
                              final StockRepository stockRepository,
                              final BooksRepository booksRepository,
                              final BookWriterRepository bookWriterRepository,
                              final NonEligibleItemRepository nonEligibleItemRepository,
                              final NonEligibilityReasonRepository nonEligibilityReasonRepository) {
        this.commandsRepository = commandsRepository;
        this.commandBookRepository = commandBookRepository;
        this.personRepository = personRepository;
        this.stockRepository = stockRepository;
        this.booksRepository = booksRepository;
        this.bookWriterRepository = bookWriterRepository;
        this.nonEligibleItemRepository = nonEligibleItemRepository;
        this.nonEligibilityReasonRepository = nonEligibilityReasonRepository;
    }

    /**
     * @see CommandService#createCommand(CommandCreationDto)
     */
    @Override
    public CommandCreationDtoOut createCommand(final CommandCreationDto command) {
        LOGGER.info("Checking the existence of the client of the command {}", command);
        final PersonEntity personEntity = personRepository.findById(command.getClientId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, command.getClientId())));

        final AtomicReference<BigDecimal> totalPrice = new AtomicReference<>(new BigDecimal("0.00"));
        final List<NonEligibleItemDtoOut> nonEligibleItems = new ArrayList<>();
        final List<BookToBeCommanded> eligibleCommandList = new ArrayList<>();

        final Map<Map<BookEntity, StockEntity>, Integer> eligibleItems = new HashMap<>();

        // here we filter the books that could be added to the commands.
        // Only the existing books whose number of demanded copies are less than or equal to
        // the number of copies available in the stock are eligible to be added to the command.
        // here the total price for the eligible books has also been calculated
        command.getToBeCommandedBooks().forEach(book ->
                booksRepository.findById(book.getBookId())
                        .ifPresentOrElse(bookEntity ->
                                stockRepository.findByBookEntity(bookEntity)
                                        .ifPresentOrElse(stockEntity -> {
                                            if (stockEntity.getNumberOfCopies() >= book.getNumberOfCopies()) {
                                                final Map<BookEntity, StockEntity> eligibleBooks = new HashMap<>();
                                                totalPrice.set(totalPrice.get().add(bookEntity.getPrice().multiply(new BigDecimal(book.getNumberOfCopies()))));
                                                eligibleCommandList.add(book);
                                                eligibleBooks.put(bookEntity, stockEntity);
                                                eligibleItems.put(eligibleBooks, book.getNumberOfCopies());
                                            } else {
                                                nonEligibleItems.add(
                                                        NonEligibleItemDtoOut.getBuilder()
                                                        .addReason(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES)
                                                        .addItem(book.getBookId())
                                                        .build());
                                            }
                                        }, () -> nonEligibleItems.add(
                                                NonEligibleItemDtoOut.getBuilder()
                                                        .addItem(book.getBookId())
                                                        .addReason(NonEligibilityReasonEnum.NOT_FOUND)
                                                        .build()
                                        )), () -> nonEligibleItems.add(
                                NonEligibleItemDtoOut.getBuilder()
                                        .addItem(book.getBookId())
                                        .addReason(NonEligibilityReasonEnum.NOT_FOUND)
                                        .build())
                        ));


        if (eligibleCommandList.isEmpty()) {
            throw new ZetaIllegalActionException(String.format(ErrorMessages.COMMAND_NOT_VALID, command));
        }

        final CommandEntity commandEntity = commandsRepository.save(CommandEntity.getBuilder()
                .addPersonEntity(personEntity)
                .addTotalPrice(totalPrice.get())
                .build());

        eligibleItems.forEach((key, value) -> {
            commandBookRepository.save(
                    CommandBookEntity.getBuilder()
                    .addBookEntity(new ArrayList<>(key.keySet()).get(0))
                    .addNumberOfCopies(value)
                    .addCommandEntity(commandEntity)
                    .build());
            stockRepository.save((new ArrayList<>(key.values())).get(0).subtractFromStock(value));
        });

        nonEligibleItems.forEach(item -> {
            final NonEligibilityReasonEntity reason = nonEligibilityReasonRepository.findByCode(item.getReason().name())
                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, item.getReason().name())));
            if (item.getReason().equals(NonEligibilityReasonEnum.NOT_FOUND)) {
                nonEligibleItemRepository.save(NonEligibleItemEntity.getBuilder()
                        .addReason(reason)
                        .addNonExistingBook(item.getItem())
                        .addCommand(commandEntity)
                        .build());
            } else if (item.getReason().equals(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES)) {
                final BookEntity bookEntity = booksRepository.findById(item.getItem())
                        .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, item.getItem())));
                nonEligibleItemRepository.save(NonEligibleItemEntity.getBuilder()
                        .addReason(reason)
                        .addBook(bookEntity)
                        .addCommand(commandEntity)
                        .build());
            }
        });

        final String message = !nonEligibleItems.isEmpty() ? SuccessMessages.NON_ADDED_BOOKS_LIST_MSG : "";

        return CommandCreationDtoOut.getBuilder()
                .addCommandId(commandEntity.getId())
                .addMessage(message)
                .addNonEligibleItems(nonEligibleItems)
                .build();
    }

    /**
     * @see CommandService#findCommandById(Long)
     */
    @Override
    public CommandDto findCommandById(Long id) {
        final CommandEntity commandEntity = commandsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, id)));

        final List<CommandedBook> commandedBooks = new ArrayList<>();
        commandBookRepository.findByCommandEntity(commandEntity)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, commandEntity.getId())))
                .forEach(entry -> {
                    final BookEntity bookEntity = entry.getBookEntity();
                    final List<PersonDto> writers = new ArrayList<>();
                    bookWriterRepository.findByBookEntity(bookEntity)
                            .ifPresent(element -> element.forEach(
                                    member -> {
                                        final PersonEntity writer = member.getPersonEntity();
                                        writers.add(PersonDto.getBuilder()
                                                .addPersonCode(writer.getPersonCode())
                                                .addFirstName(writer.getFirstName())
                                                .addMiddleName(writer.getMiddleName())
                                                .addLastName(writer.getLastName())
                                                .addBirthDate(writer.getBirthDate().toLocalDate())
                                                .addStatus(StatusEnum.WRI)
                                                .build());
                                    }

                            ));
                    final BookDto book = BookDto.getBuilder()
                            .addMainTitle(bookEntity.getMainTitle())
                            .addSubTitle(bookEntity.getSubTitle())
                            .addISBN(bookEntity.getIsbn())
                            .addPrice(bookEntity.getPrice())
                            .addWriters(writers)
                            .build();
                    commandedBooks.add(CommandedBook.getBuilder()
                            .addBook(book)
                            .addNumberOfCopies(entry.getNumberOfCopies())
                            .build());
                });

        return CommandDto.getBuilder()
                .addClientId(commandEntity.getPersonEntity().getId())
                .addCommandedBooks(commandedBooks)
                .addTotalPrice(commandEntity.getTotalPrice())
                .build();
    }
}
