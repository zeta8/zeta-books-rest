FROM alpine:latest
ENV JAVA_HOME="/usr/lib/jvm/default-jvm/"
RUN apk add openjdk11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
RUN addgroup -S herogroup && adduser -S herouser -G herogroup
USER herouser
ENV JAVA_OPTS="-Dspring.profiles.active=prod"
ENTRYPOINT ["java","-Dspring.profiles.active=prod -jar","/app.jar"]